﻿
CREATE VIEW [dbo].[complete_bucket_sales_credit] AS

/************************************************************************************************
**
**	This view displays the sales credit bucket.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/03/2020		Return all runs not just non-locked ones.
**	FHersh		01/17/2020		Added commissionable_revenue*_amt.
**	FHersh		01/20/2020		Added gross_commission_loan_amt.
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id
,		from_account_nm = to_account_nm
,		from_team_nm = to_team_nm
,		from_group_nm = to_group_nm
,		from_department_cd = to_department_cd
,		from_department_descr = to_department_descr
,		from_lob_cd = to_lob_cd
,		from_lob_descr = to_lob_descr
,		from_location_cd = to_location_cd
,		from_location_descr = to_location_descr
,		to_account_nm = from_account_nm
,		to_team_nm = from_team_nm
,		to_group_nm = from_group_nm
,		to_department_cd = from_department_cd
,		to_department_descr = from_department_descr
,		to_lob_cd = from_lob_cd
,		to_lob_descr = from_lob_descr
,		to_location_cd = from_location_cd
,		to_location_descr = from_location_descr
,		capital_mkt_amt = ISNULL(capital_mkt_amt ,0.0)
,		capital_mkt_amt_delta_adj = ISNULL(capital_mkt_amt_delta_adj ,0.0)
,		capital_mkt_adj_amt = ISNULL(capital_mkt_adj_amt ,0.0)
,		capital_mkt_adj_amt_delta_adj = ISNULL(capital_mkt_adj_amt_delta_adj ,0.0)
,		commissionable_revenue_slice_bullets_amt = ISNULL(commissionable_revenue_slice_bullets_amt, 0.0)
,		commissionable_revenue_slice_bullets_amt_delta_adj = ISNULL(commissionable_revenue_slice_bullets_amt_delta_adj, 0.0)
,       commissionable_revenue_slice_cap_mkts_amt = ISNULL(commissionable_revenue_slice_cap_mkts_amt, 0.0)
,		commissionable_revenue_slice_cap_mkts_amt_delta_adj = ISNULL(commissionable_revenue_slice_cap_mkts_amt_delta_adj, 0.0)
,		commissionable_revenue_slice_credit_amt = ISNULL(commissionable_revenue_slice_credit_amt, 0.0)
,		commissionable_revenue_slice_credit_amt_delta_adj = ISNULL(commissionable_revenue_slice_credit_amt_delta_adj, 0.0)
,		commissionable_revenue_slice_muni_amt = ISNULL(commissionable_revenue_slice_muni_amt, 0.0)
,		commissionable_revenue_slice_muni_amt_delta_adj = ISNULL(commissionable_revenue_slice_muni_amt_delta_adj, 0.0)
,		commissionable_revenue_slice_niche_amt = ISNULL(commissionable_revenue_slice_niche_amt, 0.0)
,		commissionable_revenue_slice_niche_amt_delta_adj = ISNULL(commissionable_revenue_slice_niche_amt_delta_adj, 0.0)
,		commissionable_revenue_slice_resi_mbs_amt = ISNULL(commissionable_revenue_slice_resi_mbs_amt, 0.0)
,		commissionable_revenue_slice_resi_mbs_amt_delta_adj = ISNULL(commissionable_revenue_slice_resi_mbs_amt_delta_adj, 0.0)
,		commissionable_revenue_slice_other_amt = ISNULL(commissionable_revenue_slice_other_amt, 0.0)
,		commissionable_revenue_slice_other_amt_delta_adj = ISNULL(commissionable_revenue_slice_other_amt_delta_adj, 0.0)
,		commissionable_revenue_adj_amt = ISNULL(commissionable_revenue_adj_amt, 0.0)
,		commissionable_revenue_adj_amt_delta_adj = ISNULL(commissionable_revenue_adj_amt_delta_adj, 0.0)
,		commissionable_revenue_trading_amt = ISNULL(commissionable_revenue_trading_amt, 0.0)
,		commissionable_revenue_trading_amt_delta_adj = ISNULL(commissionable_revenue_trading_amt_delta_adj, 0.0)
,		commissionable_revenue_cap_mkt_amt = ISNULL(commissionable_revenue_cap_mkt_amt, 0.0) 
,		commissionable_revenue_cap_mkt_amt_delta_adj = ISNULL(commissionable_revenue_cap_mkt_amt_delta_adj, 0.0) 
,		gross_commission_loan_amt = ISNULL(gross_commission_loan_amt ,0.0)
,		gross_commission_loan_amt_delta_adj = ISNULL(gross_commission_loan_amt_delta_adj ,0.0)
,		non_oms_sales_credit_amt = ISNULL(non_oms_sales_credit_amt ,0.0)
,		non_oms_sales_credit_amt_delta_adj = ISNULL(non_oms_sales_credit_amt_delta_adj ,0.0)
,		sales_credit_bond_amt = ISNULL(sales_credit_bond_amt, 0.0)
,		sales_credit_bond_amt_delta_adj = ISNULL(sales_credit_bond_amt_delta_adj, 0.0)
,		sales_credit_loan_amt = ISNULL(sales_credit_loan_amt, 0.0)
,		sales_credit_loan_amt_delta_adj = ISNULL(sales_credit_loan_amt_delta_adj, 0.0)
,		trade_desk_amt = ISNULL(trade_desk_amt, 0.0)
,		trade_desk_amt_delta_adj = ISNULL(trade_desk_amt_delta_adj, 0.0)
,		trade_desk_adj_amt = ISNULL(trade_desk_adj_amt, 0.0)
,		trade_desk_adj_amt_delta_adj = ISNULL(trade_desk_adj_amt_delta_adj, 0.0)
FROM	(
		SELECT	*
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'capital_mkt_amt', 'capital_mkt_amt_delta_adj',
								'capital_mkt_adj_amt', 'capital_mkt_adj_amt_delta_adj',
								'commissionable_revenue_slice_bullets_amt', 'commissionable_revenue_slice_bullets_amt_delta_adj',
								'commissionable_revenue_slice_cap_mkts_amt', 'commissionable_revenue_slice_cap_mkts_amt_delta_adj',
								'commissionable_revenue_slice_credit_amt', 'commissionable_revenue_slice_credit_amt_delta_adj',
								'commissionable_revenue_slice_muni_amt', 'commissionable_revenue_slice_muni_amt_delta_adj',
								'commissionable_revenue_slice_niche_amt', 'commissionable_revenue_slice_niche_amt_delta_adj',
								'commissionable_revenue_slice_resi_mbs_amt', 'commissionable_revenue_slice_resi_mbs_amt_delta_adj',
								'commissionable_revenue_slice_other_amt', 'commissionable_revenue_slice_other_amt_delta_adj',
								'commissionable_revenue_adj_amt', 'commissionable_revenue_adj_amt_delta_adj', 
								'commissionable_revenue_trading_amt', 'commissionable_revenue_trading_amt_delta_adj',
								'commissionable_revenue_cap_mkt_amt', 'commissionable_revenue_cap_mkt_amt_delta_adj',
								'gross_commission_loan_amt', 'gross_commission_loan_amt_delta_adj',
								'non_oms_sales_credit_amt', 'non_oms_sales_credit_amt_delta_adj',
								'sales_credit_bond_amt', 'sales_credit_bond_amt_delta_adj',
								'sales_credit_loan_amt', 'sales_credit_loan_amt_delta_adj',
								'trade_desk_amt', 'trade_desk_amt_delta_adj',
								'trade_desk_adj_amt', 'trade_desk_adj_amt_delta_adj'
								)
		) src
		PIVOT
		(
		MAX(journal_amt)
		FOR reason_nm IN	(
							[capital_mkt_amt], [capital_mkt_amt_delta_adj],
							[capital_mkt_adj_amt], [capital_mkt_adj_amt_delta_adj],
							[commissionable_revenue_slice_bullets_amt], [commissionable_revenue_slice_bullets_amt_delta_adj],
							[commissionable_revenue_slice_cap_mkts_amt], [commissionable_revenue_slice_cap_mkts_amt_delta_adj],
							[commissionable_revenue_slice_credit_amt], [commissionable_revenue_slice_credit_amt_delta_adj],
							[commissionable_revenue_slice_muni_amt], [commissionable_revenue_slice_muni_amt_delta_adj],
							[commissionable_revenue_slice_niche_amt], [commissionable_revenue_slice_niche_amt_delta_adj],
							[commissionable_revenue_slice_resi_mbs_amt], [commissionable_revenue_slice_resi_mbs_amt_delta_adj],
							[commissionable_revenue_slice_other_amt], [commissionable_revenue_slice_other_amt_delta_adj],
							[commissionable_revenue_adj_amt], [commissionable_revenue_adj_amt_delta_adj],
							[commissionable_revenue_trading_amt], [commissionable_revenue_trading_amt_delta_adj],
							[commissionable_revenue_cap_mkt_amt], [commissionable_revenue_cap_mkt_amt_delta_adj],
							[gross_commission_loan_amt], [gross_commission_loan_amt_delta_adj],
							[non_oms_sales_credit_amt], [non_oms_sales_credit_amt_delta_adj],
							[sales_credit_bond_amt], [sales_credit_bond_amt_delta_adj],
							[sales_credit_loan_amt], [sales_credit_loan_amt_delta_adj],
							[trade_desk_amt], [trade_desk_amt_delta_adj],
							[trade_desk_adj_amt], [trade_desk_adj_amt_delta_adj]
							)
		) piv








