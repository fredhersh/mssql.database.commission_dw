﻿
















CREATE VIEW [dbo].[complete_bucket_accrued_compensation] AS

/************************************************************************************************
**
**	This view displays the accrued compensation bucket.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/03/2020		Return all runs not just non-locked ones.
**	FHersh		01/08/2020		Added total_gross_revenue_amt.
**	FHersh		01/09/2020		Added fines_amt, journal_bonus_amt, new_account_bonus_kicker_amt,
**								and post_commission_rate_adj_amt
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id
,		from_account_nm
,		from_team_nm
,		from_group_nm
,		from_department_cd
,		from_department_descr
,		from_lob_cd
,		from_lob_descr
,		from_location_cd
,		from_location_descr
,		to_account_nm
,		to_team_nm
,		to_group_nm
,		to_department_cd
,		to_department_descr
,		to_lob_cd
,		to_lob_descr
,		to_location_cd
,		to_location_descr

,		prior_month_deferral_balance_amt = ISNULL(prior_month_deferral_balance_amt, 0.0)
,		prior_month_deferral_balance_amt_delta_adj = ISNULL(prior_month_deferral_balance_amt_delta_adj, 0.0)

,		accrued_bonus_adj_amt = ISNULL(accrued_bonus_adj_amt, 0.0)
,		accrued_bonus_adj_amt_delta_adj = ISNULL(accrued_bonus_adj_amt_delta_adj, 0.0)
,		adhoc_bonus_amt = ISNULL(adhoc_bonus_amt, 0.0)
,		adhoc_bonus_amt_delta_adj = ISNULL(adhoc_bonus_amt_delta_adj, 0.0)
,		adhoc_reserve_amt = ISNULL(adhoc_reserve_amt, 0.0)
,		adhoc_reserve_amt_delta_adj = ISNULL(adhoc_reserve_amt_delta_adj, 0.0)
,		bonus_amount_total_amt = ISNULL(bonus_amount_total_amt, 0.0)
,		bonus_amount_total_amt_delta_adj = ISNULL(bonus_amount_total_amt_delta_adj, 0.0)
,		bonus_rate_cost_amt = ISNULL(bonus_rate_cost_amt, 0.0)
,		bonus_rate_cost_amt_delta_adj = ISNULL(bonus_rate_cost_amt_delta_adj, 0.0)
,		bonus_reserve_amt = ISNULL(bonus_reserve_amt, 0.0)
,		bonus_reserve_amt_delta_adj = ISNULL(bonus_reserve_amt_delta_adj, 0.0)
,		bonus_reserve_rate_cost_amt = ISNULL(bonus_reserve_rate_cost_amt, 0.0)
,		bonus_reserve_rate_cost_amt_delta_adj = ISNULL(bonus_reserve_rate_cost_amt_delta_adj, 0.0)
,		deferral_balance_amt = ISNULL(deferral_balance_amt, 0.0)
,		deferral_balance_amt_delta_adj = ISNULL(deferral_balance_amt_delta_adj, 0.0)
,		deferral_input_amt = ISNULL(deferral_input_amt, 0.0)
,		deferral_input_amt_delta_adj = ISNULL(deferral_input_amt_delta_adj, 0.0)
,		draw_amt = ISNULL(draw_amt, 0.0)
,		draw_amt_delta_adj = ISNULL(draw_amt_delta_adj, 0.0)
,		fines_amt = ISNULL(fines_amt, 0.0)
,		fines_amt_delta_adj = ISNULL(fines_amt_delta_adj, 0.0)
,		ib_referral_bonus_amt = ISNULL(ib_referral_bonus_amt, 0.0)
,		ib_referral_bonus_amt_delta_adj = ISNULL(ib_referral_bonus_amt_delta_adj, 0.0)
,		ib_referral_expense_amt = ISNULL(ib_referral_expense_amt, 0.0)
,		ib_referral_expense_amt_delta_adj = ISNULL(ib_referral_expense_amt_delta_adj, 0.0)
,		journal_bonus_amt = ISNULL(journal_bonus_amt, 0.0)
,		journal_bonus_amt_delta_adj = ISNULL(journal_bonus_amt_delta_adj, 0.0)
,		journal_collective_bonus_amt = ISNULL(journal_collective_bonus_amt, 0.0)
,		journal_collective_bonus_amt_delta_adj = ISNULL(journal_collective_bonus_amt_delta_adj, 0.0)
,		journal_deferral_amt = ISNULL(journal_deferral_amt, 0.0)
,		journal_deferral_amt_delta_adj = ISNULL(journal_deferral_amt_delta_adj, 0.0)
,		journal_gross_amt = ISNULL(journal_gross_amt, 0.0)
,		journal_gross_amt_delta_adj = ISNULL(journal_gross_amt_delta_adj, 0.0)
,		journaled_bonus_amt = ISNULL(journaled_bonus_amt, 0.0)
,		journaled_bonus_amt_delta_adj = ISNULL(journaled_bonus_amt_delta_adj, 0.0)
,		monthly_guarantee_amt = ISNULL(monthly_guarantee_amt, 0.0)
,		monthly_guarantee_amt_delta_adj = ISNULL(monthly_guarantee_amt_delta_adj, 0.0)
,		new_account_bonus_kicker_amt = ISNULL(new_account_bonus_kicker_amt, 0.0)
,		new_account_bonus_kicker_amt_delta_adj = ISNULL(new_account_bonus_kicker_amt_delta_adj, 0.0)
,		paid_commission_amt = ISNULL(paid_commission_amt, 0.0)
,		paid_commission_amt_delta_adj = ISNULL(paid_commission_amt_delta_adj, 0.0)
,		post_commission_rate_adj_amt = ISNULL(post_commission_rate_adj_amt, 0.0)
,		post_commission_rate_adj_amt_delta_adj = ISNULL(post_commission_rate_adj_amt_delta_adj, 0.0)
,		pt_score_referral_bonus_amt = ISNULL(pt_score_referral_bonus_amt, 0.0)
,		pt_score_referral_bonus_amt_delta_adj = ISNULL(pt_score_referral_bonus_amt_delta_adj, 0.0)
,		salary_amt = ISNULL(salary_amt, 0.0)
,		salary_amt_delta_adj = ISNULL(salary_amt_delta_adj, 0.0)
,		total_gross_revenue_amt = ISNULL(total_gross_revenue_amt, 0.0)
,		total_gross_revenue_amt_delta_adj = ISNULL(total_gross_revenue_amt_delta_adj, 0.0)

FROM	(
		SELECT	run_period_id
		,		locked_run
		,		as_of_period_id
		,		from_account_nm
		,		from_team_nm
		,		from_group_nm
		,		from_department_cd
		,		from_department_descr
		,		from_lob_cd
		,		from_lob_descr
		,		from_location_cd
		,		from_location_descr
		,		to_account_nm
		,		to_team_nm
		,		to_group_nm
		,		to_department_cd
		,		to_department_descr
		,		to_lob_cd
		,		to_lob_descr
		,		to_location_cd
		,		to_location_descr
		,		reason_nm
		,		journal_amt
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'accrued_bonus_adj_amt', 'accrued_bonus_adj_amt_delta_adj',
								'adhoc_bonus_amt', 'adhoc_bonus_amt_delta_adj',
								'adhoc_reserve_amt', 'adhoc_reserve_amt_delta_adj',
								'bonus_amount_total_amt', 'bonus_amount_total_amt_delta_adj',
								'bonus_rate_cost_amt', 'bonus_rate_cost_amt_delta_adj',
								'bonus_reserve_amt', 'bonus_reserve_amt_delta_adj',
								'bonus_reserve_rate_cost_amt', 'bonus_reserve_rate_cost_amt_delta_adj',
								'deferral_balance_amt', 'deferral_balance_amt_delta_adj',
								'deferral_input_amt', 'deferral_input_amt_delta_adj',
								'draw_amt', 'draw_amt_delta_adj',
								'fines_amt', 'fines_amt_delta_adj',
								'ib_referral_bonus_amt', 'ib_referral_bonus_amt_delta_adj',
								'ib_referral_expense_amt', 'ib_referral_expense_amt_delta_adj',
								'journal_bonus_amt', 'journal_bonus_amt_delta_adj',
								'journal_collective_bonus_amt', 'journal_collective_bonus_amt_delta_adj',
								'journal_deferral_amt', 'journal_deferral_amt_delta_adj',
								'journal_gross_amt', 'journal_gross_amt_delta_adj',
								'journaled_bonus_amt', 'journaled_bonus_amt_delta_adj',
								'monthly_guarantee_amt', 'monthly_guarantee_amt_delta_adj',
								'new_account_bonus_kicker_amt', 'new_account_bonus_kicker_amt_delta_adj',
								'paid_commission_amt', 'paid_commission_amt_delta_adj',
								'post_commission_rate_adj_amt', 'post_commission_rate_adj_amt_delta_adj',
								'pt_score_referral_bonus_amt', 'pt_score_referral_bonus_amt_delta_adj',
								'salary_amt', 'salary_amt_delta_adj',
								'total_gross_revenue_amt', 'total_gross_revenue_amt_delta_adj'
								)

		UNION ALL

		SELECT	run_period_id
		,		locked_run
		,		as_of_period_id
		,		from_account_nm
		,		from_team_nm
		,		from_group_nm
		,		from_department_cd
		,		from_department_descr
		,		from_lob_cd
		,		from_lob_descr
		,		from_location_cd
		,		from_location_descr
		,		to_account_nm
		,		to_team_nm
		,		to_group_nm
		,		to_department_cd
		,		to_department_descr
		,		to_lob_cd
		,		to_lob_descr
		,		to_location_cd
		,		to_location_descr
		,		reason_nm = 'prior_month_' + reason_nm
		,		journal_amt
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'deferral_balance_amt', 'deferral_balance_amt_delta_adj'
								)

		) src
		PIVOT
		(
		MAX(journal_amt)
		FOR reason_nm IN	(
							[accrued_bonus_adj_amt], [accrued_bonus_adj_amt_delta_adj],
							[adhoc_bonus_amt], [adhoc_bonus_amt_delta_adj],
							[adhoc_reserve_amt], [adhoc_reserve_amt_delta_adj],
							[bonus_amount_total_amt], [bonus_amount_total_amt_delta_adj],
							[bonus_rate_cost_amt], [bonus_rate_cost_amt_delta_adj],
							[bonus_reserve_amt], [bonus_reserve_amt_delta_adj],
							[bonus_reserve_rate_cost_amt], [bonus_reserve_rate_cost_amt_delta_adj],
							[deferral_balance_amt], [deferral_balance_amt_delta_adj],
							[deferral_input_amt], [deferral_input_amt_delta_adj],
							[draw_amt], [draw_amt_delta_adj],
							[fines_amt], [fines_amt_delta_adj],
							[ib_referral_bonus_amt], [ib_referral_bonus_amt_delta_adj],
							[ib_referral_expense_amt], [ib_referral_expense_amt_delta_adj],
							[journal_bonus_amt], [journal_bonus_amt_delta_adj],
							[journal_collective_bonus_amt], [journal_collective_bonus_amt_delta_adj],
							[journal_deferral_amt], [journal_deferral_amt_delta_adj],
							[journal_gross_amt], [journal_gross_amt_delta_adj],
							[journaled_bonus_amt], [journaled_bonus_amt_delta_adj],
							[monthly_guarantee_amt], [monthly_guarantee_amt_delta_adj],
							[new_account_bonus_kicker_amt], [new_account_bonus_kicker_amt_delta_adj],
							[paid_commission_amt], [paid_commission_amt_delta_adj],
							[post_commission_rate_adj_amt], [post_commission_rate_adj_amt_delta_adj],
							[pt_score_referral_bonus_amt], [pt_score_referral_bonus_amt_delta_adj],
							[salary_amt], [salary_amt_delta_adj],
							[total_gross_revenue_amt], [total_gross_revenue_amt_delta_adj],

							[prior_month_deferral_balance_amt], [prior_month_deferral_balance_amt_delta_adj]
							)
		) piv











