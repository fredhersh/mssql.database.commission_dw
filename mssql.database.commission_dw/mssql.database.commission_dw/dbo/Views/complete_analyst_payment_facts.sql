﻿



CREATE VIEW [dbo].[complete_analyst_payment_facts] AS

/************************************************************************************************
**
**	This view displays complete analyst payment information.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id
,		from_account_nm
,		from_team_nm
,		from_group_nm
,		from_department_cd
,		from_department_descr
,		from_lob_cd
,		from_lob_descr
,		from_location_cd
,		from_location_descr
,		to_account_nm
,		to_team_nm
,		to_group_nm
,		to_department_cd
,		to_department_descr
,		to_lob_cd
,		to_lob_descr
,		to_location_cd
,		to_location_descr
,		accrued_bonus_amt = ISNULL(accrued_bonus_amt, 0.0)
,		accrued_bonus_part_amt = ISNULL(accrued_bonus_part_amt, 0.0)
,		adhoc_bonus_amt = ISNULL(adhoc_bonus_amt, 0.0)
,		adhoc_bonus_part_amt = ISNULL(adhoc_bonus_part_amt, 0.0)
,		bonus_amt = ISNULL(bonus_amt, 0.0)
,		bonus_part_amt = ISNULL(bonus_part_amt, 0.0)
,		recurring_bonus_amt = ISNULL(recurring_bonus_amt, 0.0)
,		recurring_bonus_rate_yield_part_amt = ISNULL(recurring_bonus_rate_yield_part_amt, 0.0)
,		salary_amt = ISNULL(salary_amt, 0.0)
FROM	(
		SELECT	*
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'accrued_bonus_amt',
								'accrued_bonus_part_amt',
								'adhoc_bonus_amt',
								'adhoc_bonus_part_amt',
								'bonus_amt',
								'bonus_part_amt',
								'recurring_bonus_amt',
								'recurring_bonus_rate_yield_part_amt',
								'salary_amt'
								)
		) src
		PIVOT
		(
		MAX(journal_amt)
		FOR reason_nm IN	(
							[accrued_bonus_amt],
							[accrued_bonus_part_amt],
							[adhoc_bonus_amt],
							[adhoc_bonus_part_amt],
							[bonus_amt],
							[bonus_part_amt],
							[recurring_bonus_amt],
							[recurring_bonus_rate_yield_part_amt],
							[salary_amt]
							)
		) piv




