﻿










CREATE VIEW [dbo].[complete_bucket_sales_credit_clearing] AS

/************************************************************************************************
**
**	This view displays the sales credit clearing bucket.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/03/2020		Return all runs not just non-locked ones.
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id
,		from_account_nm
,		from_team_nm
,		from_group_nm
,		from_department_cd
,		from_department_descr
,		from_lob_cd
,		from_lob_descr
,		from_location_cd
,		from_location_descr
,		to_account_nm
,		to_team_nm
,		to_group_nm
,		to_department_cd
,		to_department_descr
,		to_lob_cd
,		to_lob_descr
,		to_location_cd
,		to_location_descr
,		capital_mkt_amt = ISNULL(capital_mkt_amt ,0.0)
,		capital_mkt_amt_delta_adj = ISNULL(capital_mkt_amt_delta_adj ,0.0)
,		capital_mkt_adj_amt = ISNULL(capital_mkt_adj_amt ,0.0)
,		capital_mkt_adj_amt_delta_adj = ISNULL(capital_mkt_adj_amt_delta_adj ,0.0)
,		non_oms_sales_credit_amt = ISNULL(non_oms_sales_credit_amt, 0.0)
,		non_oms_sales_credit_amt_delta_adj = ISNULL(non_oms_sales_credit_amt_delta_adj, 0.0)
,		trade_desk_amt = ISNULL(trade_desk_amt, 0.0)
,		trade_desk_amt_delta_adj = ISNULL(trade_desk_amt_delta_adj, 0.0)
,		trade_desk_adj_amt = ISNULL(trade_desk_adj_amt, 0.0)
,		trade_desk_adj_amt_delta_adj = ISNULL(trade_desk_adj_amt_delta_adj, 0.0)
FROM	(
		SELECT	*
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'capital_mkt_amt', 'capital_mkt_amt_delta_adj',
								'capital_mkt_adj_amt', 'capital_mkt_adj_amt_delta_adj',
								'non_oms_sales_credit_amt', 'non_oms_sales_credit_amt_delta_adj',
								'trade_desk_amt', 'trade_desk_amt_delta_adj',
								'trade_desk_adj_amt', 'trade_desk_adj_amt_delta_adj'
								)
		) src
		PIVOT
		(
		MAX(journal_amt)
		FOR reason_nm IN	(
							[capital_mkt_amt], [capital_mkt_amt_delta_adj],
							[capital_mkt_adj_amt], [capital_mkt_adj_amt_delta_adj],
							[non_oms_sales_credit_amt], [non_oms_sales_credit_amt_delta_adj],
							[trade_desk_amt], [trade_desk_amt_delta_adj],
							[trade_desk_adj_amt], [trade_desk_adj_amt_delta_adj]
							)
		) piv









