﻿












CREATE VIEW [dbo].[complete_bucket_accrued_bonus_commission] AS

/************************************************************************************************
**
**	This view displays the accrued bonus comission bucket.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/03/2020		Return all runs not just non-locked ones.
**	FHersh		01/07/2020		Added new_account_bonus_kicker_amt.
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id
,		from_account_nm
,		from_team_nm
,		from_group_nm
,		from_department_cd
,		from_department_descr
,		from_lob_cd
,		from_lob_descr
,		from_location_cd
,		from_location_descr
,		to_account_nm
,		to_team_nm
,		to_group_nm
,		to_department_cd
,		to_department_descr
,		to_lob_cd
,		to_lob_descr
,		to_location_cd
,		to_location_descr
,		ib_referral_bonus_amt = ISNULL(ib_referral_bonus_amt, 0.0)
,		ib_referral_bonus_amt_delta_adj = ISNULL(ib_referral_bonus_amt_delta_adj, 0.0)
,		journal_bonus_amt = ISNULL(journal_bonus_amt, 0.0)
,		journal_bonus_amt_delta_adj = ISNULL(journal_bonus_amt_delta_adj, 0.0)
,		journal_collective_bonus_amt = ISNULL(journal_collective_bonus_amt, 0.0)
,		journal_collective_bonus_amt_delta_adj = ISNULL(journal_collective_bonus_amt_delta_adj, 0.0)
,		new_account_bonus_kicker_amt = ISNULL(new_account_bonus_kicker_amt, 0.0)
,		new_account_bonus_kicker_amt_delta_adj = ISNULL(new_account_bonus_kicker_amt_delta_adj, 0.0)
,		pt_score_referral_bonus_amt = ISNULL(pt_score_referral_bonus_amt, 0.0)
,		pt_score_referral_bonus_amt_delta_adj = ISNULL(pt_score_referral_bonus_amt_delta_adj, 0.0)
FROM	(
		SELECT	*
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'ib_referral_bonus_amt', 'ib_referral_bonus_amt_delta_adj',
								'journal_bonus_amt', 'journal_bonus_amt_delta_adj',
								'journal_collective_bonus_amt', 'journal_collective_bonus_amt_delta_adj',
								'new_account_bonus_kicker_amt', 'new_account_bonus_kicker_amt_delta_adj',
								'pt_score_referral_bonus_amt', 'pt_score_referral_bonus_amt_delta_adj'
								)
		) src
		PIVOT
		(
		MAX(journal_amt)
		FOR reason_nm IN	(
							[ib_referral_bonus_amt], [ib_referral_bonus_amt_delta_adj],
							[journal_bonus_amt], [journal_bonus_amt_delta_adj],
							[journal_collective_bonus_amt], [journal_collective_bonus_amt_delta_adj],
							[new_account_bonus_kicker_amt], [new_account_bonus_kicker_amt_delta_adj],
							[pt_score_referral_bonus_amt], [pt_score_referral_bonus_amt_delta_adj]
							)
		) piv









