﻿


CREATE VIEW [dbo].[complete_analyst_cost_facts] AS

/************************************************************************************************
**
**	This view displays complete analyst cost information.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id
,		from_account_nm
,		from_team_nm
,		from_group_nm
,		from_department_cd
,		from_department_descr
,		from_lob_cd
,		from_lob_descr
,		from_location_cd
,		from_location_descr
,		to_account_nm
,		to_team_nm
,		to_group_nm
,		to_department_cd
,		to_department_descr
,		to_lob_cd
,		to_lob_descr
,		to_location_cd
,		to_location_descr
,		accrued_adhoc_bonus_flg = ISNULL(accrued_adhoc_bonus_flg, 0.0)
,		accrued_bonus_flg = ISNULL(accrued_bonus_flg, 0.0)
,		accrued_bonus_payment_flg = ISNULL(accrued_bonus_payment_flg, 0.0)
,		bonus_amount_pct = ISNULL(bonus_amount_pct, 0.0)
,		bonus_amount_val = ISNULL(bonus_amount_val, 0.0)
,		bonus_split_rate_pct = ISNULL(bonus_split_rate_pct, 0.0)
,		bonus_val = ISNULL(bonus_val, 0.0)
,		post_commission_rate_flg = ISNULL(post_commission_rate_flg, 0.0)
,		reserve_bonus_flg = ISNULL(reserve_bonus_flg, 0.0)
,		shared_bonus_flg = ISNULL(shared_bonus_flg, 0.0)
,		shared_bonus_rate_flg = ISNULL(shared_bonus_rate_flg, 0.0)
,		total_bonus_amt = ISNULL(total_bonus_amt, 0.0)
FROM	(
		SELECT	*
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'accrued_adhoc_bonus_flg',
								'accrued_bonus_flg',
								'accrued_bonus_payment_flg',
								'bonus_amount_pct',
								'bonus_amount_val',
								'bonus_split_rate_pct',
								'bonus_val',
								'post_commission_rate_flg',
								'reserve_bonus_flg',
								'shared_bonus_flg',
								'shared_bonus_rate_flg',
								'total_bonus_amt'
								)
		) src
		PIVOT
		(
		MAX(journal_amt)
		FOR reason_nm IN	(
							[accrued_adhoc_bonus_flg],
							[accrued_bonus_flg],
							[accrued_bonus_payment_flg],
							[bonus_amount_pct],
							[bonus_amount_val],
							[bonus_split_rate_pct],
							[bonus_val],
							[post_commission_rate_flg],
							[reserve_bonus_flg],
							[shared_bonus_flg],
							[shared_bonus_rate_flg],
							[total_bonus_amt]
							)
		) piv



