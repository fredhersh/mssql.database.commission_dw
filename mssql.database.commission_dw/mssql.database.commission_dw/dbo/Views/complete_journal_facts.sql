﻿









CREATE VIEW [dbo].[complete_journal_facts] AS

/************************************************************************************************
**
**	This view displays complete journal data information.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/08/2020		For accounts aruggiero and jcurlin force group to FIG.
**
************************************************************************************************/

SELECT	run_period_id = rd.run_eff_yyyymm_no
,		locked_run = rd.is_run_locked
,		as_of_period_id = aodd.yyyymm_no
,		from_account_nm = fda.account_nm
,		from_team_nm = fdt.team_nm
,		from_group_nm =	CASE
						WHEN fda.account_nm IN ('aruggiero','jcurlin') THEN 'FIG'
						ELSE fdd.group_nm
						END
,		from_department_cd = fdoc.department_cd
,		from_department_descr = fdoc.department_descr
,		from_lob_cd = fdoc.lob_cd
,		from_lob_descr = fdoc.lob_descr
,		from_location_cd = fdoc.location_cd
,		from_location_descr = fdoc.location_descr
,		to_account_nm = tda.account_nm
,		to_team_nm = tdt.team_nm
,		to_group_nm =	CASE
						WHEN tda.account_nm IN ('aruggiero','jcurlin') THEN 'FIG'
						ELSE tdd.group_nm
						END
,		to_department_cd = tdoc.department_cd
,		to_department_descr = tdoc.department_descr
,		to_lob_cd = tdoc.lob_cd
,		to_lob_descr = tdoc.lob_descr
,		to_location_cd = tdoc.location_cd
,		to_location_descr = tdoc.location_descr
,		dr.reason_nm
,		fct.journal_amt
FROM	fact_journal fct	INNER JOIN dim_run rd ON (fct.run_id = rd.run_id)
							INNER JOIN dim_date aodd ON (fct.as_of_date_id = aodd.date_id)
							INNER JOIN dim_account fda ON (fct.from_account_id = fda.account_id)
							INNER JOIN dim_team fdt ON (fct.from_team_id = fdt.team_id)
							INNER JOIN dim_group fdd ON (fct.from_group_id = fdd.group_id)
							INNER JOIN dim_oracle_code fdoc ON (fct.from_oracle_code_Id = fdoc.oracle_code_id)
							INNER JOIN dim_account tda ON (fct.to_account_id = tda.account_id)
							INNER JOIN dim_team tdt ON (fct.to_team_id = tdt.team_id)
							INNER JOIN dim_group tdd ON (fct.to_group_id = tdd.group_id)
							INNER JOIN dim_oracle_code tdoc ON (fct.to_oracle_code_Id = tdoc.oracle_code_id)
							INNER JOIN dim_reason dr ON (fct.reason_id = dr.reason_id)
							INNER JOIN dim_comment dc ON (fct.comment_id = dc.comment_id)









