﻿



CREATE VIEW [dbo].[report_analyst_cost_derivation] AS

/************************************************************************************************
**
**	This view displays analyst cost derivation for reporting.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/03/2020		Return all runs not just non-locked ones.
**	FHersh		01/10/2020		Added new_money_bonus, misc_bonus, accrued_bonus and
**								reserve_bonus.
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id 
,		from_account_nm
,		from_team_nm
,		from_group_nm
,		from_department_cd
,		from_department_descr
,		from_lob_cd
,		from_lob_descr
,		from_location_cd
,		from_location_descr
,		to_account_nm
,		to_team_nm
,		to_group_nm
,		to_department_cd
,		to_department_descr
,		to_lob_cd
,		to_lob_descr
,		to_location_cd
,		to_location_descr
,		bonus_amt = ISNULL(bonus_amt, 0)
,		salary_amt = ISNULL(salary_amt, 0)
,		new_money_bonus =	CASE
							WHEN ISNULL(accrued_bonus_flg, 0) = 0 AND ISNULL(recurring_bonus_amt, 0) <> 0 AND ISNULL(accrued_bonus_payment_flg, 0) = 0 AND ISNULL(accrued_adhoc_bonus_flg, 0) = 0 AND ISNULL(reserve_bonus_flg, 0) = 0 AND ISNULL(adhoc_bonus_amt, 0) <> 0 THEN ISNULL(recurring_bonus_amt, 0) + ISNULL(adhoc_bonus_amt, 0)
							WHEN ISNULL(accrued_bonus_flg, 0) = 0 AND ISNULL(recurring_bonus_amt, 0) <> 0 THEN recurring_bonus_amt 
							WHEN ISNULL(accrued_bonus_payment_flg, 0) = 0 AND ISNULL(accrued_adhoc_bonus_flg, 0) = 0 AND ISNULL(reserve_bonus_flg, 0) = 0 AND ISNULL(adhoc_bonus_amt, 0) <> 0 THEN ISNULL(adhoc_bonus_amt, 0)
							ELSE 0.0
							END 
,		misc_bonus =	CASE
						WHEN ISNULL(accrued_bonus_flg, 0) = 0 AND ISNULL(accrued_adhoc_bonus_flg, 0) = 0 THEN ISNULL(adhoc_bonus_amt, 0)
						ELSE 0.0
						END
,		accrued_bonus =	CASE
						WHEN ISNULL(accrued_bonus_flg, 0) = 1 THEN ISNULL(recurring_bonus_amt, 0)
						ELSE 0.0
						END
,		reserve_bonus =	CASE
						WHEN ISNULL(reserve_bonus_flg, 0) = 1 THEN ISNULL(adhoc_bonus_amt, 0)
						ELSE 0.0
						END
FROM	( 
		SELECT	run_period_id
		,		locked_run
		,		as_of_period_id
		,		from_account_nm
		,		from_team_nm
		,		from_group_nm
		,		from_department_cd
		,		from_department_descr
		,		from_lob_cd
		,		from_lob_descr
		,		from_location_cd
		,		from_location_descr
		,		to_account_nm
		,		to_team_nm
		,		to_group_nm
		,		to_department_cd
		,		to_department_descr
		,		to_lob_cd
		,		to_lob_descr
		,		to_location_cd
		,		to_location_descr
		,		reason_nm
		,		journal_amt
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'accrued_adhoc_bonus_flg', 'accrued_bonus_flg', 'accrued_bonus_payment_flg', 'adhoc_bonus_amt',
								'bonus_amt', 'recurring_bonus_amt', 'reserve_bonus_flg', 'salary_amt'
								)
		AND		from_account_nm <> 'ptcp'
		) src
		PIVOT
		(
		SUM(journal_amt)
		FOR reason_nm IN	(
							[accrued_adhoc_bonus_flg], [accrued_bonus_flg], [accrued_bonus_payment_flg], [adhoc_bonus_amt],
							[bonus_amt], [recurring_bonus_amt], [reserve_bonus_flg], [salary_amt]
							)
		) piv


