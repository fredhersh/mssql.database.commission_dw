﻿









CREATE VIEW [dbo].[complete_bucket_commission_misc_deduction] AS

/************************************************************************************************
**
**	This view displays the commission miscellaneous deduction bucket.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/03/2020		Return all runs not just non-locked ones.
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id
,		from_account_nm
,		from_team_nm
,		from_group_nm
,		from_department_cd
,		from_department_descr
,		from_lob_cd
,		from_lob_descr
,		from_location_cd
,		from_location_descr
,		to_account_nm
,		to_team_nm
,		to_group_nm
,		to_department_cd
,		to_department_descr
,		to_lob_cd
,		to_lob_descr
,		to_location_cd
,		to_location_descr
,		fines_amt = ISNULL(fines_amt, 0.0)
,		post_commission_rate_adj_amt = ISNULL(post_commission_rate_adj_amt, 0.0)
FROM	(
		SELECT	*
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'fines_amt', 'fines_amt_delta_adj',
								'post_commission_rate_adj_amt', 'post_commission_rate_adj_amt_delta_adj'
								)
		) src
		PIVOT
		(
		MAX(journal_amt)
		FOR reason_nm IN	(
							[fines_amt], [fines_amt_delta_adj],
							[post_commission_rate_adj_amt], [post_commission_rate_adj_amt_delta_adj]
							)
		) piv








