﻿




CREATE VIEW [dbo].[complete_salesperson_commission_facts] AS

/************************************************************************************************
**
**	This view displays complete salesperson commission information.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/07/2020		Added new_account_bonus_kicker_amt.
**	FHersh		01/17/2020		Added commissionable_revenue*_amt.
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id
,		from_account_nm
,		from_team_nm
,		from_group_nm
,		from_department_cd
,		from_department_descr
,		from_lob_cd
,		from_lob_descr
,		from_location_cd
,		from_location_descr
,		to_account_nm
,		to_team_nm
,		to_group_nm
,		to_department_cd
,		to_department_descr
,		to_lob_cd
,		to_lob_descr
,		to_location_cd
,		to_location_descr
,		accrued_bonus_adj_amt = ISNULL(accrued_bonus_adj_amt, 0.0)
,		accrued_bonus_rate_cost_amt = ISNULL(accrued_bonus_rate_cost_amt, 0.0)
,		accrued_total_amt = ISNULL(accrued_total_amt, 0.0)
,		adhoc_bonus_amt = ISNULL(adhoc_bonus_amt, 0.0)
,		adhoc_reserve_amt = ISNULL(adhoc_reserve_amt, 0.0)
,		advance_balance_amt = ISNULL(advance_balance_amt, 0.0)
,		advance_cost_amt = ISNULL(advance_cost_amt, 0.0)
,		advance_to_adj_amt = ISNULL(advance_to_adj_amt, 0.0)
,		analyst_salary_cost_total_amt = ISNULL(analyst_salary_cost_total_amt, 0.0)
,		bonus_adj_amt = ISNULL(bonus_adj_amt, 0.0)
,		bonus_amount_total_amt = ISNULL(bonus_amount_total_amt, 0.0)
,		bonus_rate_cost_amt = ISNULL(bonus_rate_cost_amt, 0.0)
,		bonus_reserve_amt = ISNULL(bonus_reserve_amt, 0.0)
,		bonus_reserve_rate_cost_amt = ISNULL(bonus_reserve_rate_cost_amt, 0.0)
,		capital_mkt_adj_amt = ISNULL(capital_mkt_adj_amt, 0.0)
,		capital_mkt_amt = ISNULL(capital_mkt_amt, 0.0)
,		collective_bonus_adj_amt = ISNULL(collective_bonus_adj_amt, 0.0)
,		collective_bonus_amt = ISNULL(collective_bonus_amt, 0.0)
,		commission_adj_amt = ISNULL(commission_adj_amt, 0.0)
,		commission_per_producer_ytd_amt = ISNULL(commission_per_producer_ytd_amt, 0.0)
,		commissionable_revenue_adj_amt = ISNULL(commissionable_revenue_adj_amt, 0.0)
,		commissionable_revenue_amt = ISNULL(commissionable_revenue_amt, 0.0)
,		commissionable_revenue_cap_mkt_amt = ISNULL(commissionable_revenue_cap_mkt_amt, 0.0)
,		commissionable_revenue_slice_bullets_amt = ISNULL(commissionable_revenue_slice_bullets_amt, 0.0)
,		commissionable_revenue_slice_cap_mkts_amt = ISNULL(commissionable_revenue_slice_cap_mkts_amt, 0.0)
,		commissionable_revenue_slice_credit_amt = ISNULL(commissionable_revenue_slice_credit_amt, 0.0)
,		commissionable_revenue_slice_muni_amt = ISNULL(commissionable_revenue_slice_muni_amt, 0.0)
,		commissionable_revenue_slice_niche_amt = ISNULL(commissionable_revenue_slice_niche_amt, 0.0)
,		commissionable_revenue_slice_other_amt = ISNULL(commissionable_revenue_slice_other_amt, 0.0)
,		commissionable_revenue_slice_resi_mbs_amt = ISNULL(commissionable_revenue_slice_resi_mbs_amt, 0.0)
,		commissionable_revenue_trading_amt = ISNULL(commissionable_revenue_trading_amt, 0.0),		deferral_adj_amt = ISNULL(deferral_adj_amt, 0.0)
,		deferral_balance_amt = ISNULL(deferral_balance_amt, 0.0)
,		deferral_input_amt = ISNULL(deferral_input_amt, 0.0)
,		draw_amt = ISNULL(draw_amt, 0.0)
,		fig_rate_deferral_amt = ISNULL(fig_rate_deferral_amt, 0.0)
,		fig_rate_pct = ISNULL(fig_rate_pct, 0.0)
,		fig_table_per_producer_amt = ISNULL(fig_table_per_producer_amt, 0.0)
,		fines_amt = ISNULL(fines_amt, 0.0)
,		gross_adj_amt = ISNULL(gross_adj_amt, 0.0)
,		gross_commission_bond_amt = ISNULL(gross_commission_bond_amt, 0.0)
,		gross_commission_loan_amt = ISNULL(gross_commission_loan_amt, 0.0)
,		gross_sales_credit_adj_amt = ISNULL(gross_sales_credit_adj_amt, 0.0)
,		ib_referral_bonus_amt = ISNULL(ib_referral_bonus_amt, 0.0)
,		ib_referral_expense_amt = ISNULL(ib_referral_expense_amt, 0.0)
,		journal_accrued_bonus_amt = ISNULL(journal_accrued_bonus_amt, 0.0)
,		journal_bonus_amt = ISNULL(journal_bonus_amt, 0.0)
,		journal_collective_bonus_amt = ISNULL(journal_collective_bonus_amt, 0.0)
,		journal_deferral_adj_amt = ISNULL(journal_deferral_adj_amt, 0.0)
,		journal_deferral_amt = ISNULL(journal_deferral_amt, 0.0)
,		journal_gross_amt = ISNULL(journal_gross_amt, 0.0)
,		monthly_guarantee_amt = ISNULL(monthly_guarantee_amt, 0.0)
,		new_account_bonus_amt = ISNULL(new_account_bonus_amt, 0.0)
,		new_account_bonus_kicker_amt = ISNULL(new_account_bonus_kicker_amt, 0.0)
,		non_oms_sales_credit_amt = ISNULL(non_oms_sales_credit_amt, 0.0)
,		paid_commission_amt = ISNULL(paid_commission_amt, 0.0)
,		past_month_adj_amt = ISNULL(past_month_adj_amt, 0.0)
,		post_commission_rate_adj_amt = ISNULL(post_commission_rate_adj_amt, 0.0)
,		pre_commission_rate_adj_amt = ISNULL(pre_commission_rate_adj_amt, 0.0)
,		prepaid_commission_amt = ISNULL(prepaid_commission_amt, 0.0)
,		pt_score_referral_bonus_amt = ISNULL(pt_score_referral_bonus_amt, 0.0)
,		reserve_adj_amt = ISNULL(reserve_adj_amt, 0.0)
,		reserve_bonus_amt = ISNULL(reserve_bonus_amt, 0.0)
,		reserve_total_amt = ISNULL(reserve_total_amt, 0.0)
,		sales_credit_bond_amt = ISNULL(sales_credit_bond_amt, 0.0)
,		sales_credit_loan_amt = ISNULL(sales_credit_loan_amt, 0.0)
,		shared_loan_sales_credit_amt = ISNULL(shared_loan_sales_credit_amt, 0.0)
,		shared_non_oms_sales_credit_amt = ISNULL(shared_non_oms_sales_credit_amt, 0.0)
,		shared_sales_credit_amt = ISNULL(shared_sales_credit_amt, 0.0)
,		total_costs_amt = ISNULL(total_costs_amt, 0.0)
,		total_gross_revenue_amt = ISNULL(total_gross_revenue_amt, 0.0)
,		trade_desk_adj_amt = ISNULL(trade_desk_adj_amt, 0.0)
,		trade_desk_amt = ISNULL(trade_desk_amt, 0.0)
FROM	(
		SELECT	*
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'accrued_bonus_adj_amt',
								'accrued_bonus_rate_cost_amt',
								'accrued_total_amt',
								'adhoc_bonus_amt',
								'adhoc_reserve_amt',
								'advance_balance_amt',
								'advance_cost_amt',
								'advance_to_adj_amt',
								'analyst_salary_cost_total_amt',
								'bonus_adj_amt',
								'bonus_amount_total_amt',
								'bonus_rate_cost_amt',
								'bonus_reserve_amt',
								'bonus_reserve_rate_cost_amt',
								'capital_mkt_adj_amt',
								'capital_mkt_amt',
								'collective_bonus_adj_amt',
								'collective_bonus_amt',
								'commission_adj_amt',
								'commission_per_producer_ytd_amt',
								'commissionable_revenue_adj_amt',
								'commissionable_revenue_amt',
								'commissionable_revenue_cap_mkt_amt',
								'commissionable_revenue_slice_bullets_amt',
								'commissionable_revenue_slice_cap_mkts_amt',
								'commissionable_revenue_slice_credit_amt',
								'commissionable_revenue_slice_muni_amt',
								'commissionable_revenue_slice_niche_amt',
								'commissionable_revenue_slice_other_amt',
								'commissionable_revenue_slice_resi_mbs_amt',
								'commissionable_revenue_trading_amt',
								'deferral_adj_amt',
								'deferral_balance_amt',
								'deferral_input_amt',
								'draw_amt',
								'fig_rate_deferral_amt',
								'fig_rate_pct',
								'fig_table_per_producer_amt',
								'fines_amt',
								'gross_adj_amt',
								'gross_commission_bond_amt',
								'gross_commission_loan_amt',
								'gross_sales_credit_adj_amt',
								'ib_referral_bonus_amt',
								'ib_referral_expense_amt',
								'journal_accrued_bonus_amt',
								'journal_bonus_amt',
								'journal_collective_bonus_amt',
								'journal_deferral_adj_amt',
								'journal_deferral_amt',
								'journal_gross_amt',
								'monthly_guarantee_amt',
								'new_account_bonus_amt',
								'new_account_bonus_kicker_amt',
								'non_oms_sales_credit_amt',
								'paid_commission_amt',
								'past_month_adj_amt',
								'post_commission_rate_adj_amt',
								'pre_commission_rate_adj_amt',
								'prepaid_commission_amt',
								'pt_score_referral_bonus_amt',
								'reserve_adj_amt',
								'reserve_bonus_amt',
								'reserve_total_amt',
								'sales_credit_bond_amt',
								'sales_credit_loan_amt',
								'shared_loan_sales_credit_amt',
								'shared_non_oms_sales_credit_amt',
								'shared_sales_credit_amt',
								'total_costs_amt',
								'total_gross_revenue_amt',
								'trade_desk_adj_amt',
								'trade_desk_amt'								)
								) src
		PIVOT
		(
		MAX(journal_amt)
		FOR reason_nm IN	(
							[accrued_bonus_adj_amt],
							[accrued_bonus_rate_cost_amt],
							[accrued_total_amt],
							[adhoc_bonus_amt],
							[adhoc_reserve_amt],
							[advance_balance_amt],
							[advance_cost_amt],
							[advance_to_adj_amt],
							[analyst_salary_cost_total_amt],
							[bonus_adj_amt],
							[bonus_amount_total_amt],
							[bonus_rate_cost_amt],
							[bonus_reserve_amt],
							[bonus_reserve_rate_cost_amt],
							[capital_mkt_adj_amt],
							[capital_mkt_amt],
							[collective_bonus_adj_amt],
							[collective_bonus_amt],
							[commission_adj_amt],
							[commission_per_producer_ytd_amt],
							[commissionable_revenue_adj_amt],
							[commissionable_revenue_amt],
							[commissionable_revenue_cap_mkt_amt],
							[commissionable_revenue_slice_bullets_amt],
							[commissionable_revenue_slice_cap_mkts_amt],
							[commissionable_revenue_slice_credit_amt],
							[commissionable_revenue_slice_muni_amt],
							[commissionable_revenue_slice_niche_amt],
							[commissionable_revenue_slice_other_amt],
							[commissionable_revenue_slice_resi_mbs_amt],
							[commissionable_revenue_trading_amt],
							[deferral_adj_amt],
							[deferral_balance_amt],
							[deferral_input_amt],
							[draw_amt],
							[fig_rate_deferral_amt],
							[fig_rate_pct],
							[fig_table_per_producer_amt],
							[fines_amt],
							[gross_adj_amt],
							[gross_commission_bond_amt],
							[gross_commission_loan_amt],
							[gross_sales_credit_adj_amt],
							[ib_referral_bonus_amt],
							[ib_referral_expense_amt],
							[journal_accrued_bonus_amt],
							[journal_bonus_amt],
							[journal_collective_bonus_amt],
							[journal_deferral_adj_amt],
							[journal_deferral_amt],
							[journal_gross_amt],
							[monthly_guarantee_amt],
							[new_account_bonus_amt],
							[new_account_bonus_kicker_amt],
							[non_oms_sales_credit_amt],
							[paid_commission_amt],
							[past_month_adj_amt],
							[post_commission_rate_adj_amt],
							[pre_commission_rate_adj_amt],
							[prepaid_commission_amt],
							[pt_score_referral_bonus_amt],
							[reserve_adj_amt],
							[reserve_bonus_amt],
							[reserve_total_amt],
							[sales_credit_bond_amt],
							[sales_credit_loan_amt],
							[shared_loan_sales_credit_amt],
							[shared_non_oms_sales_credit_amt],
							[shared_sales_credit_amt],
							[total_costs_amt],
							[total_gross_revenue_amt],
							[trade_desk_adj_amt],
							[trade_desk_amt]
							)
		) piv



