﻿









CREATE VIEW [dbo].[complete_bucket_sales_assistant_bonus_salesperson] AS

/************************************************************************************************
**
**	This view displays the sales assistant bonus salesperson bucket.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/03/2020		Return all runs not just non-locked ones.
**
************************************************************************************************/

SELECT	run_period_id
,		locked_run
,		as_of_period_id
,		from_account_nm
,		from_team_nm
,		from_group_nm
,		from_department_cd
,		from_department_descr
,		from_lob_cd
,		from_lob_descr
,		from_location_cd
,		from_location_descr
,		to_account_nm
,		to_team_nm
,		to_group_nm
,		to_department_cd
,		to_department_descr
,		to_lob_cd
,		to_lob_descr
,		to_location_cd
,		to_location_descr
,		accrued_bonus_adj_amt = ISNULL(accrued_bonus_adj_amt, 0.0)
,		accrued_bonus_adj_amt_delta_adj = ISNULL(accrued_bonus_adj_amt_delta_adj, 0.0)
,		accrued_bonus_rate_cost_amt = ISNULL(accrued_bonus_rate_cost_amt, 0.0)
,		accrued_bonus_rate_cost_amt_delta_adj = ISNULL(accrued_bonus_rate_cost_amt_delta_adj, 0.0)
,		accrued_total_amt = ISNULL(accrued_total_amt, 0.0)
,		accrued_total_amt_delta_adj = ISNULL(accrued_total_amt_delta_adj, 0.0)
,		adhoc_reserve_amt = ISNULL(adhoc_reserve_amt, 0.0)
,		adhoc_reserve_amt_delta_adj = ISNULL(adhoc_reserve_amt_delta_adj, 0.0)
,		bonus_reserve_amt = ISNULL(bonus_reserve_amt, 0.0)
,		bonus_reserve_amt_delta_adj = ISNULL(bonus_reserve_amt_delta_adj, 0.0)
,		bonus_reserve_rate_cost_amt  = ISNULL(bonus_reserve_rate_cost_amt , 0.0)
,		bonus_reserve_rate_cost_amt_delta_adj = ISNULL(bonus_reserve_rate_cost_amt_delta_adj, 0.0)
,		reserve_adj_amt = ISNULL(reserve_adj_amt, 0.0)
,		reserve_adj_amt_delta_adj = ISNULL(reserve_adj_amt_delta_adj, 0.0)
,		reserve_bonus_amt = ISNULL(reserve_bonus_amt, 0.0)
,		reserve_bonus_amt_delta_adj = ISNULL(reserve_bonus_amt_delta_adj, 0.0)
FROM	(
		SELECT	*
		FROM	complete_journal_facts
		WHERE	reason_nm IN	(
								'accrued_bonus_adj_amt', 'accrued_bonus_adj_amt_delta_adj',
								'accrued_bonus_rate_cost_amt', 'accrued_bonus_rate_cost_amt_delta_adj',
								'accrued_total_amt', 'accrued_total_amt_delta_adj',
								'adhoc_reserve_amt', 'adhoc_reserve_amt_delta_adj',
								'bonus_reserve_amt', 'bonus_reserve_amt_delta_adj',
								'bonus_reserve_rate_cost_amt', 'bonus_reserve_rate_cost_amt_delta_adj',
								'reserve_adj_amt', 'reserve_adj_amt_delta_adj',
								'reserve_bonus_amt', 'reserve_bonus_amt_delta_adj'
								)
		) src
		PIVOT
		(
		MAX(journal_amt)
		FOR reason_nm IN	(
							[accrued_bonus_adj_amt], [accrued_bonus_adj_amt_delta_adj],
							[accrued_bonus_rate_cost_amt], [accrued_bonus_rate_cost_amt_delta_adj],
							[accrued_total_amt], [accrued_total_amt_delta_adj],
							[adhoc_reserve_amt], [adhoc_reserve_amt_delta_adj],
							[bonus_reserve_amt], [bonus_reserve_amt_delta_adj],
							[bonus_reserve_rate_cost_amt], [bonus_reserve_rate_cost_amt_delta_adj],
							[reserve_adj_amt], [reserve_adj_amt_delta_adj],
							[reserve_bonus_amt], [reserve_bonus_amt_delta_adj]
							)
		) piv








