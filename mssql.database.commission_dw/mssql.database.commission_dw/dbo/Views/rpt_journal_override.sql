﻿






CREATE VIEW [dbo].[rpt_journal_override] AS

/************************************************************************************************
**
**	This view displays all of the override types in one view.
**
**	FHersh		02/26/2020		Created.
**	Fhersh		02/28/2020		Standardize view around table rpt_override.
**
************************************************************************************************/

SELECT	[asOfPeriodId]
,		[Group]
,		[Team]
,		[Person]
,		[Category]
,		[OracleAccount]
,		[OracleAccountDesc]
,		[Side] =	CASE
					WHEN [Debit] <> 0 THEN 'Debit'
					WHEN [Credit] <> 0.0 THEN 'Credit'
					ELSE 'Unknown'
					END
,		[LOB]
,		[Department]
,		[Location]
,		[Debit]
,		[Credit]
,		[Formula]
FROM	[dbo].[rpt_override]