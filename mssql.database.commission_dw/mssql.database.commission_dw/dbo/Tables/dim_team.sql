﻿CREATE TABLE [dbo].[dim_team] (
    [team_id] INT          IDENTITY (1, 1) NOT NULL,
    [team_nm] VARCHAR (50) NOT NULL,
    CONSTRAINT [pk_dim_team] PRIMARY KEY CLUSTERED ([team_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [uie_dim_team_team_nm]
    ON [dbo].[dim_team]([team_nm] ASC)
    ON [commission_dw_fg02];

