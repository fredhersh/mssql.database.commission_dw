﻿CREATE TABLE [dbo].[dim_comment] (
    [comment_id]  INT            IDENTITY (1, 1) NOT NULL,
    [comment_txt] VARCHAR (1000) NOT NULL,
    CONSTRAINT [pk_dim_comment] PRIMARY KEY CLUSTERED ([comment_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [uie_dim_comment_comment_txt]
    ON [dbo].[dim_comment]([comment_txt] ASC)
    ON [commission_dw_fg02];

