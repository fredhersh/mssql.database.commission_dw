﻿CREATE TABLE [dbo].[dim_date] (
    [date_id]   INT      IDENTITY (1, 1) NOT NULL,
    [date_dt]   DATETIME NOT NULL,
    [year_no]   INT      NOT NULL,
    [month_no]  INT      NOT NULL,
    [yyyymm_no] INT      NOT NULL,
    [day_no]    INT      NOT NULL,
    CONSTRAINT [pk_dim_date] PRIMARY KEY CLUSTERED ([date_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [uie_dim_date_date_dt]
    ON [dbo].[dim_date]([date_dt] ASC)
    ON [commission_dw_fg02];

