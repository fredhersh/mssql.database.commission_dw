﻿CREATE TABLE [dbo].[dim_reason] (
    [reason_id]           INT           IDENTITY (1, 1) NOT NULL,
    [reason_nm]           VARCHAR (100)  NOT NULL,
    [reason_descr]        VARCHAR (200) NOT NULL,
    [delta_adj_reason_id] INT           NULL,
    CONSTRAINT [pk_dim_reason] PRIMARY KEY CLUSTERED ([reason_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [uie_dim_reason_reason_nm]
    ON [dbo].[dim_reason]([reason_nm] ASC)
    ON [commission_dw_fg02];

