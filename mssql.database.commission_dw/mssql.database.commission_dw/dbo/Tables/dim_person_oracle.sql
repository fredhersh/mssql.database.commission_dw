﻿CREATE TABLE [dbo].[dim_person_oracle] (
    [person_cd]     VARCHAR (50) NOT NULL,
    [start_id]      INT          NOT NULL,
    [end_id]        INT          NULL,
    [department_cd] VARCHAR (3)  NOT NULL,
    [lob_cd]        VARCHAR (2)  NOT NULL,
    [location_cd]   VARCHAR (3)  NOT NULL,
    CONSTRAINT [pk_dim_person_oracle] PRIMARY KEY CLUSTERED ([person_cd] ASC, [start_id] ASC)
);

