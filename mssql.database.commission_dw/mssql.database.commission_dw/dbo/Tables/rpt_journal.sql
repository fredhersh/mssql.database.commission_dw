﻿CREATE TABLE [dbo].[rpt_journal] (
    [AsOfPeriodId]      INT             NOT NULL,
    [Group]             VARCHAR (50)    NOT NULL,
    [Team]              VARCHAR (50)    NOT NULL,
    [Person]            VARCHAR (50)    NOT NULL,
    [Category]          VARCHAR (50)    NOT NULL,
    [OracleAccount]     VARCHAR (5)     NOT NULL,
    [OracleAccountDesc] VARCHAR (50)    NOT NULL,
    [Side]              VARCHAR (10)    NOT NULL,
    [LOB]               VARCHAR (5)     NOT NULL,
    [Department]        VARCHAR (5)     NOT NULL,
    [Location]          VARCHAR (5)     NOT NULL,
    [Debit]             DECIMAL (18, 2) NULL,
    [Credit]            DECIMAL (18, 2) NULL,
    [Formula]           VARCHAR (2000)  NOT NULL,
    CONSTRAINT [pk_rpt_journal] PRIMARY KEY CLUSTERED ([AsOfPeriodId] ASC, [Group] ASC, [Team] ASC, [Person] ASC, [Category] ASC, [OracleAccount] ASC, [OracleAccountDesc] ASC, [Side] ASC, [LOB] ASC, [Department] ASC, [Location] ASC, [Formula] ASC)
);



