﻿CREATE TABLE [dbo].[dim_run] (
    [run_id]            INT         IDENTITY (1, 1) NOT NULL,
    [run_dt]            DATETIME    NOT NULL,
    [run_eff_year_no]   INT         NOT NULL,
    [run_eff_month_no]  INT         NOT NULL,
    [run_eff_yyyymm_no] INT         NOT NULL,
    [is_run_locked]     VARCHAR (3) NOT NULL,
    [calc_run_id]       INT         NOT NULL,
    CONSTRAINT [pk_dim_run] PRIMARY KEY CLUSTERED ([run_id] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [uie_dim_run_calc_run_id]
    ON [dbo].[dim_run]([calc_run_id] ASC)
    ON [commission_dw_fg02];

