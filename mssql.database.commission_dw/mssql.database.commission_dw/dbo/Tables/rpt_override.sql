﻿CREATE TABLE [dbo].[rpt_override] (
    [asOfPeriodId]      INT             NOT NULL,
    [group]             NVARCHAR (50)   NOT NULL,
    [team]              NVARCHAR (50)   NOT NULL,
    [person]            NVARCHAR (50)   NOT NULL,
    [category]          NVARCHAR (50)   NOT NULL,
    [oracleAccount]     NVARCHAR (5)    NOT NULL,
    [oracleAccountDesc] NVARCHAR (50)   NOT NULL,
    [lob]               NVARCHAR (5)    NOT NULL,
    [department]        NVARCHAR (5)    NOT NULL,
    [location]          NVARCHAR (5)    NOT NULL,
    [intercompany]      NVARCHAR (255)  NULL,
    [project]           NVARCHAR (255)  NULL,
    [future]            NVARCHAR (255)  NULL,
    [debit]             DECIMAL (18, 2) NULL,
    [credit]            DECIMAL (18, 2) NULL,
    [formula]           NVARCHAR (2000) NOT NULL,
    CONSTRAINT [pk_rpt_override] PRIMARY KEY CLUSTERED ([asOfPeriodId] ASC, [group] ASC, [team] ASC, [person] ASC, [category] ASC, [oracleAccount] ASC, [oracleAccountDesc] ASC, [lob] ASC, [department] ASC, [location] ASC, [formula] ASC)
);

