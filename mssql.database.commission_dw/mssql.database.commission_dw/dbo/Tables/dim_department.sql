﻿CREATE TABLE [dbo].[dim_department] (
    [department_id] INT          IDENTITY (1, 1) NOT NULL,
    [department_nm] VARCHAR (50) NOT NULL,
    CONSTRAINT [pk_dim_department] PRIMARY KEY CLUSTERED ([department_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [uie_dim_department_department_nm]
    ON [dbo].[dim_department]([department_nm] ASC)
    ON [commission_dw_fg02];

