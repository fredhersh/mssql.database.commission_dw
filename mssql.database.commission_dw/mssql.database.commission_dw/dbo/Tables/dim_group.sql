﻿CREATE TABLE [dbo].[dim_group] (
    [group_id] INT          IDENTITY (1, 1) NOT NULL,
    [group_nm] VARCHAR (50) NOT NULL,
    CONSTRAINT [pk_dim_group] PRIMARY KEY CLUSTERED ([group_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [uie_dim_group_group_nm]
    ON [dbo].[dim_group]([group_nm] ASC)
    ON [commission_dw_fg02];

