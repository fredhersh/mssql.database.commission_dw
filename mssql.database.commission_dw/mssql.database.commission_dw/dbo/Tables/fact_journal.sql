﻿CREATE TABLE [dbo].[fact_journal] (
    [run_id]              INT             NOT NULL,
    [as_of_date_id]       INT             NOT NULL,
    [from_account_id]     INT             NOT NULL,
    [from_team_id]        INT             NOT NULL,
    [from_group_id]       INT             NOT NULL,
    [from_oracle_code_id] INT             NOT NULL,
    [to_account_id]       INT             NOT NULL,
    [to_team_id]          INT             NOT NULL,
    [to_group_id]         INT             NOT NULL,
    [to_oracle_code_id]   INT             NOT NULL,
    [reason_id]           INT             NOT NULL,
    [comment_id]          INT             NOT NULL,
    [journal_amt]         DECIMAL (18, 8) NULL,
    CONSTRAINT [pk_fact_journal] PRIMARY KEY CLUSTERED ([run_id] ASC, [as_of_date_id] ASC, [from_account_id] ASC, [from_team_id] ASC, [from_group_id] ASC, [from_oracle_code_id] ASC, [to_account_id] ASC, [to_team_id] ASC, [to_group_id] ASC, [to_oracle_code_id] ASC, [reason_id] ASC, [comment_id] ASC)
);




GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_as_of_date_id]
    ON [dbo].[fact_journal]([as_of_date_id] ASC)
    INCLUDE([run_id], [from_account_id], [from_team_id], [from_group_id], [from_oracle_code_id], [to_account_id], [to_team_id], [to_group_id], [to_oracle_code_id], [reason_id], [comment_id], [journal_amt])
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_from_account_id]
    ON [dbo].[fact_journal]([from_account_id] ASC)
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_from_team_id]
    ON [dbo].[fact_journal]([from_team_id] ASC)
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_from_group_id]
    ON [dbo].[fact_journal]([from_group_id] ASC)
    INCLUDE([run_id], [as_of_date_id], [from_account_id], [from_team_id], [from_oracle_code_id], [to_account_id], [to_team_id], [to_group_id], [to_oracle_code_id], [reason_id], [comment_id], [journal_amt])
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_from_oracle_code_id]
    ON [dbo].[fact_journal]([from_oracle_code_id] ASC)
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_to_account_id]
    ON [dbo].[fact_journal]([to_account_id] ASC)
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_to_team_id]
    ON [dbo].[fact_journal]([to_team_id] ASC)
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_to_group_id]
    ON [dbo].[fact_journal]([to_group_id] ASC)
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_to_oracle_code_id]
    ON [dbo].[fact_journal]([to_oracle_code_id] ASC)
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_reason_id]
    ON [dbo].[fact_journal]([reason_id] ASC)
    INCLUDE([run_id], [as_of_date_id], [from_account_id], [from_team_id], [from_group_id], [from_oracle_code_id], [to_account_id], [to_team_id], [to_group_id], [to_oracle_code_id], [comment_id], [journal_amt])
    ON [commission_dw_fg02];


GO
CREATE NONCLUSTERED INDEX [ie_fact_journal_comment_id]
    ON [dbo].[fact_journal]([comment_id] ASC)
    ON [commission_dw_fg02];

