﻿CREATE TABLE [dbo].[dim_account] (
    [account_id]  INT          IDENTITY (1, 1) NOT NULL,
    [account_nm]  VARCHAR (50) NOT NULL,
    [account_typ] VARCHAR (20) NOT NULL,
    CONSTRAINT [pk_dim_account] PRIMARY KEY CLUSTERED ([account_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [uie_dim_account_account_nm]
    ON [dbo].[dim_account]([account_nm] ASC)
    ON [commission_dw_fg02];

