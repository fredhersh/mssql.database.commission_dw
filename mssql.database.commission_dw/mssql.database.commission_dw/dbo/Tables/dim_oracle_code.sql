﻿CREATE TABLE [dbo].[dim_oracle_code] (
    [oracle_code_id]   INT           IDENTITY (1, 1) NOT NULL,
    [department_cd]    VARCHAR (3)   NULL,
    [department_descr] VARCHAR (100) NULL,
    [lob_cd]           VARCHAR (2)   NULL,
    [lob_descr]        VARCHAR (100) NULL,
    [location_cd]      VARCHAR (3)   NULL,
    [location_descr]   VARCHAR (100) NULL,
    CONSTRAINT [pk_dim_oracle_code] PRIMARY KEY CLUSTERED ([oracle_code_id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [uie_dim_oracle_code]
    ON [dbo].[dim_oracle_code]([department_cd] ASC, [lob_cd] ASC, [location_cd] ASC)
    ON [commission_dw_fg02];

