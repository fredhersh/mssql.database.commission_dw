﻿



CREATE PROCEDURE [dbo].[refresh_dim_team]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the Team dimension
**	table (dbo.dim_team).  It is primarily used as a team definition.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create dim_team if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.schema_id = s.schema_id WHERE t.name = 'dim_team' AND t.type='U' AND s.name = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_team
	(team_id		INT IDENTITY(1,1) NOT NULL
	,team_nm		VARCHAR(50) NOT NULL
	CONSTRAINT pk_dim_team PRIMARY KEY CLUSTERED 
	(
		team_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Insert Default team record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_team WHERE team_id = 0)
BEGIN

	SET IDENTITY_INSERT dbo.dim_team ON

	INSERT	dbo.dim_team
	(team_id
	,team_nm
	)
	SELECT	team_id = 0
	,		team_nm = 'unknown'

	SET IDENTITY_INSERT dbo.dim_team OFF

END

/*
** Insert Non-Applicable team record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_team WHERE team_id = 1)
BEGIN

	SET IDENTITY_INSERT dbo.dim_team ON

	INSERT	dbo.dim_team
	(team_id
	,team_nm
	)
	SELECT	team_id = 1
	,		team_nm = 'non applicable'

	SET IDENTITY_INSERT dbo.dim_team OFF

END

/*
** Create temp table for teams
*/

CREATE TABLE #teams
(team_nm	VARCHAR(50)
)

/*
** Populate temporary table of teams
*/

INSERT	#teams
(team_nm
)
SELECT DISTINCT	team_nm = lower(cts.team_nm)
FROM			[$(commission)].out.calc_team_setup cts

/*
** Populate team table
*/

INSERT	dbo.dim_team
(		team_nm
)
SELECT	t.team_nm
FROM	#teams t LEFT JOIN dim_team da ON (t.team_nm = da.team_nm)
WHERE	da.team_nm IS NULL

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_team' AND object_id = OBJECT_ID('dbo.dim_team'))
BEGIN
	ALTER TABLE dbo.dim_team ADD  CONSTRAINT pk_dim_team PRIMARY KEY CLUSTERED (team_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'uie_dim_team_team_nm' AND object_id = OBJECT_ID('dbo.dim_team'))
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX uie_dim_team_team_nm ON dbo.dim_team(team_nm ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

/*
** Drop temporary table
*/

IF (OBJECT_ID('tempdb..#teams')) IS NOT NULL
	DROP TABLE #teams




