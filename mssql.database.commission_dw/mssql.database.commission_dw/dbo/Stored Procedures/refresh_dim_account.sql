﻿


CREATE PROCEDURE [dbo].[refresh_dim_account]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the Account dimension
**	table (dbo.dim_account).  It is primarily used as a account definition.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create dim_account if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.schema_id = s.schema_id WHERE t.name = 'dim_account' AND t.type='U' AND s.name = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_account
	(account_id		INT IDENTITY(1,1) NOT NULL
	,account_nm		VARCHAR(50) NOT NULL
	,account_typ	VARCHAR(20) NOT NULL
	CONSTRAINT pk_dim_account PRIMARY KEY CLUSTERED 
	(
		account_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Insert Default Account record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_account WHERE account_id = 0)
BEGIN

	SET IDENTITY_INSERT dbo.dim_account ON

	INSERT	dbo.dim_account
	(account_id
	,account_nm
	,account_typ
	)
	SELECT	account_id = 0
	,		account_nm = 'unknown'
	,		account_typ = 'unknown'

	SET IDENTITY_INSERT dbo.dim_account OFF

END

/*
** Insert PTCP Account record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_account WHERE account_id = 1)
BEGIN

	SET IDENTITY_INSERT dbo.dim_account ON

	INSERT	dbo.dim_account
	(account_id
	,account_nm
	,account_typ
	)
	SELECT	account_id = 1
	,		account_nm = 'ptcp'
	,		account_typ = 'organization'

	SET IDENTITY_INSERT dbo.dim_account OFF

END

/*
** Create temp table for accounts
*/

CREATE TABLE #accounts
(account_nm		VARCHAR(50)
,account_typ	VARCHAR(20)
)

/*
** Populate temporary table of accounts
*/

INSERT	#accounts
(account_nm
,account_typ
)
SELECT DISTINCT	account_nm = lower(cps.person_cd)
,				account_typ = 'person'
FROM			[$(commission)].out.calc_person_setup cps

UNION

SELECT DISTINCT	account_nm = lower(cts.team_nm)
,				account_typ = 'organization'
FROM			[$(commission)].out.calc_team_setup cts

/*
** Populate Account table
*/

INSERT	dbo.dim_account
(		account_nm
,		account_typ
)
SELECT	t.account_nm
,	    t.account_typ
FROM	#accounts t LEFT JOIN dim_account da ON (t.account_nm = da.account_nm)
WHERE	da.account_nm IS NULL

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_account' AND object_id = OBJECT_ID('dbo.dim_account'))
BEGIN
	ALTER TABLE dbo.dim_account ADD  CONSTRAINT pk_dim_account PRIMARY KEY CLUSTERED (account_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'uie_dim_account_account_nm' AND object_id = OBJECT_ID('dbo.dim_account'))
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX uie_dim_account_account_nm ON dbo.dim_account(account_nm ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

/*
** Drop temporary table
*/

IF (OBJECT_ID('tempdb..#accounts')) IS NOT NULL
	DROP TABLE #accounts



