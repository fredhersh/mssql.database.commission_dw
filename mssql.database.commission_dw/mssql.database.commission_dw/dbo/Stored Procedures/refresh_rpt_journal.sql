﻿

CREATE PROCEDURE [dbo].[refresh_rpt_journal] AS

/************************************************************************************************
**
**	This procedure refreshes persisted data required for the Journal Report.  The logic was
**	originally supplied by Phil K.
**
**	FHersh		01/02/2020		Created.
**	FHersh		01/03/2020		Added Optimize For Unknown option.
**  PKrawchuk   01/03/2020      Added Franco's 1%
**  PKrawchuk   01/07/2020      Added New Account Kicker. Removed Money conversions, since all data now rounded to two decimals.
**  PKrawchuk   01/08/2020      Added New Account Kicker. Removed Money conversions, since all data now rounded to two decimals.
**	FHersh		01/13/2020		Removed hardcoded @AsOfPeriodId for #Bonuses.
**	FHersh		01/14/2020		Default NULL to Unknown for LOB, Department and Location for
**								3 - Accrued Bonuses / 62030.
**	FHersh		01/20/2020		Added logic to support commissionable_revenue*_amt (per Saket S.).
**	FHersh		01/21/2020		For commissionable_revenue*_amt use 43151 for OracleAccount (per Saket S.).
**	FHersh		02/27/2020		Adjustments in Category 2 - Sales Assistant Bonuses (made by Phil K.).
**
************************************************************************************************/


SET NOCOUNT ON

/*
** Create rpt_journal if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.SCHEMA_ID = s.schema_id WHERE t.name = 'rpt_journal' AND t.type='U' AND s.NAME = 'dbo')
BEGIN

	CREATE TABLE dbo.rpt_journal
	([AsOfPeriodId]			INT NOT NULL
	,[Group]				VARCHAR(50) NOT NULL
	,[Team]					VARCHAR(50) NOT NULL
	,[Person]				VARCHAR(50) NOT NULL
	,[Category]				VARCHAR(50) NOT NULL
	,[OracleAccount]		VARCHAR(5) NOT NULL
	,[OracleAccountDesc]	VARCHAR(50) NOT NULL
	,[Side]					VARCHAR(10) NOT NULL
	,[LOB]					VARCHAR(5) NOT NULL
	,[Department]			VARCHAR(5) NOT NULL
	,[Location]				VARCHAR(5) NOT NULL
	,[Debit]				DECIMAL(18, 2) NULL
	,[Credit]				DECIMAL(18, 2) NULL
	,[Formula]				VARCHAR(2000) NOT NULL
	CONSTRAINT pk_rpt_journal PRIMARY KEY CLUSTERED 
	(
		[AsOfPeriodId] ASC,
		[Group] ASC,
		[Team] ASC,
		[Person] ASC,
		[Category] ASC,
		[OracleAccount]	ASC,
		[OracleAccountDesc] ASC,
		[Side] ASC,
		[LOB] ASC,
		[Department] ASC,
		[Location] ASC,
		[Formula] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Truncate journal report table
*/

TRUNCATE TABLE rpt_journal

---Make a temp table for the salaries.
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'61020' AS [OracleAccount],
	'Salary Expense Sharing' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN salary_amt < 0 THEN -salary_amt ELSE 0 END) as Debit,
	(CASE WHEN salary_amt > 0 THEN salary_amt ELSE 0 END) AS Credit,
	'salary_amt for ' + to_account_nm + ' (' + FORMAT(salary_amt, 'N2') + ')' AS Formula
INTO #Salaries
FROM [complete_bucket_salary_expense_sharing]
WHERE from_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND salary_amt <> 0


---Make a temp table for the bonuses. Intentionally only permitting positive values to go here.
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'64040' AS [OracleAccount],
	'Bonus Expense Sharing' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN ((Case When adhoc_reserve_amt <> 0 then 0 Else bonus_reserve_amt + bonus_reserve_rate_cost_amt End) + adhoc_bonus_amt + bonus_amount_total_amt + bonus_rate_cost_amt - adhoc_reserve_amt + accrued_bonus_adj_amt ) < 0 THEN -((Case When adhoc_reserve_amt <> 0 then 0 Else bonus_reserve_amt + bonus_reserve_rate_cost_amt End) + adhoc_bonus_amt + bonus_amount_total_amt + bonus_rate_cost_amt - adhoc_reserve_amt + accrued_bonus_adj_amt ) ELSE 0 END) as Debit,
	(CASE WHEN ((Case When adhoc_reserve_amt <> 0 then 0 Else bonus_reserve_amt + bonus_reserve_rate_cost_amt End) + adhoc_bonus_amt + bonus_amount_total_amt + bonus_rate_cost_amt - adhoc_reserve_amt + accrued_bonus_adj_amt ) > 0 THEN ((Case When adhoc_reserve_amt <> 0 then 0 Else bonus_reserve_amt + bonus_reserve_rate_cost_amt End) + adhoc_bonus_amt + bonus_amount_total_amt + bonus_rate_cost_amt - adhoc_reserve_amt + accrued_bonus_adj_amt ) ELSE 0 END) AS Credit,
	'((Case When adhoc_reserve_amt(' + FORMAT(adhoc_reserve_amt, 'N2') + ') <> 0 then 0 Else bonus_reserve_amt(' + FORMAT(bonus_reserve_amt, 'N2') + ') + bonus_reserve_rate_cost_amt(' + FORMAT(bonus_reserve_rate_cost_amt, 'N2') + ') End) + adhoc_bonus_amt(' + FORMAT(adhoc_bonus_amt, 'N2') + ') + bonus_amount_total_amt(' + FORMAT(bonus_amount_total_amt, 'N2') + ') + bonus_rate_cost_amt(' + FORMAT(bonus_rate_cost_amt, 'N2') + ') - adhoc_reserve_amt(' + FORMAT(adhoc_reserve_amt, 'N2') + ') + accrued_bonus_adj_amt(' + FORMAT(accrued_bonus_adj_amt, 'N2') + ')' AS Formula	 
INTO #Bonuses
FROM [complete_bucket_bonus_expense_sharing]
WHERE from_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND ((Case When adhoc_reserve_amt <> 0 then 0 Else bonus_reserve_amt + bonus_reserve_rate_cost_amt End) + adhoc_bonus_amt + bonus_amount_total_amt + bonus_rate_cost_amt - adhoc_reserve_amt + accrued_bonus_adj_amt ) > 0


----------------------------SALES CREDIT------------------------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------
---------------CREDIT SIDE. 43100 AND 43105
-----------------------------------------------------------------------------------------------------------------
INSERT	rpt_journal
([AsOfPeriodId]
,[Group]
,[Team]
,[Person]
,[Category]
,[OracleAccount]
,[OracleAccountDesc]
,[Side]
,[LOB]
,[Department]
,[Location]
,[Debit]
,[Credit]
,[Formula]
)
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Adjustments)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_adj_amt < 0 THEN -commissionable_revenue_adj_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_adj_amt > 0 THEN commissionable_revenue_adj_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_adj_amt(' + FORMAT(commissionable_revenue_adj_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_adj_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Bullet and Niche)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) < 0 THEN -(commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) ELSE 0 END) AS Debit,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) > 0 THEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_bullets_amt(' + FORMAT(commissionable_revenue_slice_bullets_amt, 'N2') + ') + commissionable_revenue_slice_niche_amt(' + FORMAT(commissionable_revenue_slice_niche_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Credit)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_credit_amt < 0 THEN -commissionable_revenue_slice_credit_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_credit_amt > 0 THEN commissionable_revenue_slice_credit_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_credit_amt(' + FORMAT(commissionable_revenue_slice_credit_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_credit_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Muni)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_muni_amt < 0 THEN -commissionable_revenue_slice_muni_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_muni_amt > 0 THEN commissionable_revenue_slice_muni_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_muni_amt(' + FORMAT(commissionable_revenue_slice_muni_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_muni_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (ResiMBS)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt < 0 THEN -commissionable_revenue_slice_resi_mbs_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt > 0 THEN commissionable_revenue_slice_resi_mbs_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_resi_mbs_amt(' + FORMAT(commissionable_revenue_slice_resi_mbs_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_resi_mbs_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Other)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_other_amt < 0 THEN -commissionable_revenue_slice_other_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_other_amt > 0 THEN commissionable_revenue_slice_other_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_other_amt(' + FORMAT(commissionable_revenue_slice_other_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_other_amt <> 0

UNION ALL 

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43105' AS [OracleAccount],
	'Sales Credit (Capital Markets)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt < 0 THEN -commissionable_revenue_slice_cap_mkts_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt > 0 THEN commissionable_revenue_slice_cap_mkts_amt ELSE 0 END) AS Credit,
	--'commissionable_revenue_slice_cap_mkts_amt(' + FORMAT(commissionable_revenue_slice_cap_mkts_amt, 'N2') + ')' AS Formula
	'non_oms_sales_credit_amt(' + FORMAT(non_oms_sales_credit_amt, 'N2') + '), gross_commission_loan_amt(' + FORMAT(gross_commission_loan_amt, 'N2') + ') + Portion of trades from Sales Credit (commission_val) and Shared Sales Credit (shared_commission_val)' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_cap_mkts_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Adjustments)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_adj_amt < 0 THEN -commissionable_revenue_adj_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_adj_amt > 0 THEN commissionable_revenue_adj_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_adj_amt(' + FORMAT(commissionable_revenue_adj_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_adj_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Bullet and Niche)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) < 0 THEN -(commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) ELSE 0 END) AS Debit,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) > 0 THEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_bullets_amt(' + FORMAT(commissionable_revenue_slice_bullets_amt, 'N2') + ') + commissionable_revenue_slice_niche_amt(' + FORMAT(commissionable_revenue_slice_niche_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Credit)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_credit_amt < 0 THEN -commissionable_revenue_slice_credit_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_credit_amt > 0 THEN commissionable_revenue_slice_credit_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_credit_amt(' + FORMAT(commissionable_revenue_slice_credit_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_credit_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Muni)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_muni_amt < 0 THEN -commissionable_revenue_slice_muni_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_muni_amt > 0 THEN commissionable_revenue_slice_muni_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_muni_amt(' + FORMAT(commissionable_revenue_slice_muni_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_muni_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (ResiMBS)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt < 0 THEN -commissionable_revenue_slice_resi_mbs_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt > 0 THEN commissionable_revenue_slice_resi_mbs_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_resi_mbs_amt(' + FORMAT(commissionable_revenue_slice_resi_mbs_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_resi_mbs_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Other)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_other_amt < 0 THEN -commissionable_revenue_slice_other_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_other_amt > 0 THEN commissionable_revenue_slice_other_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_other_amt(' + FORMAT(commissionable_revenue_slice_other_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_other_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43105' AS [OracleAccount],
	'Sales Credit (Capital Markets)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt < 0 THEN -commissionable_revenue_slice_cap_mkts_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt > 0 THEN commissionable_revenue_slice_cap_mkts_amt ELSE 0 END) AS Credit,
	--'commissionable_revenue_slice_cap_mkts_amt(' + FORMAT(commissionable_revenue_slice_cap_mkts_amt, 'N2') + ')' AS Formula
	'non_oms_sales_credit_amt(' + FORMAT(non_oms_sales_credit_amt, 'N2') + '), gross_commission_loan_amt(' + FORMAT(gross_commission_loan_amt, 'N2') + ') + Portion of trades from Sales Credit (commission_val) and Shared Sales Credit (shared_commission_val)' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_cap_mkts_amt <> 0

UNION ALL

-----------------------------------------------------------------------------------------------------------------
---------------DEBIT SIDE. 43150 AND 43151
-----------------------------------------------------------------------------------------------------------------
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Adjustments)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_adj_amt > 0 THEN commissionable_revenue_adj_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_adj_amt < 0 THEN -commissionable_revenue_adj_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_adj_amt(' + FORMAT(commissionable_revenue_adj_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_adj_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Bullet and Niche)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) > 0 THEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) ELSE 0 END) AS Debit,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) < 0 THEN -(commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_bullets_amt(' + FORMAT(commissionable_revenue_slice_bullets_amt, 'N2') + ') + commissionable_revenue_slice_niche_amt(' + FORMAT(commissionable_revenue_slice_niche_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Credit)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_credit_amt > 0 THEN commissionable_revenue_slice_credit_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_credit_amt < 0 THEN -commissionable_revenue_slice_credit_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_credit_amt(' + FORMAT(commissionable_revenue_slice_credit_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_credit_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Muni)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_muni_amt > 0 THEN commissionable_revenue_slice_muni_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_muni_amt < 0 THEN -commissionable_revenue_slice_muni_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_muni_amt(' + FORMAT(commissionable_revenue_slice_muni_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_muni_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (ResiMBS)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt > 0 THEN commissionable_revenue_slice_resi_mbs_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt < 0 THEN -commissionable_revenue_slice_resi_mbs_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_resi_mbs_amt(' + FORMAT(commissionable_revenue_slice_resi_mbs_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_resi_mbs_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Other)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_other_amt > 0 THEN commissionable_revenue_slice_other_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_other_amt < 0 THEN -commissionable_revenue_slice_other_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_other_amt(' + FORMAT(commissionable_revenue_slice_other_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_other_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Capital Markets)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'05' AS LOB, --4315_'s have hardcoded Oracle codes 
	'205' AS Department,
	'000' AS Location,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt > 0 THEN commissionable_revenue_slice_cap_mkts_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt < 0 THEN -commissionable_revenue_slice_cap_mkts_amt ELSE 0 END) AS Credit,
	--'commissionable_revenue_slice_cap_mkts_amt(' + FORMAT(commissionable_revenue_slice_cap_mkts_amt, 'N2') + ')' AS Formula
	'non_oms_sales_credit_amt(' + FORMAT(non_oms_sales_credit_amt, 'N2') + '), gross_commission_loan_amt(' + FORMAT(gross_commission_loan_amt, 'N2') + ') + Portion of trades from Sales Credit (commission_val) and Shared Sales Credit (shared_commission_val)' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_cap_mkts_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Adjustments)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_adj_amt > 0 THEN commissionable_revenue_adj_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_adj_amt < 0 THEN -commissionable_revenue_adj_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_adj_amt(' + FORMAT(commissionable_revenue_adj_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_adj_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Bullet and Niche)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) > 0 THEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) ELSE 0 END) AS Debit,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) < 0 THEN -(commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_bullets_amt(' + FORMAT(commissionable_revenue_slice_bullets_amt, 'N2') + ') + commissionable_revenue_slice_niche_amt(' + FORMAT(commissionable_revenue_slice_niche_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND (commissionable_revenue_slice_bullets_amt + commissionable_revenue_slice_niche_amt) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Credit)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_credit_amt > 0 THEN commissionable_revenue_slice_credit_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_credit_amt < 0 THEN -commissionable_revenue_slice_credit_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_credit_amt(' + FORMAT(commissionable_revenue_slice_credit_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_credit_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Muni)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_muni_amt > 0 THEN commissionable_revenue_slice_muni_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_muni_amt < 0 THEN -commissionable_revenue_slice_muni_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_muni_amt(' + FORMAT(commissionable_revenue_slice_muni_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_muni_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (ResiMBS)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt > 0 THEN commissionable_revenue_slice_resi_mbs_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt < 0 THEN -commissionable_revenue_slice_resi_mbs_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_resi_mbs_amt(' + FORMAT(commissionable_revenue_slice_resi_mbs_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_resi_mbs_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Other)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_other_amt > 0 THEN commissionable_revenue_slice_other_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_other_amt < 0 THEN -commissionable_revenue_slice_other_amt ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_other_amt(' + FORMAT(commissionable_revenue_slice_other_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_other_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Capital Markets)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'05' AS LOB, --4315_'s have hardcoded Oracle codes 
	'205' AS Department,
	'000' AS Location,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt > 0 THEN commissionable_revenue_slice_cap_mkts_amt ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt < 0 THEN -commissionable_revenue_slice_cap_mkts_amt ELSE 0 END) AS Credit,
	-- 'commissionable_revenue_slice_cap_mkts_amt(' + FORMAT(commissionable_revenue_slice_cap_mkts_amt, 'N2') + ')' AS Formula
	'non_oms_sales_credit_amt(' + FORMAT(non_oms_sales_credit_amt, 'N2') + '), gross_commission_loan_amt(' + FORMAT(gross_commission_loan_amt, 'N2') + ') + Portion of trades from Sales Credit (commission_val) and Shared Sales Credit (shared_commission_val)' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND commissionable_revenue_slice_cap_mkts_amt <> 0

UNION ALL

-------------------------------------------------------------------------------------------ACCRUED COMP----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
---------------CREDIT SIDE. 62100 63020 62020 61020 64040 21250 13200 62050 
-----------------------------------------------------------------------------------------------------------------

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62100' AS [OracleAccount],
	'IB Referral Expense' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN ib_referral_expense_amt > 0 THEN ib_referral_expense_amt ELSE 0 END) as Debit, --This field has weird signing because it is negative
	(CASE WHEN ib_referral_expense_amt < 0 THEN -ib_referral_expense_amt ELSE 0 END) AS Credit,
	'ib_referral_expense_amt(' + FORMAT(ib_referral_expense_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_compensation]
WHERE to_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND ib_referral_expense_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'63020' AS [OracleAccount],
	'Draw Reimbursed' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN draw_amt < 0 THEN -draw_amt ELSE 0 END) as Debit,
	(CASE WHEN draw_amt > 0 THEN draw_amt ELSE 0 END) AS Credit,
	'draw_amt(' + FORMAT(draw_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_draw_guarantee]
WHERE from_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND draw_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62020' AS [OracleAccount],
	'Commissions - House Share' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN monthly_guarantee_amt < 0 THEN -monthly_guarantee_amt ELSE 0 END) as Debit,
	(CASE WHEN monthly_guarantee_amt > 0 THEN monthly_guarantee_amt ELSE 0 END) AS Credit,
	'monthly_guarantee_amt(' + FORMAT(monthly_guarantee_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_draw_guarantee]
WHERE from_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND monthly_guarantee_amt <> 0

UNION ALL

SELECT * FROM #Salaries

UNION ALL

SELECT * FROM #Bonuses

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'21250' AS [OracleAccount],
	'Accrued Compensation' AS [OracleAccountDesc],
	'Credit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	(CASE WHEN ((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0 THEN -((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Debit,
	(CASE WHEN ((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) > 0 THEN ((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Credit,
	'journal_gross_amt(' + FORMAT(journal_gross_amt, 'N2') + ') + post_commission_rate_adj_amt(' + FORMAT(post_commission_rate_adj_amt, 'N2') + ') - fines_amt(' + FORMAT(fines_amt, 'N2') + ') + ib_referral_expense_amt(' + FORMAT(ib_referral_expense_amt, 'N2') + ') - draw_amt(' + FORMAT(draw_amt, 'N2') + ') - monthly_guarantee_amt(' + FORMAT(monthly_guarantee_amt, 'N2') + ') - (all analyst salaries(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id))) + ')) - (all analyst bonuses(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) + '))'  AS Formula
FROM [complete_bucket_accrued_compensation] cbac
WHERE from_account_nm = 'ptcp'
	AND to_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND ((journal_gross_amt + [journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt] + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) > 0

UNION ALL

--If someone on a Draw didn't meet their draw
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'13200' AS [OracleAccount],
	'Advances To Employees' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN ((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0 THEN -((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Debit,
	(CASE WHEN ((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) > 0 THEN ((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Credit,
	'journal_gross_amt(' + FORMAT(journal_gross_amt, 'N2') + ') + post_commission_rate_adj_amt(' + FORMAT(post_commission_rate_adj_amt, 'N2') + ') - fines_amt(' + FORMAT(fines_amt, 'N2') + ') + ib_referral_expense_amt(' + FORMAT(ib_referral_expense_amt, 'N2') + ') - draw_amt(' + FORMAT(draw_amt, 'N2') + ') - (all analyst salaries(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id))) + ')) - (all analyst bonuses(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) + '))'  AS Formula
FROM [complete_bucket_accrued_compensation] cbac
WHERE from_account_nm = 'ptcp'
	AND to_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND ((journal_gross_amt + [journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt] + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - draw_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0
	AND draw_amt <> 0 

UNION ALL

--We need to DEBIT Accrued Comp when people don't meet there guarantee
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'21250' AS [OracleAccount],
	'Accrued Compensation (Un-met guarantee offset)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	(CASE WHEN ((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0 THEN -((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Debit,
	(CASE WHEN ((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) > 0 THEN ((journal_gross_amt + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Credit,
	'journal_gross_amt(' + FORMAT(journal_gross_amt, 'N2') + ') + post_commission_rate_adj_amt(' + FORMAT(post_commission_rate_adj_amt, 'N2') + ') - fines_amt(' + FORMAT(fines_amt, 'N2') + ') + ib_referral_expense_amt(' + FORMAT(ib_referral_expense_amt, 'N2') + ') - monthly_guarantee_amt(' + FORMAT(monthly_guarantee_amt, 'N2') + ') - (all analyst salaries(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id))) + ')) - (all analyst bonuses(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) + '))'  AS Formula
FROM [complete_bucket_accrued_compensation] cbac
WHERE from_account_nm = 'ptcp'
	AND to_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND ((journal_gross_amt + [journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt] + post_commission_rate_adj_amt - fines_amt + ib_referral_expense_amt - monthly_guarantee_amt - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62050' AS [OracleAccount],
	'Miscellaneous Deductions (FINES)' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN fines_amt < 0 THEN -fines_amt ELSE 0 END) as Debit, 
	(CASE WHEN fines_amt > 0 THEN fines_amt ELSE 0 END) AS Credit,
	'fines_amt(' + FORMAT(fines_amt, 'N2') + ')'
FROM [complete_bucket_accrued_compensation]
WHERE to_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND fines_amt <> 0

UNION ALL

--Excluding Grier and Jones, because they have a special thing.
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62050' AS [OracleAccount],
	'Miscellaneous Deductions (Other adj)' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN post_commission_rate_adj_amt > 0 THEN post_commission_rate_adj_amt ELSE 0 END) as Debit,
	(CASE WHEN post_commission_rate_adj_amt < 0 THEN -post_commission_rate_adj_amt ELSE 0 END) AS Credit,
	'post_commission_rate_adj_amt(' + FORMAT(post_commission_rate_adj_amt, 'N2') + ')'
FROM [complete_bucket_accrued_compensation]
WHERE to_group_nm <> 'IB'
	AND to_account_nm NOT IN ('kgrier','cjones')
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND post_commission_rate_adj_amt <> 0

UNION ALL

--Special KGRIER thing
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62031' AS [OracleAccount],
	'Other Bonus' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN post_commission_rate_adj_amt > 0 THEN post_commission_rate_adj_amt ELSE 0 END) as Debit, 
	(CASE WHEN post_commission_rate_adj_amt < 0 THEN -post_commission_rate_adj_amt ELSE 0 END) AS Credit,
	'post_commission_rate_adj_amt(' + FORMAT(post_commission_rate_adj_amt, 'N2') + ')'
FROM [complete_bucket_accrued_compensation]
WHERE 
	to_account_nm = 'kgrier'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND post_commission_rate_adj_amt <> 0

UNION ALL

--Special CJONES thing
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'64040' AS [OracleAccount],
	'Bonus Expense Sharing' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN post_commission_rate_adj_amt > 0 THEN post_commission_rate_adj_amt ELSE 0 END) as Debit,
	(CASE WHEN post_commission_rate_adj_amt < 0 THEN -post_commission_rate_adj_amt ELSE 0 END) AS Credit,
	'post_commission_rate_adj_amt(' + FORMAT(post_commission_rate_adj_amt, 'N2') + ')'
FROM [complete_bucket_accrued_compensation]
WHERE 
	to_account_nm = 'cjones'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND post_commission_rate_adj_amt <> 0

UNION ALL

-----------------------------------------------------------------------------------------------------------------
---------------DEBIT SIDE. 62010
-----------------------------------------------------------------------------------------------------------------
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62010' AS [OracleAccount],
	'Commissions - Gross' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN journal_gross_amt > 0 THEN journal_gross_amt ELSE 0 END) as Debit,
	(CASE WHEN journal_gross_amt < 0 THEN -journal_gross_amt ELSE 0 END) AS Credit,
	'journal_gross_amt(' + FORMAT(journal_gross_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_compensation]
WHERE to_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND journal_gross_amt <> 0

UNION ALL

-------------------------------------------------------------------------ACCRUED BONUSES----------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
---------------CREDIT SIDE. 21202
-----------------------------------------------------------------------------------------------------------------
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'21202' AS [OracleAccount],
	'Accrued Bonuses - Commissions' AS [OracleAccountDesc],
	'Credit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	(CASE WHEN ([journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt]) < 0 THEN ([journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt]) ELSE 0 END) as Debit,
	(CASE WHEN ([journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt]) > 0 THEN ([journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt]) ELSE 0 END) as Credit,
	'journal_bonus_amt(' + CONVERT(VARCHAR(MAX),([journal_bonus_amt])) + ') + ib_referral_bonus_amt(' + CONVERT(VARCHAR(MAX),([ib_referral_bonus_amt])) + ') + journal_collective_bonus_amt(' + CONVERT(VARCHAR(MAX),([journal_collective_bonus_amt])) + ') + pt_score_referral_bonus_amt(' + CONVERT(VARCHAR(MAX),([pt_score_referral_bonus_amt])) + ') + new_account_bonus_kicker_amt(' + CONVERT(VARCHAR(MAX),([new_account_bonus_kicker_amt])) + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND [journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt] <> 0

UNION ALL

--ebrown's offset really is to 21250
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'21250' AS [OracleAccount],
	'Accrued Compensation (EPB)' AS [OracleAccountDesc],
	'Credit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	(CASE WHEN ([journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt]) < 0 THEN ([journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt]) ELSE 0 END) as Debit,
	(CASE WHEN ([journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt]) > 0 THEN ([journal_bonus_amt] + [ib_referral_bonus_amt] + [journal_collective_bonus_amt] + [pt_score_referral_bonus_amt] + [new_account_bonus_kicker_amt]) ELSE 0 END) as Credit,
	'journal_bonus_amt(' + CONVERT(VARCHAR(MAX),([journal_bonus_amt])) + ') + ib_referral_bonus_amt(' + CONVERT(VARCHAR(MAX),([ib_referral_bonus_amt])) + ') + journal_collective_bonus_amt(' + CONVERT(VARCHAR(MAX),([journal_collective_bonus_amt])) + ') + pt_score_referral_bonus_amt(' + CONVERT(VARCHAR(MAX),([pt_score_referral_bonus_amt])) + ') + new_account_bonus_kicker_amt(' + CONVERT(VARCHAR(MAX),([new_account_bonus_kicker_amt])) + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND to_group_nm <> 'IB' AND to_account_nm = 'ebrown'

UNION ALL

--Franco's 1%
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	'team fd' AS Team,
	'fdaniele' AS Person,
	'5 - Bonuses Revenue Based' AS Category,
	'21200' AS [OracleAccount],
	'Accrued Bonuses' AS [OracleAccountDesc],
	'Credit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	0 as Debit,
	(SUM(trade_desk_amt) + SUM(trade_desk_adj_amt) + SUM(non_oms_sales_credit_amt) + SUM(capital_mkt_amt) + SUM(capital_mkt_adj_amt)) * .01 as Credit,
	'(trade_desk_amt(' + CONVERT(VARCHAR(MAX),SUM(trade_desk_amt)) + ') + trade_desk_adj_amt( ' + CONVERT(VARCHAR(MAX),SUM(trade_desk_adj_amt)) + ') + non_oms_sales_credit_amt(' + CONVERT(VARCHAR(MAX),SUM(non_oms_sales_credit_amt)) + ') + capital_mkt_amt(' + CONVERT(VARCHAR(MAX),SUM(capital_mkt_amt)) + ') + capital_mkt_adj_amt (' + CONVERT(VARCHAR(MAX),SUM(capital_mkt_adj_amt)) + ')) * .01, for IG' AS Formula
FROM [complete_bucket_sales_credit]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND from_group_nm = 'IG'
GROUP BY as_of_period_id, from_group_nm

UNION ALL

-----------------------------------------------------------------------------------------------------------------
---------------DEBIT SIDE. 62030 62100 62032 62100
-----------------------------------------------------------------------------------------------------------------
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62030' AS [OracleAccount],
	'New Account Bonus' AS [OracleAccountDesc],
	'Debit' AS Side,
	ISNULL(to_lob_cd, 'Unknown') AS LOB,
	ISNULL(to_department_cd, 'Unknown') AS Department,
	ISNULL(to_location_cd, 'Unknown') AS Location,	
	(CASE WHEN ([journal_bonus_amt]) > 0 THEN ([journal_bonus_amt]) ELSE 0 END) as Debit,
	(CASE WHEN ([journal_bonus_amt]) < 0 THEN -([journal_bonus_amt]) ELSE 0 END) as Credit,
	'journal_bonus_amt(' + CONVERT(VARCHAR(MAX),([journal_bonus_amt])) + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND [journal_bonus_amt] <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62100' AS [OracleAccount],
	'IB Referral Bonus' AS [OracleAccountDesc],
	'Debit' AS Side,
	'04' AS LOB, --62100 has hardcoded Oracle values.
	'204' AS Department,
	'001' AS Location,
	(CASE WHEN ib_referral_bonus_amt > 0 THEN ib_referral_bonus_amt ELSE 0 END) as Debit,
	(CASE WHEN ib_referral_bonus_amt < 0 THEN -ib_referral_bonus_amt ELSE 0 END) AS Credit,
	'ib_referral_bonus_amt(' + FORMAT(ib_referral_bonus_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND ib_referral_bonus_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62032' AS [OracleAccount],
	'Collective Bonus' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN journal_collective_bonus_amt > 0 THEN journal_collective_bonus_amt ELSE 0 END) as Debit,
	(CASE WHEN journal_collective_bonus_amt < 0 THEN -journal_collective_bonus_amt ELSE 0 END) AS Credit,
	'journaled_collective_bonus_amt(' + FORMAT(journal_collective_bonus_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND journal_collective_bonus_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62032' AS [OracleAccount],
	'Collective Bonus New Account' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN new_account_bonus_kicker_amt > 0 THEN new_account_bonus_kicker_amt ELSE 0 END) as Debit,
	(CASE WHEN new_account_bonus_kicker_amt < 0 THEN -new_account_bonus_kicker_amt ELSE 0 END) AS Credit,
	'new_account_bonus_kicker_amt(' + FORMAT(new_account_bonus_kicker_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND new_account_bonus_kicker_amt <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62100' AS [OracleAccount],
	'PT Score Referral Bonus' AS [OracleAccountDesc],
	'Debit' AS Side,
	'07' AS LOB, --62100 has hardcoded Oracle values.
	'207' AS Department,
	'001' AS Location,
	(CASE WHEN pt_score_referral_bonus_amt > 0 THEN pt_score_referral_bonus_amt ELSE 0 END) as Debit,
	(CASE WHEN pt_score_referral_bonus_amt < 0 THEN -pt_score_referral_bonus_amt ELSE 0 END) AS Credit,
	'pt_score_referral_bonus_amt(' + FORMAT(pt_score_referral_bonus_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND pt_score_referral_bonus_amt <> 0

UNION ALL

--Special EPB recoding.
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'5 - Bonuses Revenue Based' AS Category,
	'64020' AS [OracleAccount],
	'Bonus - Revenue Based' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN ([journal_bonus_amt]) > 0 THEN ([journal_bonus_amt]) ELSE 0 END) as Debit,
	(CASE WHEN ([journal_bonus_amt]) < 0 THEN -([journal_bonus_amt]) ELSE 0 END) as Credit,
	'journal_bonus_amt(' + CONVERT(VARCHAR(MAX),([journal_bonus_amt])) + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND to_group_nm <> 'IB' AND to_account_nm = 'ebrown'

UNION ALL

--Franco's 1%
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	'team fd' AS Team,
	'fdaniele' AS Person,
	'5 - Bonuses Revenue Based' AS Category,
	'64020' AS [OracleAccount],
	'Bonus - Revenue Based' AS [OracleAccountDesc],
	'Debit' AS Side,
	'02' AS LOB,
	'101' AS Department,
	'001' AS Location,	
	(SUM(trade_desk_amt) + SUM(trade_desk_adj_amt) + SUM(non_oms_sales_credit_amt) + SUM(capital_mkt_amt) + SUM(capital_mkt_adj_amt)) * .01 as Debit,
	0 as Credit,
	'(trade_desk_amt(' + CONVERT(VARCHAR(MAX),SUM(trade_desk_amt)) + ') + trade_desk_adj_amt( ' + CONVERT(VARCHAR(MAX),SUM(trade_desk_adj_amt)) + ') + non_oms_sales_credit_amt(' + CONVERT(VARCHAR(MAX),SUM(non_oms_sales_credit_amt)) + ') + capital_mkt_amt(' + CONVERT(VARCHAR(MAX),SUM(capital_mkt_amt)) + ') + capital_mkt_adj_amt (' + CONVERT(VARCHAR(MAX),SUM(capital_mkt_adj_amt)) + ')) * .01, for IG' AS Formula
FROM [complete_bucket_sales_credit]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND from_group_nm = 'IG'
GROUP BY as_of_period_id, from_group_nm

UNION ALL
--------------------------------------------------------------------SALES ASSISTANT BONUSES-----------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
---------------CREDIT SIDE. 21201 - Accrued Bonuses - Sales Assistant Bonuses
-----------------------------------------------------------------------------------------------------------------

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'2 - Sales Assistant Bonuses' AS Category,
	'21201' AS [OracleAccount],
	'Accrued Bonuses - Sales Assistant Bonuses' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN (bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) < 0 THEN -(bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) ELSE 0 END) as Debit,
	(CASE WHEN (bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) > 0 THEN (bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) ELSE 0 END) as Credit,
	'bonus_reserve_rate_cost_amt(' + FORMAT(bonus_reserve_rate_cost_amt, 'N2') + ') + bonus_reserve_amt(' + FORMAT(bonus_reserve_amt, 'N2') + ') - reserve_bonus_amt(' + FORMAT(reserve_bonus_amt, 'N2') + ') - adhoc_reserve_amt(' + FORMAT(adhoc_reserve_amt, 'N2') + ') + accrued_bonus_rate_cost_amt(' + FORMAT(accrued_bonus_rate_cost_amt, 'N2') + ') + accrued_total_amt(' + FORMAT(accrued_total_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_assistant_bonus_salesperson]
WHERE to_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND (bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'2 - Sales Assistant Bonuses' AS Category,
	'21201' AS [OracleAccount],
	'Accrued Bonuses - Sales Assistant Bonuses' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN new_money_bonus < 0 THEN -new_money_bonus ELSE 0 END) as Debit,
	(CASE WHEN new_money_bonus > 0 THEN new_money_bonus ELSE 0 END) as Credit,
	'new_money_bonus(' + FORMAT(new_money_bonus, 'N2') + ') from ' + from_account_nm AS Formula
FROM [report_analyst_cost_derivation]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND new_money_bonus <> 0

UNION ALL
-----------------------------------------------------------------------------------------------------------------
---------------DEBIT SIDE. 64025
-----------------------------------------------------------------------------------------------------------------

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'2 - Sales Assistant Bonuses' AS Category,
	'64025' AS [OracleAccount],
	'Sales Assistant Bonuses' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN (bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) > 0 THEN (bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) ELSE 0 END) as Debit,
	(CASE WHEN (bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) < 0 THEN -(bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) ELSE 0 END) as Credit,
	'bonus_reserve_rate_cost_amt(' + FORMAT(bonus_reserve_rate_cost_amt, 'N2') + ') + bonus_reserve_amt(' + FORMAT(bonus_reserve_amt, 'N2') + ') - reserve_bonus_amt(' + FORMAT(reserve_bonus_amt, 'N2') + ') - adhoc_reserve_amt(' + FORMAT(adhoc_reserve_amt, 'N2') + ') + accrued_bonus_rate_cost_amt(' + FORMAT(accrued_bonus_rate_cost_amt, 'N2') + ') + accrued_total_amt(' + FORMAT(accrued_total_amt, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_assistant_bonus_salesperson]
WHERE to_group_nm <> 'IB'
--	AND as_of_period_id = @AsOfPeriodId
	AND run_period_id = as_of_period_id
	AND (bonus_reserve_rate_cost_amt + bonus_reserve_amt - reserve_bonus_amt - adhoc_reserve_amt + accrued_bonus_rate_cost_amt + accrued_total_amt) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'2 - Sales Assistant Bonuses' AS Category,
	'64025' AS [OracleAccount],
	'Sales Assistant Bonuses' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN new_money_bonus > 0 THEN new_money_bonus ELSE 0 END) as Debit,
	(CASE WHEN new_money_bonus < 0 THEN -new_money_bonus ELSE 0 END) as Credit,
	'new_money_bonus(' + FORMAT(new_money_bonus, 'N2') + ') from ' + from_account_nm AS Formula
FROM [report_analyst_cost_derivation]
--WHERE as_of_period_id = @AsOfPeriodId
WHERE run_period_id = as_of_period_id
	AND new_money_bonus <> 0

/*
** Drop temporary table
*/

IF (OBJECT_ID('tempdb..#Salaries')) IS NOT NULL
	DROP TABLE #Salaries

IF (OBJECT_ID('tempdb..#Bonuses')) IS NOT NULL
	DROP TABLE #Bonuses