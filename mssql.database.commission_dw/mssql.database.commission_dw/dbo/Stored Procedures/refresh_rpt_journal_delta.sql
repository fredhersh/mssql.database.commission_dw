﻿


CREATE PROCEDURE [dbo].[refresh_rpt_journal_delta] AS

/************************************************************************************************
**
**	This procedure refreshes persisted deleta data required for the Journal Report.  This code
**	is based off of dbo.refresh_rpt_journal.  The logic was originally supplied by Phil K.
**
**	FHersh		04/07/2020		Created.
**
************************************************************************************************/


SET NOCOUNT ON

/*
** Create rpt_journal if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.SCHEMA_ID = s.schema_id WHERE t.name = 'rpt_journal_delta' AND t.type='U' AND s.NAME = 'dbo')
BEGIN

	CREATE TABLE dbo.rpt_journal_delta
	([AsOfPeriodId]			INT NOT NULL
	,[Group]				VARCHAR(50) NOT NULL
	,[Team]					VARCHAR(50) NOT NULL
	,[Person]				VARCHAR(50) NOT NULL
	,[Category]				VARCHAR(50) NOT NULL
	,[OracleAccount]		VARCHAR(5) NOT NULL
	,[OracleAccountDesc]	VARCHAR(50) NOT NULL
	,[Side]					VARCHAR(10) NOT NULL
	,[LOB]					VARCHAR(5) NOT NULL
	,[Department]			VARCHAR(5) NOT NULL
	,[Location]				VARCHAR(5) NOT NULL
	,[Debit]				DECIMAL(18, 2) NULL
	,[Credit]				DECIMAL(18, 2) NULL
	,[Formula]				VARCHAR(2000) NOT NULL
	CONSTRAINT pk_rpt_journal_delta PRIMARY KEY CLUSTERED 
	(
		[AsOfPeriodId] ASC,
		[Group] ASC,
		[Team] ASC,
		[Person] ASC,
		[Category] ASC,
		[OracleAccount]	ASC,
		[OracleAccountDesc] ASC,
		[Side] ASC,
		[LOB] ASC,
		[Department] ASC,
		[Location] ASC,
		[Formula] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

DECLARE	@last_locked_period_id	INT
,		@last_locked_period_yr	INT

/*
** Get last locked period
*/

SELECT	@last_locked_period_id = MAX(run_eff_yyyymm_no)
FROM	dim_run
WHERE	is_run_locked = 'Yes'

SELECT	@last_locked_period_yr = run_eff_year_no
FROM	dim_run
WHERE	is_run_locked = 'Yes'
AND		run_eff_yyyymm_no = (SELECT	MAX(run_eff_yyyymm_no) FROM	dim_run WHERE is_run_locked = 'Yes')

/*
** Truncate journal report table
*/

TRUNCATE TABLE rpt_journal_delta

---Make a temp table for the salaries.
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'61020' AS [OracleAccount],
	'Salary Expense Sharing' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN salary_amt_delta_adj < 0 THEN -salary_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN salary_amt_delta_adj > 0 THEN salary_amt_delta_adj ELSE 0 END) AS Credit,
	'salary_amt_delta_adj for ' + to_account_nm + ' (' + FORMAT(salary_amt_delta_adj, 'N2') + ')' AS Formula
INTO #Salaries
FROM [complete_bucket_salary_expense_sharing]
WHERE from_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND salary_amt_delta_adj <> 0


---Make a temp table for the bonuses. Intentionally only permitting positive values to go here.
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'64040' AS [OracleAccount],
	'Bonus Expense Sharing' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN ((Case When adhoc_reserve_amt_delta_adj <> 0 then 0 Else bonus_reserve_amt_delta_adj + bonus_reserve_rate_cost_amt_delta_adj End) + adhoc_bonus_amt_delta_adj + bonus_amount_total_amt_delta_adj + bonus_rate_cost_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_adj_amt_delta_adj ) < 0 THEN -((Case When adhoc_reserve_amt_delta_adj <> 0 then 0 Else bonus_reserve_amt_delta_adj + bonus_reserve_rate_cost_amt_delta_adj End) + adhoc_bonus_amt_delta_adj + bonus_amount_total_amt_delta_adj + bonus_rate_cost_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_adj_amt_delta_adj ) ELSE 0 END) as Debit,
	(CASE WHEN ((Case When adhoc_reserve_amt_delta_adj <> 0 then 0 Else bonus_reserve_amt_delta_adj + bonus_reserve_rate_cost_amt_delta_adj End) + adhoc_bonus_amt_delta_adj + bonus_amount_total_amt_delta_adj + bonus_rate_cost_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_adj_amt_delta_adj ) > 0 THEN ((Case When adhoc_reserve_amt_delta_adj <> 0 then 0 Else bonus_reserve_amt_delta_adj + bonus_reserve_rate_cost_amt_delta_adj End) + adhoc_bonus_amt_delta_adj + bonus_amount_total_amt_delta_adj + bonus_rate_cost_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_adj_amt_delta_adj ) ELSE 0 END) AS Credit,
	'((Case When adhoc_reserve_amt_delta_adj(' + FORMAT(adhoc_reserve_amt_delta_adj, 'N2') + ') <> 0 then 0 Else bonus_reserve_amt_delta_adj(' + FORMAT(bonus_reserve_amt_delta_adj, 'N2') + ') + bonus_reserve_rate_cost_amt_delta_adj(' + FORMAT(bonus_reserve_rate_cost_amt_delta_adj, 'N2') + ') End) + adhoc_bonus_amt_delta_adj(' + FORMAT(adhoc_bonus_amt_delta_adj, 'N2') + ') + bonus_amount_total_amt_delta_adj(' + FORMAT(bonus_amount_total_amt_delta_adj, 'N2') + ') + bonus_rate_cost_amt_delta_adj(' + FORMAT(bonus_rate_cost_amt_delta_adj, 'N2') + ') - adhoc_reserve_amt_delta_adj(' + FORMAT(adhoc_reserve_amt_delta_adj, 'N2') + ') + accrued_bonus_adj_amt_delta_adj(' + FORMAT(accrued_bonus_adj_amt_delta_adj, 'N2') + ')' AS Formula	 
INTO #Bonuses
FROM [complete_bucket_bonus_expense_sharing]
WHERE from_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND ((Case When adhoc_reserve_amt_delta_adj <> 0 then 0 Else bonus_reserve_amt_delta_adj + bonus_reserve_rate_cost_amt_delta_adj End) + adhoc_bonus_amt_delta_adj + bonus_amount_total_amt_delta_adj + bonus_rate_cost_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_adj_amt_delta_adj ) > 0


----------------------------SALES CREDIT------------------------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------
---------------CREDIT SIDE. 43100 AND 43105
-----------------------------------------------------------------------------------------------------------------
INSERT	rpt_journal_delta
([AsOfPeriodId]
,[Group]
,[Team]
,[Person]
,[Category]
,[OracleAccount]
,[OracleAccountDesc]
,[Side]
,[LOB]
,[Department]
,[Location]
,[Debit]
,[Credit]
,[Formula]
)
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Adjustments)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_adj_amt_delta_adj < 0 THEN -commissionable_revenue_adj_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_adj_amt_delta_adj > 0 THEN commissionable_revenue_adj_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_adj_amt_delta_adj(' + FORMAT(commissionable_revenue_adj_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_adj_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Bullet and Niche)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) < 0 THEN -(commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) ELSE 0 END) AS Debit,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) > 0 THEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_bullets_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_bullets_amt_delta_adj, 'N2') + ') + commissionable_revenue_slice_niche_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_niche_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Credit)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_credit_amt_delta_adj < 0 THEN -commissionable_revenue_slice_credit_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_credit_amt_delta_adj > 0 THEN commissionable_revenue_slice_credit_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_credit_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_credit_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_credit_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Muni)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_muni_amt_delta_adj < 0 THEN -commissionable_revenue_slice_muni_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_muni_amt_delta_adj > 0 THEN commissionable_revenue_slice_muni_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_muni_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_muni_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_muni_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (ResiMBS)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt_delta_adj < 0 THEN -commissionable_revenue_slice_resi_mbs_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt_delta_adj > 0 THEN commissionable_revenue_slice_resi_mbs_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_resi_mbs_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_resi_mbs_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_resi_mbs_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Other)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_other_amt_delta_adj < 0 THEN -commissionable_revenue_slice_other_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_other_amt_delta_adj > 0 THEN commissionable_revenue_slice_other_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_other_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_other_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_other_amt_delta_adj <> 0

UNION ALL 

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43105' AS [OracleAccount],
	'Sales Credit (Capital Markets)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt_delta_adj < 0 THEN -commissionable_revenue_slice_cap_mkts_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt_delta_adj > 0 THEN commissionable_revenue_slice_cap_mkts_amt_delta_adj ELSE 0 END) AS Credit,
	--'commissionable_revenue_slice_cap_mkts_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_cap_mkts_amt_delta_adj, 'N2') + ')' AS Formula
	'non_oms_sales_credit_amt_delta_adj(' + FORMAT(non_oms_sales_credit_amt_delta_adj, 'N2') + '), gross_commission_loan_amt_delta_adj(' + FORMAT(gross_commission_loan_amt_delta_adj, 'N2') + ') + Portion of trades from Sales Credit (commission_val) and Shared Sales Credit (shared_commission_val)' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_cap_mkts_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Adjustments)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_adj_amt_delta_adj < 0 THEN -commissionable_revenue_adj_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_adj_amt_delta_adj > 0 THEN commissionable_revenue_adj_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_adj_amt_delta_adj(' + FORMAT(commissionable_revenue_adj_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_adj_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Bullet and Niche)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) < 0 THEN -(commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) ELSE 0 END) AS Debit,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) > 0 THEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_bullets_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_bullets_amt_delta_adj, 'N2') + ') + commissionable_revenue_slice_niche_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_niche_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Credit)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_credit_amt_delta_adj < 0 THEN -commissionable_revenue_slice_credit_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_credit_amt_delta_adj > 0 THEN commissionable_revenue_slice_credit_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_credit_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_credit_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_credit_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Muni)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_muni_amt_delta_adj < 0 THEN -commissionable_revenue_slice_muni_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_muni_amt_delta_adj > 0 THEN commissionable_revenue_slice_muni_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_muni_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_muni_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_muni_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (ResiMBS)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt_delta_adj < 0 THEN -commissionable_revenue_slice_resi_mbs_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt_delta_adj > 0 THEN commissionable_revenue_slice_resi_mbs_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_resi_mbs_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_resi_mbs_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_resi_mbs_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43100' AS [OracleAccount],
	'Sales Credit (Other)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_other_amt_delta_adj < 0 THEN -commissionable_revenue_slice_other_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_other_amt_delta_adj > 0 THEN commissionable_revenue_slice_other_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_other_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_other_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_other_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43105' AS [OracleAccount],
	'Sales Credit (Capital Markets)' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt_delta_adj < 0 THEN -commissionable_revenue_slice_cap_mkts_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt_delta_adj > 0 THEN commissionable_revenue_slice_cap_mkts_amt_delta_adj ELSE 0 END) AS Credit,
	--'commissionable_revenue_slice_cap_mkts_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_cap_mkts_amt_delta_adj, 'N2') + ')' AS Formula
	'non_oms_sales_credit_amt_delta_adj(' + FORMAT(non_oms_sales_credit_amt_delta_adj, 'N2') + '), gross_commission_loan_amt_delta_adj(' + FORMAT(gross_commission_loan_amt_delta_adj, 'N2') + ') + Portion of trades from Sales Credit (commission_val) and Shared Sales Credit (shared_commission_val)' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_cap_mkts_amt_delta_adj <> 0

UNION ALL

-----------------------------------------------------------------------------------------------------------------
---------------DEBIT SIDE. 43150 AND 43151
-----------------------------------------------------------------------------------------------------------------
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Adjustments)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_adj_amt_delta_adj > 0 THEN commissionable_revenue_adj_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_adj_amt_delta_adj < 0 THEN -commissionable_revenue_adj_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_adj_amt_delta_adj(' + FORMAT(commissionable_revenue_adj_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_adj_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Bullet and Niche)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) > 0 THEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) ELSE 0 END) AS Debit,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) < 0 THEN -(commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_bullets_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_bullets_amt_delta_adj, 'N2') + ') + commissionable_revenue_slice_niche_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_niche_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Credit)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_credit_amt_delta_adj > 0 THEN commissionable_revenue_slice_credit_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_credit_amt_delta_adj < 0 THEN -commissionable_revenue_slice_credit_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_credit_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_credit_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_credit_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Muni)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_muni_amt_delta_adj > 0 THEN commissionable_revenue_slice_muni_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_muni_amt_delta_adj < 0 THEN -commissionable_revenue_slice_muni_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_muni_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_muni_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_muni_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (ResiMBS)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt_delta_adj > 0 THEN commissionable_revenue_slice_resi_mbs_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt_delta_adj < 0 THEN -commissionable_revenue_slice_resi_mbs_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_resi_mbs_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_resi_mbs_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_resi_mbs_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Other)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_other_amt_delta_adj > 0 THEN commissionable_revenue_slice_other_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_other_amt_delta_adj < 0 THEN -commissionable_revenue_slice_other_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_other_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_other_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_other_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43150' AS [OracleAccount],
	'Sales Credit (Capital Markets)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'05' AS LOB, --4315_'s have hardcoded Oracle codes 
	'205' AS Department,
	'000' AS Location,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt_delta_adj > 0 THEN commissionable_revenue_slice_cap_mkts_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt_delta_adj < 0 THEN -commissionable_revenue_slice_cap_mkts_amt_delta_adj ELSE 0 END) AS Credit,
	--'commissionable_revenue_slice_cap_mkts_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_cap_mkts_amt_delta_adj, 'N2') + ')' AS Formula
	'non_oms_sales_credit_amt_delta_adj(' + FORMAT(non_oms_sales_credit_amt_delta_adj, 'N2') + '), gross_commission_loan_amt_delta_adj(' + FORMAT(gross_commission_loan_amt_delta_adj, 'N2') + ') + Portion of trades from Sales Credit (commission_val) and Shared Sales Credit (shared_commission_val)' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'FIG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_cap_mkts_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Adjustments)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_adj_amt_delta_adj > 0 THEN commissionable_revenue_adj_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_adj_amt_delta_adj < 0 THEN -commissionable_revenue_adj_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_adj_amt_delta_adj(' + FORMAT(commissionable_revenue_adj_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_adj_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Bullet and Niche)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) > 0 THEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) ELSE 0 END) AS Debit,
	(CASE WHEN (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) < 0 THEN -(commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_bullets_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_bullets_amt_delta_adj, 'N2') + ') + commissionable_revenue_slice_niche_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_niche_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND (commissionable_revenue_slice_bullets_amt_delta_adj + commissionable_revenue_slice_niche_amt_delta_adj) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Credit)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_credit_amt_delta_adj > 0 THEN commissionable_revenue_slice_credit_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_credit_amt_delta_adj < 0 THEN -commissionable_revenue_slice_credit_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_credit_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_credit_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_credit_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Muni)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_muni_amt_delta_adj > 0 THEN commissionable_revenue_slice_muni_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_muni_amt_delta_adj < 0 THEN -commissionable_revenue_slice_muni_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_muni_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_muni_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_muni_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (ResiMBS)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt_delta_adj > 0 THEN commissionable_revenue_slice_resi_mbs_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_resi_mbs_amt_delta_adj < 0 THEN -commissionable_revenue_slice_resi_mbs_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_resi_mbs_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_resi_mbs_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_resi_mbs_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Other)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'03' AS LOB, --4315_'s have hardcoded Oracle codes 
	'203' AS Department,
	'001' AS Location,
	(CASE WHEN commissionable_revenue_slice_other_amt_delta_adj > 0 THEN commissionable_revenue_slice_other_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_other_amt_delta_adj < 0 THEN -commissionable_revenue_slice_other_amt_delta_adj ELSE 0 END) AS Credit,
	'commissionable_revenue_slice_other_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_other_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_other_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'1 - Sales Credit' AS Category,
	'43151' AS [OracleAccount],
	'Sales Credit (Capital Markets)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'05' AS LOB, --4315_'s have hardcoded Oracle codes 
	'205' AS Department,
	'000' AS Location,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt_delta_adj > 0 THEN commissionable_revenue_slice_cap_mkts_amt_delta_adj ELSE 0 END) AS Debit,
	(CASE WHEN commissionable_revenue_slice_cap_mkts_amt_delta_adj < 0 THEN -commissionable_revenue_slice_cap_mkts_amt_delta_adj ELSE 0 END) AS Credit,
	-- 'commissionable_revenue_slice_cap_mkts_amt_delta_adj(' + FORMAT(commissionable_revenue_slice_cap_mkts_amt_delta_adj, 'N2') + ')' AS Formula
	'non_oms_sales_credit_amt_delta_adj(' + FORMAT(non_oms_sales_credit_amt_delta_adj, 'N2') + '), gross_commission_loan_amt_delta_adj(' + FORMAT(gross_commission_loan_amt_delta_adj, 'N2') + ') + Portion of trades from Sales Credit (commission_val) and Shared Sales Credit (shared_commission_val)' AS Formula
FROM [complete_bucket_sales_credit]
WHERE from_group_nm = 'IG'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND commissionable_revenue_slice_cap_mkts_amt_delta_adj <> 0

UNION ALL

-------------------------------------------------------------------------------------------ACCRUED COMP----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
---------------CREDIT SIDE. 62100 63020 62020 61020 64040 21250 13200 62050 
-----------------------------------------------------------------------------------------------------------------

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62100' AS [OracleAccount],
	'IB Referral Expense' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN ib_referral_expense_amt_delta_adj > 0 THEN ib_referral_expense_amt_delta_adj ELSE 0 END) as Debit, --This field has weird signing because it is negative
	(CASE WHEN ib_referral_expense_amt_delta_adj < 0 THEN -ib_referral_expense_amt_delta_adj ELSE 0 END) AS Credit,
	'ib_referral_expense_amt_delta_adj(' + FORMAT(ib_referral_expense_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_compensation]
WHERE to_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND ib_referral_expense_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'63020' AS [OracleAccount],
	'Draw Reimbursed' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN draw_amt_delta_adj < 0 THEN -draw_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN draw_amt_delta_adj > 0 THEN draw_amt_delta_adj ELSE 0 END) AS Credit,
	'draw_amt_delta_adj(' + FORMAT(draw_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_draw_guarantee]
WHERE from_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND draw_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	from_team_nm AS Team,
	from_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62020' AS [OracleAccount],
	'Commissions - House Share' AS [OracleAccountDesc],
	'Credit' AS Side,
	from_lob_cd AS LOB,
	from_department_cd AS Department,
	from_location_cd AS Location,	
	(CASE WHEN monthly_guarantee_amt_delta_adj < 0 THEN -monthly_guarantee_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN monthly_guarantee_amt_delta_adj > 0 THEN monthly_guarantee_amt_delta_adj ELSE 0 END) AS Credit,
	'monthly_guarantee_amt_delta_adj(' + FORMAT(monthly_guarantee_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_draw_guarantee]
WHERE from_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND monthly_guarantee_amt_delta_adj <> 0

UNION ALL

SELECT * FROM #Salaries

UNION ALL

SELECT * FROM #Bonuses

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'21250' AS [OracleAccount],
	'Accrued Compensation' AS [OracleAccountDesc],
	'Credit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	(CASE WHEN ((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0 THEN -((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Debit,
	(CASE WHEN ((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) > 0 THEN ((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Credit,
	'journal_gross_amt_delta_adj(' + FORMAT(journal_gross_amt_delta_adj, 'N2') + ') + post_commission_rate_adj_amt_delta_adj(' + FORMAT(post_commission_rate_adj_amt_delta_adj, 'N2') + ') - fines_amt_delta_adj(' + FORMAT(fines_amt_delta_adj, 'N2') + ') + ib_referral_expense_amt_delta_adj(' + FORMAT(ib_referral_expense_amt_delta_adj, 'N2') + ') - draw_amt_delta_adj(' + FORMAT(draw_amt_delta_adj, 'N2') + ') - monthly_guarantee_amt_delta_adj(' + FORMAT(monthly_guarantee_amt_delta_adj, 'N2') + ') - (all analyst salaries(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id))) + ')) - (all analyst bonuses(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) + '))'  AS Formula
FROM [complete_bucket_accrued_compensation] cbac
WHERE from_account_nm = 'ptcp'
	AND to_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND ((journal_gross_amt_delta_adj + [journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj] + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) > 0

UNION ALL

--If someone on a Draw didn't meet their draw
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'13200' AS [OracleAccount],
	'Advances To Employees' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN ((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0 THEN -((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Debit,
	(CASE WHEN ((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) > 0 THEN ((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Credit,
	'journal_gross_amt_delta_adj(' + FORMAT(journal_gross_amt_delta_adj, 'N2') + ') + post_commission_rate_adj_amt_delta_adj(' + FORMAT(post_commission_rate_adj_amt_delta_adj, 'N2') + ') - fines_amt_delta_adj(' + FORMAT(fines_amt_delta_adj, 'N2') + ') + ib_referral_expense_amt_delta_adj(' + FORMAT(ib_referral_expense_amt_delta_adj, 'N2') + ') - draw_amt_delta_adj(' + FORMAT(draw_amt_delta_adj, 'N2') + ') - (all analyst salaries(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id))) + ')) - (all analyst bonuses(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) + '))'  AS Formula
FROM [complete_bucket_accrued_compensation] cbac
WHERE from_account_nm = 'ptcp'
	AND to_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND ((journal_gross_amt_delta_adj + [journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj] + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - draw_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0
	AND draw_amt_delta_adj <> 0 

UNION ALL

--We need to DEBIT Accrued Comp when people don't meet there guarantee
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'21250' AS [OracleAccount],
	'Accrued Compensation (Un-met guarantee offset)' AS [OracleAccountDesc],
	'Debit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	(CASE WHEN ((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0 THEN -((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Debit,
	(CASE WHEN ((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) > 0 THEN ((journal_gross_amt_delta_adj + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) ELSE 0 END) AS Credit,
	'journal_gross_amt_delta_adj(' + FORMAT(journal_gross_amt_delta_adj, 'N2') + ') + post_commission_rate_adj_amt_delta_adj(' + FORMAT(post_commission_rate_adj_amt_delta_adj, 'N2') + ') - fines_amt_delta_adj(' + FORMAT(fines_amt_delta_adj, 'N2') + ') + ib_referral_expense_amt_delta_adj(' + FORMAT(ib_referral_expense_amt_delta_adj, 'N2') + ') - monthly_guarantee_amt_delta_adj(' + FORMAT(monthly_guarantee_amt_delta_adj, 'N2') + ') - (all analyst salaries(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id))) + ')) - (all analyst bonuses(' + CONVERT(VARCHAR(MAX),((SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) + '))'  AS Formula
FROM [complete_bucket_accrued_compensation] cbac
WHERE from_account_nm = 'ptcp'
	AND to_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND ((journal_gross_amt_delta_adj + [journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj] + post_commission_rate_adj_amt_delta_adj - fines_amt_delta_adj + ib_referral_expense_amt_delta_adj - monthly_guarantee_amt_delta_adj - (SELECT COALESCE(SUM(credit),0) FROM #Salaries bos WHERE bos.Person = cbac.to_account_nm AND bos.AsOfPeriodId = cbac.as_of_period_id) - (SELECT COALESCE(SUM(credit),0) FROM #Bonuses bob WHERE bob.Person = cbac.to_account_nm AND bob.AsOfPeriodId = cbac.as_of_period_id))) < 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62050' AS [OracleAccount],
	'Miscellaneous Deductions (FINES)' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN fines_amt_delta_adj < 0 THEN -fines_amt_delta_adj ELSE 0 END) as Debit, 
	(CASE WHEN fines_amt_delta_adj > 0 THEN fines_amt_delta_adj ELSE 0 END) AS Credit,
	'fines_amt_delta_adj(' + FORMAT(fines_amt_delta_adj, 'N2') + ')'
FROM [complete_bucket_accrued_compensation]
WHERE to_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND fines_amt_delta_adj <> 0

UNION ALL

--Excluding Grier and Jones, because they have a special thing.
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62050' AS [OracleAccount],
	'Miscellaneous Deductions (Other adj)' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN post_commission_rate_adj_amt_delta_adj > 0 THEN post_commission_rate_adj_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN post_commission_rate_adj_amt_delta_adj < 0 THEN -post_commission_rate_adj_amt_delta_adj ELSE 0 END) AS Credit,
	'post_commission_rate_adj_amt_delta_adj(' + FORMAT(post_commission_rate_adj_amt_delta_adj, 'N2') + ')'
FROM [complete_bucket_accrued_compensation]
WHERE to_group_nm <> 'IB'
	AND to_account_nm NOT IN ('kgrier','cjones')
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND post_commission_rate_adj_amt_delta_adj <> 0

UNION ALL

--Special KGRIER thing
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62031' AS [OracleAccount],
	'Other Bonus' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN post_commission_rate_adj_amt_delta_adj > 0 THEN post_commission_rate_adj_amt_delta_adj ELSE 0 END) as Debit, 
	(CASE WHEN post_commission_rate_adj_amt_delta_adj < 0 THEN -post_commission_rate_adj_amt_delta_adj ELSE 0 END) AS Credit,
	'post_commission_rate_adj_amt_delta_adj(' + FORMAT(post_commission_rate_adj_amt_delta_adj, 'N2') + ')'
FROM [complete_bucket_accrued_compensation]
WHERE 
	to_account_nm = 'kgrier'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND post_commission_rate_adj_amt_delta_adj <> 0

UNION ALL

--Special CJONES thing
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'64040' AS [OracleAccount],
	'Bonus Expense Sharing' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN post_commission_rate_adj_amt_delta_adj > 0 THEN post_commission_rate_adj_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN post_commission_rate_adj_amt_delta_adj < 0 THEN -post_commission_rate_adj_amt_delta_adj ELSE 0 END) AS Credit,
	'post_commission_rate_adj_amt_delta_adj(' + FORMAT(post_commission_rate_adj_amt_delta_adj, 'N2') + ')'
FROM [complete_bucket_accrued_compensation]
WHERE 
	to_account_nm = 'cjones'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND post_commission_rate_adj_amt_delta_adj <> 0

UNION ALL

-----------------------------------------------------------------------------------------------------------------
---------------DEBIT SIDE. 62010
-----------------------------------------------------------------------------------------------------------------
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'62010' AS [OracleAccount],
	'Commissions - Gross' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN journal_gross_amt_delta_adj > 0 THEN journal_gross_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN journal_gross_amt_delta_adj < 0 THEN -journal_gross_amt_delta_adj ELSE 0 END) AS Credit,
	'journal_gross_amt_delta_adj(' + FORMAT(journal_gross_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_compensation]
WHERE to_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND journal_gross_amt_delta_adj <> 0

UNION ALL

-------------------------------------------------------------------------ACCRUED BONUSES----------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
---------------CREDIT SIDE. 21202
-----------------------------------------------------------------------------------------------------------------
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'21202' AS [OracleAccount],
	'Accrued Bonuses - Commissions' AS [OracleAccountDesc],
	'Credit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	(CASE WHEN ([journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj]) < 0 THEN ([journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj]) ELSE 0 END) as Debit,
	(CASE WHEN ([journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj]) > 0 THEN ([journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj]) ELSE 0 END) as Credit,
	'journal_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([journal_bonus_amt_delta_adj])) + ') + ib_referral_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([ib_referral_bonus_amt_delta_adj])) + ') + journal_collective_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([journal_collective_bonus_amt_delta_adj])) + ') + pt_score_referral_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([pt_score_referral_bonus_amt_delta_adj])) + ') + new_account_bonus_kicker_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([new_account_bonus_kicker_amt_delta_adj])) + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND [journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj] <> 0

UNION ALL

--ebrown's offset really is to 21250
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'4 - Accrued Comp' AS Category,
	'21250' AS [OracleAccount],
	'Accrued Compensation (EPB)' AS [OracleAccountDesc],
	'Credit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	(CASE WHEN ([journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj]) < 0 THEN ([journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj]) ELSE 0 END) as Debit,
	(CASE WHEN ([journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj]) > 0 THEN ([journal_bonus_amt_delta_adj] + [ib_referral_bonus_amt_delta_adj] + [journal_collective_bonus_amt_delta_adj] + [pt_score_referral_bonus_amt_delta_adj] + [new_account_bonus_kicker_amt_delta_adj]) ELSE 0 END) as Credit,
	'journal_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([journal_bonus_amt_delta_adj])) + ') + ib_referral_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([ib_referral_bonus_amt_delta_adj])) + ') + journal_collective_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([journal_collective_bonus_amt_delta_adj])) + ') + pt_score_referral_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([pt_score_referral_bonus_amt_delta_adj])) + ') + new_account_bonus_kicker_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([new_account_bonus_kicker_amt_delta_adj])) + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND to_group_nm <> 'IB' AND to_account_nm = 'ebrown'

UNION ALL

--Franco's 1%
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	'team fd' AS Team,
	'fdaniele' AS Person,
	'5 - Bonuses Revenue Based' AS Category,
	'21200' AS [OracleAccount],
	'Accrued Bonuses' AS [OracleAccountDesc],
	'Credit' AS Side,
	'00' AS LOB,
	'000' AS Department,
	'000' AS Location,	
	0 as Debit,
	(SUM(trade_desk_amt_delta_adj) + SUM(trade_desk_adj_amt_delta_adj) + SUM(non_oms_sales_credit_amt_delta_adj) + SUM(capital_mkt_amt_delta_adj) + SUM(capital_mkt_adj_amt_delta_adj)) * .01 as Credit,
	'(trade_desk_amt_delta_adj(' + CONVERT(VARCHAR(MAX),SUM(trade_desk_amt_delta_adj)) + ') + trade_desk_adj_amt_delta_adj( ' + CONVERT(VARCHAR(MAX),SUM(trade_desk_adj_amt_delta_adj)) + ') + non_oms_sales_credit_amt_delta_adj(' + CONVERT(VARCHAR(MAX),SUM(non_oms_sales_credit_amt_delta_adj)) + ') + capital_mkt_amt_delta_adj(' + CONVERT(VARCHAR(MAX),SUM(capital_mkt_amt_delta_adj)) + ') + capital_mkt_adj_amt_delta_adj (' + CONVERT(VARCHAR(MAX),SUM(capital_mkt_adj_amt_delta_adj)) + ')) * .01, for IG' AS Formula
FROM [complete_bucket_sales_credit]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND from_group_nm = 'IG'
GROUP BY as_of_period_id, from_group_nm

UNION ALL

-----------------------------------------------------------------------------------------------------------------
---------------DEBIT SIDE. 62030 62100 62032 62100
-----------------------------------------------------------------------------------------------------------------
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62030' AS [OracleAccount],
	'New Account Bonus' AS [OracleAccountDesc],
	'Debit' AS Side,
	ISNULL(to_lob_cd, 'Unknown') AS LOB,
	ISNULL(to_department_cd, 'Unknown') AS Department,
	ISNULL(to_location_cd, 'Unknown') AS Location,	
	(CASE WHEN ([journal_bonus_amt_delta_adj]) > 0 THEN ([journal_bonus_amt_delta_adj]) ELSE 0 END) as Debit,
	(CASE WHEN ([journal_bonus_amt_delta_adj]) < 0 THEN -([journal_bonus_amt_delta_adj]) ELSE 0 END) as Credit,
	'journal_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([journal_bonus_amt_delta_adj])) + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND [journal_bonus_amt_delta_adj] <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62100' AS [OracleAccount],
	'IB Referral Bonus' AS [OracleAccountDesc],
	'Debit' AS Side,
	'04' AS LOB, --62100 has hardcoded Oracle values.
	'204' AS Department,
	'001' AS Location,
	(CASE WHEN ib_referral_bonus_amt_delta_adj > 0 THEN ib_referral_bonus_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN ib_referral_bonus_amt_delta_adj < 0 THEN -ib_referral_bonus_amt_delta_adj ELSE 0 END) AS Credit,
	'ib_referral_bonus_amt_delta_adj(' + FORMAT(ib_referral_bonus_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND ib_referral_bonus_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62032' AS [OracleAccount],
	'Collective Bonus' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN journal_collective_bonus_amt_delta_adj > 0 THEN journal_collective_bonus_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN journal_collective_bonus_amt_delta_adj < 0 THEN -journal_collective_bonus_amt_delta_adj ELSE 0 END) AS Credit,
	'journaled_collective_bonus_amt_delta_adj(' + FORMAT(journal_collective_bonus_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND journal_collective_bonus_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62032' AS [OracleAccount],
	'Collective Bonus New Account' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN new_account_bonus_kicker_amt_delta_adj > 0 THEN new_account_bonus_kicker_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN new_account_bonus_kicker_amt_delta_adj < 0 THEN -new_account_bonus_kicker_amt_delta_adj ELSE 0 END) AS Credit,
	'new_account_bonus_kicker_amt_delta_adj(' + FORMAT(new_account_bonus_kicker_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND new_account_bonus_kicker_amt_delta_adj <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'3 - Accrued Bonuses' AS Category,
	'62100' AS [OracleAccount],
	'PT Score Referral Bonus' AS [OracleAccountDesc],
	'Debit' AS Side,
	'07' AS LOB, --62100 has hardcoded Oracle values.
	'207' AS Department,
	'001' AS Location,
	(CASE WHEN pt_score_referral_bonus_amt_delta_adj > 0 THEN pt_score_referral_bonus_amt_delta_adj ELSE 0 END) as Debit,
	(CASE WHEN pt_score_referral_bonus_amt_delta_adj < 0 THEN -pt_score_referral_bonus_amt_delta_adj ELSE 0 END) AS Credit,
	'pt_score_referral_bonus_amt_delta_adj(' + FORMAT(pt_score_referral_bonus_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND to_group_nm <> 'IB' AND to_account_nm <> 'ebrown'
	AND pt_score_referral_bonus_amt_delta_adj <> 0

UNION ALL

--Special EPB recoding.
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'5 - Bonuses Revenue Based' AS Category,
	'64020' AS [OracleAccount],
	'Bonus - Revenue Based' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN ([journal_bonus_amt_delta_adj]) > 0 THEN ([journal_bonus_amt_delta_adj]) ELSE 0 END) as Debit,
	(CASE WHEN ([journal_bonus_amt_delta_adj]) < 0 THEN -([journal_bonus_amt_delta_adj]) ELSE 0 END) as Credit,
	'journal_bonus_amt_delta_adj(' + CONVERT(VARCHAR(MAX),([journal_bonus_amt_delta_adj])) + ')' AS Formula
FROM [complete_bucket_accrued_bonus_commission]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND to_group_nm <> 'IB' AND to_account_nm = 'ebrown'

UNION ALL

--Franco's 1%
SELECT 
	as_of_period_id AS [AsOfPeriodId],
	from_group_nm AS [Group],
	'team fd' AS Team,
	'fdaniele' AS Person,
	'5 - Bonuses Revenue Based' AS Category,
	'64020' AS [OracleAccount],
	'Bonus - Revenue Based' AS [OracleAccountDesc],
	'Debit' AS Side,
	'02' AS LOB,
	'101' AS Department,
	'001' AS Location,	
	(SUM(trade_desk_amt_delta_adj) + SUM(trade_desk_adj_amt_delta_adj) + SUM(non_oms_sales_credit_amt_delta_adj) + SUM(capital_mkt_amt_delta_adj) + SUM(capital_mkt_adj_amt_delta_adj)) * .01 as Debit,
	0 as Credit,
	'(trade_desk_amt_delta_adj(' + CONVERT(VARCHAR(MAX),SUM(trade_desk_amt_delta_adj)) + ') + trade_desk_adj_amt_delta_adj( ' + CONVERT(VARCHAR(MAX),SUM(trade_desk_adj_amt_delta_adj)) + ') + non_oms_sales_credit_amt_delta_adj(' + CONVERT(VARCHAR(MAX),SUM(non_oms_sales_credit_amt_delta_adj)) + ') + capital_mkt_amt_delta_adj(' + CONVERT(VARCHAR(MAX),SUM(capital_mkt_amt_delta_adj)) + ') + capital_mkt_adj_amt_delta_adj (' + CONVERT(VARCHAR(MAX),SUM(capital_mkt_adj_amt_delta_adj)) + ')) * .01, for IG' AS Formula
FROM [complete_bucket_sales_credit]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND from_group_nm = 'IG'
GROUP BY as_of_period_id, from_group_nm

UNION ALL
--------------------------------------------------------------------SALES ASSISTANT BONUSES-----------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
---------------CREDIT SIDE. 21201 - Accrued Bonuses - Sales Assistant Bonuses
-----------------------------------------------------------------------------------------------------------------

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'2 - Sales Assistant Bonuses' AS Category,
	'21201' AS [OracleAccount],
	'Accrued Bonuses - Sales Assistant Bonuses' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN (bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) < 0 THEN -(bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) ELSE 0 END) as Debit,
	(CASE WHEN (bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) > 0 THEN (bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) ELSE 0 END) as Credit,
	'bonus_reserve_rate_cost_amt_delta_adj(' + FORMAT(bonus_reserve_rate_cost_amt_delta_adj, 'N2') + ') + bonus_reserve_amt_delta_adj(' + FORMAT(bonus_reserve_amt_delta_adj, 'N2') + ') - reserve_bonus_amt_delta_adj(' + FORMAT(reserve_bonus_amt_delta_adj, 'N2') + ') - adhoc_reserve_amt_delta_adj(' + FORMAT(adhoc_reserve_amt_delta_adj, 'N2') + ') + accrued_bonus_rate_cost_amt_delta_adj(' + FORMAT(accrued_bonus_rate_cost_amt_delta_adj, 'N2') + ') + accrued_total_amt_delta_adj(' + FORMAT(accrued_total_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_assistant_bonus_salesperson]
WHERE to_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND (bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'2 - Sales Assistant Bonuses' AS Category,
	'21201' AS [OracleAccount],
	'Accrued Bonuses - Sales Assistant Bonuses' AS [OracleAccountDesc],
	'Credit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN new_money_bonus < 0 THEN -new_money_bonus ELSE 0 END) as Debit,
	(CASE WHEN new_money_bonus > 0 THEN new_money_bonus ELSE 0 END) as Credit,
	'new_money_bonus(' + FORMAT(new_money_bonus, 'N2') + ') from ' + from_account_nm AS Formula
FROM [report_analyst_cost_derivation]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND new_money_bonus <> 0

UNION ALL
-----------------------------------------------------------------------------------------------------------------
---------------DEBIT SIDE. 64025
-----------------------------------------------------------------------------------------------------------------

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'2 - Sales Assistant Bonuses' AS Category,
	'64025' AS [OracleAccount],
	'Sales Assistant Bonuses' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN (bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) > 0 THEN (bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) ELSE 0 END) as Debit,
	(CASE WHEN (bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) < 0 THEN -(bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) ELSE 0 END) as Credit,
	'bonus_reserve_rate_cost_amt_delta_adj(' + FORMAT(bonus_reserve_rate_cost_amt_delta_adj, 'N2') + ') + bonus_reserve_amt_delta_adj(' + FORMAT(bonus_reserve_amt_delta_adj, 'N2') + ') - reserve_bonus_amt_delta_adj(' + FORMAT(reserve_bonus_amt_delta_adj, 'N2') + ') - adhoc_reserve_amt_delta_adj(' + FORMAT(adhoc_reserve_amt_delta_adj, 'N2') + ') + accrued_bonus_rate_cost_amt_delta_adj(' + FORMAT(accrued_bonus_rate_cost_amt_delta_adj, 'N2') + ') + accrued_total_amt_delta_adj(' + FORMAT(accrued_total_amt_delta_adj, 'N2') + ')' AS Formula
FROM [complete_bucket_sales_assistant_bonus_salesperson]
WHERE to_group_nm <> 'IB'
	AND as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND (bonus_reserve_rate_cost_amt_delta_adj + bonus_reserve_amt_delta_adj - reserve_bonus_amt_delta_adj - adhoc_reserve_amt_delta_adj + accrued_bonus_rate_cost_amt_delta_adj + accrued_total_amt_delta_adj) <> 0

UNION ALL

SELECT 
	as_of_period_id AS [AsOfPeriodId],
	to_group_nm AS [Group],
	to_team_nm AS Team,
	to_account_nm AS Person,
	'2 - Sales Assistant Bonuses' AS Category,
	'64025' AS [OracleAccount],
	'Sales Assistant Bonuses' AS [OracleAccountDesc],
	'Debit' AS Side,
	to_lob_cd AS LOB,
	to_department_cd AS Department,
	to_location_cd AS Location,	
	(CASE WHEN new_money_bonus > 0 THEN new_money_bonus ELSE 0 END) as Debit,
	(CASE WHEN new_money_bonus < 0 THEN -new_money_bonus ELSE 0 END) as Credit,
	'new_money_bonus(' + FORMAT(new_money_bonus, 'N2') + ') from ' + from_account_nm AS Formula
FROM [report_analyst_cost_derivation]
WHERE as_of_period_id <= @last_locked_period_id AND LEFT(as_of_period_id, 4) = @last_locked_period_yr
	AND locked_run = 'No'
	AND new_money_bonus <> 0


/*
** Drop temporary table
*/

IF (OBJECT_ID('tempdb..#Salaries')) IS NOT NULL
	DROP TABLE #Salaries

IF (OBJECT_ID('tempdb..#Bonuses')) IS NOT NULL
	DROP TABLE #Bonuses