﻿




CREATE PROCEDURE [dbo].[refresh_fact_journal]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the Journal fact
**	table (dbo.fact_journal).  It is primarily used as a journal fact definition.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/02/2020		Added INCLUDE fields onto ie_fact_journal_as_of_date_id.
**	FHersh		01/07/2020		Added new_account_bonus_kicker_amt.
**								Change journal_amt to DECIMAL(18, 2).
**	FHersh		01/17/2020		Added commissionable_revenue*_amt.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create fact_journal if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.SCHEMA_ID = s.schema_id WHERE t.name = 'fact_journal' AND t.type='U' AND s.NAME = 'dbo')
BEGIN

	CREATE TABLE dbo.fact_journal
	(run_id					INT NOT NULL
	,as_of_date_id			INT NOT NULL
	,from_account_id		INT NOT NULL
	,from_team_id			INT NOT NULL
	,from_group_id			INT NOT NULL
	,from_oracle_code_id	INT NOT NULL
	,to_account_id			INT NOT NULL
	,to_team_id				INT NOT NULL
	,to_group_id			INT NOT NULL
	,to_oracle_code_id		INT NOT NULL
	,reason_id				INT NOT NULL
	,comment_id				INT NOT NULL
	,journal_amt			DECIMAL(18, 2) NOT NULL
	CONSTRAINT pk_fact_journal PRIMARY KEY CLUSTERED 
	(
		run_id ASC,
		as_of_date_id ASC,
		from_account_id ASC,
		from_team_id ASC,
		from_group_id ASC,
		from_oracle_code_id	ASC,
		to_account_id ASC,
		to_team_id ASC,
		to_group_id ASC,
		to_oracle_code_id ASC,
		reason_id ASC,
		comment_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Remove all records from journal_fact that are from a locked run
*/

DELETE
FROM	fct
FROM	fact_journal fct INNER JOIN dim_run dr ON (fct.run_id = dr.run_id)
WHERE	dr.is_run_locked = 'No'

/*
** Remove all records from journal_fact that are estimates
*/

DELETE
FROM	fct
FROM	fact_journal fct INNER JOIN dim_date dd ON (fct.as_of_date_id = dd.date_id)
WHERE	dd.yyyymm_no >= CAST(CONVERT(CHAR(6), GETDATE(), 112) AS INT)

/*
** Get list of run_ids to process
**	which consisted from locked runs not in journal_fact and most recent run_id
**
*/

SELECT	dr.run_id, dr.calc_run_id
INTO	#runs
FROM	dim_run dr LEFT JOIN fact_journal fct ON (dr.run_id = fct.run_id)
WHERE	dr.is_run_locked = 'Yes'
AND		fct.run_id IS NULL
UNION
SELECT	dr.run_id, dr.calc_run_id
FROM	dim_run dr
WHERE	dr.run_dt = (SELECT MAX(run_dt) FROM dim_run)

/*
** Populate jornal fact table
*/

INSERT	fact_journal
(run_id
,as_of_date_id
,from_account_id
,from_team_id
,from_group_id
,from_oracle_code_id
,to_account_id
,to_team_id
,to_group_id
,to_oracle_code_id
,reason_id
,comment_id
,journal_amt
)

----------------------------------------------------------------------------------------------------------------------------------------------------------------
--calc_analyst_cost amounts
----------------------------------------------------------------------------------------------------------------------------------------------------------------
--accrued_adhoc_bonus_flg
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.accrued_adhoc_bonus_flg, 0)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'accrued_adhoc_bonus_flg') dr

UNION ALL

--accrued_bonus_flg
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.accrued_bonus_flg, 0)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'accrued_bonus_flg') dr

UNION ALL

--accrued_bonus_payment_flg
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.accrued_bonus_payment_flg, 0)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'accrued_bonus_payment_flg') dr

UNION ALL

--bonus_amount_pct
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.bonus_amount_pct, 8)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_amount_pct') dr

UNION ALL

--bonus_amount_val
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.bonus_amount_val, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_amount_val') dr

UNION ALL

--bonus_split_rate_pct
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.bonus_split_rate_pct, 8)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_split_rate_pct') dr

UNION ALL

--bonus_val
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.bonus_val, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_val') dr

UNION ALL

--post_commission_rate_flg
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.post_commission_rate_flg, 0)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'post_commission_rate_flg') dr

UNION ALL

--reserve_bonus_flg
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.reserve_bonus_flg, 0)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'reserve_bonus_flg') dr

UNION ALL

--shared_bonus_flg
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.shared_bonus_flg, 0)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'shared_bonus_flg') dr

UNION ALL

--shared_bonus_rate_flg
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.shared_bonus_rate_flg, 0)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'shared_bonus_rate_flg') dr

UNION ALL

--total_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(cac.total_bonus_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
				INNER JOIN commission.out.calc_analyst_cost cac ON (cas.calc_analyst_setup_id = cac.calc_analyst_setup_id)
				LEFT JOIN dim_date aot ON (cac.commission_month_val = aot.month_no AND cac.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
				LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
				LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
				LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'total_bonus_amt') dr

UNION ALL

----------------------------------------------------------------------------------------------------------------------------------------------------------------
--calc_analyst_payment amounts
----------------------------------------------------------------------------------------------------------------------------------------------------------------

--accrued_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = ISNULL(dc.comment_id, 0)
,				journal_amt = ROUND(cap.accrued_bonus_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
						INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
						INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
						INNER JOIN commission.out.calc_analyst_payment cap ON (cas.calc_analyst_setup_id = cap.calc_analyst_setup_id)
						LEFT JOIN dim_date aot ON (cap.commission_month_val = aot.month_no AND cap.commission_year_val = aot.year_no AND aot.day_no = 1)
						LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
						LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
						LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
						LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
						LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
						LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
						LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
						LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
						CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'accrued_bonus_amt') dr
						CROSS APPLY (SELECT comment_id FROM dim_comment WHERE comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.') dc

UNION ALL

--accrued_bonus_part_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = ISNULL(dc.comment_id, 0)
,				journal_amt = ROUND(cap.accrued_bonus_part_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
						INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
						INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
						INNER JOIN commission.out.calc_analyst_payment cap ON (cas.calc_analyst_setup_id = cap.calc_analyst_setup_id)
						LEFT JOIN dim_date aot ON (cap.commission_month_val = aot.month_no AND cap.commission_year_val = aot.year_no AND aot.day_no = 1)
						LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
						LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
						LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
						LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
						LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
						LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
						LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
						LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
						CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'accrued_bonus_part_amt') dr
						CROSS APPLY (SELECT comment_id FROM dim_comment WHERE comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.') dc

UNION ALL

--adhoc_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = ISNULL(dc.comment_id, 0)
,				journal_amt = ROUND(cap.adhoc_bonus_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
						INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
						INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
						INNER JOIN commission.out.calc_analyst_payment cap ON (cas.calc_analyst_setup_id = cap.calc_analyst_setup_id)
						LEFT JOIN dim_date aot ON (cap.commission_month_val = aot.month_no AND cap.commission_year_val = aot.year_no AND aot.day_no = 1)
						LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
						LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
						LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
						LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
						LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
						LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
						LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
						LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
						CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'adhoc_bonus_amt') dr
						CROSS APPLY (SELECT comment_id FROM dim_comment WHERE comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.') dc

UNION ALL

--adhoc_bonus_part_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = ISNULL(dc.comment_id, 0)
,				journal_amt = ROUND(cap.adhoc_bonus_part_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
						INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
						INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
						INNER JOIN commission.out.calc_analyst_payment cap ON (cas.calc_analyst_setup_id = cap.calc_analyst_setup_id)
						LEFT JOIN dim_date aot ON (cap.commission_month_val = aot.month_no AND cap.commission_year_val = aot.year_no AND aot.day_no = 1)
						LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
						LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
						LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
						LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
						LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
						LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
						LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
						LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
						CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'adhoc_bonus_part_amt') dr
						CROSS APPLY (SELECT comment_id FROM dim_comment WHERE comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.') dc

UNION ALL

--bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = ISNULL(dc.comment_id, 0)
,				journal_amt = ROUND(cap.bonus_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
						INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
						INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
						INNER JOIN commission.out.calc_analyst_payment cap ON (cas.calc_analyst_setup_id = cap.calc_analyst_setup_id)
						LEFT JOIN dim_date aot ON (cap.commission_month_val = aot.month_no AND cap.commission_year_val = aot.year_no AND aot.day_no = 1)
						LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
						LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
						LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
						LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
						LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
						LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
						LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
						LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
						CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_amt') dr
						CROSS APPLY (SELECT comment_id FROM dim_comment WHERE comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.') dc

UNION ALL

--bonus_part_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = ISNULL(dc.comment_id, 0)
,				journal_amt = ROUND(cap.bonus_part_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
						INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
						INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
						INNER JOIN commission.out.calc_analyst_payment cap ON (cas.calc_analyst_setup_id = cap.calc_analyst_setup_id)
						LEFT JOIN dim_date aot ON (cap.commission_month_val = aot.month_no AND cap.commission_year_val = aot.year_no AND aot.day_no = 1)
						LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
						LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
						LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
						LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
						LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
						LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
						LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
						LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
						CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_part_amt') dr
						CROSS APPLY (SELECT comment_id FROM dim_comment WHERE comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.') dc

UNION ALL

--recurring_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = ISNULL(dc.comment_id, 0)
,				journal_amt = ROUND(cap.recurring_bonus_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
						INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
						INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
						INNER JOIN commission.out.calc_analyst_payment cap ON (cas.calc_analyst_setup_id = cap.calc_analyst_setup_id)
						LEFT JOIN dim_date aot ON (cap.commission_month_val = aot.month_no AND cap.commission_year_val = aot.year_no AND aot.day_no = 1)
						LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
						LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
						LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
						LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
						LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
						LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
						LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
						LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
						CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'recurring_bonus_amt') dr
						CROSS APPLY (SELECT comment_id FROM dim_comment WHERE comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.') dc

UNION ALL

--recurring_bonus_rate_yield_part_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = ISNULL(dc.comment_id, 0)
,				journal_amt = ROUND(cap.recurring_bonus_rate_yield_part_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
						INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
						INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
						INNER JOIN commission.out.calc_analyst_payment cap ON (cas.calc_analyst_setup_id = cap.calc_analyst_setup_id)
						LEFT JOIN dim_date aot ON (cap.commission_month_val = aot.month_no AND cap.commission_year_val = aot.year_no AND aot.day_no = 1)
						LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
						LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
						LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
						LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
						LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
						LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
						LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
						LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
						CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'recurring_bonus_rate_yield_part_amt') dr
						CROSS APPLY (SELECT comment_id FROM dim_comment WHERE comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.') dc

UNION ALL

--salary_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = ISNULL(dfa.account_id, 0)
,				from_team_id = ISNULL(dft.team_id, 0)
,				from_department_id = ISNULL(dfd.department_id, 0)
,				from_oracle_code_id = ISNULL(foc.oracle_code_id, 0)
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = ISNULL(dc.comment_id, 0)
,				journal_amt = ROUND(cap.salary_amt, 2)
FROM			#runs r	INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
						INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
						INNER JOIN commission.out.calc_analyst_setup cas ON (cps.calc_person_setup_id = cas.calc_person_setup_id)
						INNER JOIN commission.out.calc_analyst_payment cap ON (cas.calc_analyst_setup_id = cap.calc_analyst_setup_id)
						LEFT JOIN dim_date aot ON (cap.commission_month_val = aot.month_no AND cap.commission_year_val = aot.year_no AND aot.day_no = 1)
						LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
						LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
						LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
						LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
						LEFT JOIN dim_account dfa ON (cps.parent_person_cd = dfa.account_nm)
						LEFT JOIN dim_team dft ON (cts.team_nm = dft.team_nm)
						LEFT JOIN dim_department dfd ON (cts.dept = dfd.department_nm)
						LEFT JOIN dim_person_oracle fpo ON (cps.parent_person_cd = fpo.person_cd AND aot.date_id BETWEEN fpo.start_id AND ISNULL(fpo.end_id, 99999999))
						LEFT JOIN dim_oracle_code foc ON (fpo.department_cd = foc.department_cd AND fpo.lob_cd = foc.lob_cd AND fpo.location_cd = foc.location_cd)
						CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'salary_amt') dr
						CROSS APPLY (SELECT comment_id FROM dim_comment WHERE comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.') dc

UNION ALL

----------------------------------------------------------------------------------------------------------------------------------------------------------------
--calc_salesperson_commission
----------------------------------------------------------------------------------------------------------------------------------------------------------------

--accrued_bonus_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.accrued_bonus_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'accrued_bonus_adj_amt') dr

UNION ALL

--accrued_bonus_rate_cost_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.accrued_bonus_rate_cost_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'accrued_bonus_rate_cost_amt') dr

UNION ALL

--accrued_total_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.accrued_total_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'accrued_total_amt') dr

UNION ALL

--adhoc_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.adhoc_bonus_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'adhoc_bonus_amt') dr

UNION ALL

--adhoc_reserve_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.adhoc_reserve_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'adhoc_reserve_amt') dr

UNION ALL

--advance_balance_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.advance_balance_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'advance_balance_amt') dr

UNION ALL

--advance_cost_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.advance_cost_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'advance_cost_amt') dr

UNION ALL

--advance_to_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.advance_to_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'advance_to_adj_amt') dr

UNION ALL

--analyst_salary_cost_total_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.analyst_salary_cost_total_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'analyst_salary_cost_total_amt') dr

UNION ALL

--bonus_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.bonus_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_adj_amt') dr

UNION ALL

--bonus_amount_total_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.bonus_amount_total_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_amount_total_amt') dr

UNION ALL

--bonus_rate_cost_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.bonus_rate_cost_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_rate_cost_amt') dr

UNION ALL

--bonus_reserve_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.bonus_reserve_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_reserve_amt') dr

UNION ALL

--bonus_reserve_rate_cost_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.bonus_reserve_rate_cost_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'bonus_reserve_rate_cost_amt') dr

UNION ALL

--capital_mkt_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.capital_mkt_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'capital_mkt_adj_amt') dr

UNION ALL

--capital_mkt_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.capital_mkt_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'capital_mkt_amt') dr

UNION ALL

--collective_bonus_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.collective_bonus_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'collective_bonus_adj_amt') dr

UNION ALL

--collective_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.collective_bonus_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'collective_bonus_amt') dr

UNION ALL

--commission_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commission_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commission_adj_amt') dr

UNION ALL

--commission_per_producer_ytd_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commission_per_producer_ytd_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commission_per_producer_ytd_amt') dr

UNION ALL

--commissionable_revenue_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_adj_amt') dr

UNION ALL

--commissionable_revenue_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_amt') dr

UNION ALL

--commissionable_revenue_cap_mkt_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_cap_mkt_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_cap_mkt_amt') dr

UNION ALL

--commissionable_revenue_slice_bullets_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_slice_bullets_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_bullets_amt') dr

UNION ALL

--commissionable_revenue_slice_cap_mkts_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_slice_cap_mkts_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_cap_mkts_amt') dr

UNION ALL

--commissionable_revenue_slice_credit_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_slice_credit_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_credit_amt') dr

UNION ALL

--commissionable_revenue_slice_muni_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_slice_muni_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_muni_amt') dr

UNION ALL

--commissionable_revenue_slice_niche_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_slice_niche_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_niche_amt') dr

UNION ALL

--commissionable_revenue_slice_other_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_slice_other_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_other_amt') dr

UNION ALL

--commissionable_revenue_slice_resi_mbs_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_slice_resi_mbs_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_resi_mbs_amt') dr

UNION ALL

--commissionable_revenue_trading_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.commissionable_revenue_trading_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'commissionable_revenue_trading_amt') dr

UNION ALL

--deferral_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.deferral_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'deferral_adj_amt') dr

UNION ALL

--deferral_balance_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.deferral_balance_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'deferral_balance_amt') dr

UNION ALL

--deferral_input_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.deferral_input_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'deferral_input_amt') dr

UNION ALL

--draw_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.draw_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'draw_amt') dr

UNION ALL

--fig_rate_deferral_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.fig_rate_deferral_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'fig_rate_deferral_amt') dr

UNION ALL

--fig_rate_pct
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.fig_rate_pct, 8)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'fig_rate_pct') dr

UNION ALL

--fig_table_per_producer_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.fig_table_per_producer_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'fig_table_per_producer_amt') dr

UNION ALL

--fines_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.fines_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'fines_amt') dr

UNION ALL

--gross_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.gross_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'gross_adj_amt') dr

UNION ALL

--gross_commission_bond_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.gross_commission_bond_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'gross_commission_bond_amt') dr

UNION ALL

--gross_commission_loan_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.gross_commission_loan_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'gross_commission_loan_amt') dr

UNION ALL

--gross_sales_credit_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.gross_sales_credit_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'gross_sales_credit_adj_amt') dr

UNION ALL

--ib_referral_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.ib_referral_bonus_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'ib_referral_bonus_amt') dr

UNION ALL

--ib_referral_expense_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.ib_referral_expense_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'ib_referral_expense_amt') dr

UNION ALL

--journal_accrued_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.journal_accrued_bonus_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'journal_accrued_bonus_amt') dr

UNION ALL

--journal_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.journal_bonus_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'journal_bonus_amt') dr

UNION ALL

--journal_collective_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.journal_collective_bonus_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'journal_collective_bonus_amt') dr

UNION ALL

--journal_deferral_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.journal_deferral_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'journal_deferral_adj_amt') dr

UNION ALL

--journal_deferral_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.journal_deferral_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'journal_deferral_amt') dr

UNION ALL

--journal_gross_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.journal_gross_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'journal_gross_amt') dr

UNION ALL

--monthly_guarantee_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.monthly_guarantee_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'monthly_guarantee_amt') dr

UNION ALL

--new_account_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.new_account_bonus_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'new_account_bonus_amt') dr

UNION ALL

--new_account_bonus_kicker_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.new_account_bonus_kicker_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'new_account_bonus_kicker_amt') dr

UNION ALL

--non_oms_sales_credit_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.non_oms_sales_credit_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'non_oms_sales_credit_amt') dr

UNION ALL

--paid_commission_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.paid_commission_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'paid_commission_amt') dr

UNION ALL

--past_month_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.past_month_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'past_month_adj_amt') dr

UNION ALL

--post_commission_rate_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.post_commission_rate_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'post_commission_rate_adj_amt') dr

UNION ALL

--pre_commission_rate_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.pre_commission_rate_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'pre_commission_rate_adj_amt') dr

UNION ALL

--prepaid_commission_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.prepaid_commission_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'prepaid_commission_amt') dr

UNION ALL

--pt_score_referral_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.pt_score_referral_bonus_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'pt_score_referral_bonus_amt') dr

UNION ALL

--reserve_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.reserve_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'reserve_adj_amt') dr

UNION ALL

--reserve_bonus_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.reserve_bonus_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'reserve_bonus_amt') dr

UNION ALL

--reserve_total_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.reserve_total_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'reserve_total_amt') dr

UNION ALL

--sales_credit_bond_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.sales_credit_bond_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'sales_credit_bond_amt') dr

UNION ALL

--sales_credit_loan_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.sales_credit_loan_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'sales_credit_loan_amt') dr

UNION ALL

--shared_loan_sales_credit_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.shared_loan_sales_credit_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'shared_loan_sales_credit_amt') dr

UNION ALL

--shared_non_oms_sales_credit_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.shared_non_oms_sales_credit_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'shared_non_oms_sales_credit_amt') dr

UNION ALL

--shared_sales_credit_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.shared_sales_credit_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'shared_sales_credit_amt') dr

UNION ALL

--total_costs_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.total_costs_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'total_costs_amt') dr

UNION ALL

--total_gross_revenue_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.total_gross_revenue_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'total_gross_revenue_amt') dr

UNION ALL

--trade_desk_adj_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.trade_desk_adj_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'trade_desk_adj_amt') dr

UNION ALL

--trade_desk_amt
SELECT DISTINCT	r.run_id
,				as_of_time_id = ISNULL(aot.date_id, 0)
,				from_account_id = 1
,				from_team_id = 1
,				from_department_id = 1
,				from_oracle_code_id = 0
,				to_account_id = ISNULL(dta.account_id, 0)
,				to_team_id = ISNULL(dtt.team_id, 0)
,				to_department_id = ISNULL(dtd.department_id, 0)
,				to_oracle_code_id = ISNULL(toc.oracle_code_id, 0)
,				reason_id = ISNULL(dr.reason_id, 0)
,				comment_id = 0
,				journal_amt = ROUND(csc.trade_desk_amt, 2)
FROM			#runs r INNER JOIN commission.out.calc_team_setup cts ON (r.calc_run_id = cts.calc_run_id)
				INNER JOIN commission.out.calc_person_setup cps ON (cts.calc_team_setup_id = cps.calc_team_setup_id)
				INNER JOIN commission.out.calc_salesperson_setup css ON (cps.calc_person_setup_id = css.calc_person_setup_id)
				INNER JOIN commission.out.calc_salesperson_commission csc ON (css.calc_salesperson_setup_id = csc.calc_salesperson_setup_id)
				LEFT JOIN dim_date aot ON (csc.commission_month_val = aot.month_no AND csc.commission_year_val = aot.year_no AND aot.day_no = 1)
				LEFT JOIN dim_account dta ON (cps.person_cd = dta.account_nm)
				LEFT JOIN dim_team dtt ON (cts.team_nm = dtt.team_nm)
				LEFT JOIN dim_department dtd ON (cts.dept = dtd.department_nm)
				LEFT JOIN dim_person_oracle tpo ON (cps.person_cd = tpo.person_cd AND aot.date_id BETWEEN tpo.start_id AND ISNULL(tpo.end_id, 99999999))
				LEFT JOIN dim_oracle_code toc ON (tpo.department_cd = toc.department_cd AND tpo.lob_cd = toc.lob_cd AND tpo.location_cd = toc.location_cd)
				CROSS APPLY (SELECT reason_id FROM dim_reason WHERE reason_nm = 'trade_desk_amt') dr

/*
** Populate fact_journal with delta adjustment records
*/

DECLARE	@last_locked_run_id		INT
,		@last_estimate_run_id	INT

SELECT	@last_locked_run_id = MAX(run_id)
FROM	dim_run
WHERE	is_run_locked = 'Yes'

SELECT	@last_estimate_run_id = MAX(run_id)
FROM	dim_run
WHERE	is_run_locked = 'No'

INSERT	fact_journal
(run_id
,as_of_date_id
,from_account_id
,from_team_id
,from_group_id
,from_oracle_code_id
,to_account_id
,to_team_id
,to_group_id
,to_oracle_code_id
,reason_id
,comment_id
,journal_amt
)
SELECT	run_id = e.run_id
,		e.as_of_date_id
,		e.from_account_id
,		e.from_team_id
,		e.from_group_id
,		e.from_oracle_code_id
,		e.to_account_id
,		e.to_team_id
,		e.to_group_id
,		e.to_oracle_code_id
,		reason_id = r.delta_adj_reason_id
,		comment_id = 0
,		journal_amt = e.journal_amt - l.journal_amt
FROM	fact_journal l	INNER JOIN fact_journal e ON (l.as_of_date_id = e.as_of_date_id AND l.from_account_id = e.from_account_id AND l.from_team_id = e.from_team_id AND l.from_group_id = e.from_group_id AND l.from_oracle_code_id = e.from_oracle_code_id AND l.to_account_id = e.to_account_id AND l.to_team_id = e.to_team_id AND l.to_group_id = e.to_group_id AND l.to_oracle_code_id = e.to_oracle_code_id AND l.reason_id = e.reason_id AND l.comment_id = e.comment_id)
						INNER JOIN dim_reason r ON (e.reason_id = r.reason_id)
WHERE	l.run_id = @last_locked_run_id
AND		e.run_id = @last_estimate_run_id
AND		l.journal_amt <> e.journal_amt

UNION ALL

SELECT	run_id = @last_estimate_run_id
,		l.as_of_date_id
,		l.from_account_id
,		l.from_team_id
,		l.from_group_id
,		l.from_oracle_code_id
,		l.to_account_id
,		l.to_team_id
,		l.to_group_id
,		l.to_oracle_code_id
,		reason_id = r.delta_adj_reason_id
,		comment_id = 0
,		journal_amt = -l.journal_amt 
FROM	fact_journal l	LEFT JOIN fact_journal e ON (l.as_of_date_id = e.as_of_date_id AND l.from_account_id = e.from_account_id AND l.from_team_id = e.from_team_id AND l.from_group_id = e.from_group_id AND l.from_oracle_code_id = e.from_oracle_code_id AND l.to_account_id = e.to_account_id AND l.to_team_id = e.to_team_id AND l.to_group_id = e.to_group_id AND l.to_oracle_code_id = e.to_oracle_code_id AND l.reason_id = e.reason_id AND l.comment_id = e.comment_id AND e.run_id = @last_estimate_run_id)
						INNER JOIN dim_reason r ON (l.reason_id = r.reason_id)
WHERE	l.run_id = @last_locked_run_id
AND		CHARINDEX('_delta_adj', r.reason_nm) = 0
AND		e.run_id IS NULL

UNION ALL

SELECT	run_id = @last_estimate_run_id
,		e.as_of_date_id
,		e.from_account_id
,		e.from_team_id
,		e.from_group_id
,		e.from_oracle_code_id
,		e.to_account_id
,		e.to_team_id
,		e.to_group_id
,		e.to_oracle_code_id
,		reason_id = r.delta_adj_reason_id
,		comment_id = 0
,		journal_amt = e.journal_amt
FROM	fact_journal l	RIGHT JOIN fact_journal e ON (l.as_of_date_id = e.as_of_date_id AND l.from_account_id = e.from_account_id AND l.from_team_id = e.from_team_id AND l.from_group_id = e.from_group_id AND l.from_oracle_code_id = e.from_oracle_code_id AND l.to_account_id = e.to_account_id AND l.to_team_id = e.to_team_id AND l.to_group_id = e.to_group_id AND l.to_oracle_code_id = e.to_oracle_code_id AND l.reason_id = e.reason_id AND l.comment_id = e.comment_id AND l.run_id = @last_locked_run_id)
						INNER JOIN dim_reason r ON (e.reason_id = r.reason_id)
WHERE	e.run_id = @last_estimate_run_id
AND		CHARINDEX('_delta_adj', r.reason_nm) = 0
AND		l.run_id IS NULL

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_fact_journal' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	ALTER TABLE dbo.fact_journal ADD  CONSTRAINT pk_fact_journal PRIMARY KEY CLUSTERED (run_id ASC, as_of_date_id ASC, from_account_id ASC, from_team_id ASC, from_group_id ASC, to_account_id ASC, to_team_id ASC, to_group_id ASC, reason_id ASC, comment_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_as_of_date_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_as_of_date_id ON dbo.fact_journal(as_of_date_id ASC) INCLUDE ([run_id],[from_account_id],[from_team_id],[from_group_id],[from_oracle_code_id],[to_account_id],[to_team_id],[to_group_id],[to_oracle_code_id],[reason_id],[comment_id],[journal_amt]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_from_account_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_from_account_id ON dbo.fact_journal(from_account_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_from_team_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_from_team_id ON dbo.fact_journal(from_team_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_from_group_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_from_group_id ON dbo.fact_journal(from_group_id ASC) INCLUDE ([run_id],[as_of_date_id],[from_account_id],[from_team_id],[from_oracle_code_id],[to_account_id],[to_team_id],[to_group_id],[to_oracle_code_id],[reason_id],[comment_id],[journal_amt]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_from_oracle_code_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_from_oracle_code_id ON dbo.fact_journal(from_oracle_code_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_to_account_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_to_account_id ON dbo.fact_journal(to_account_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_to_team_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_to_team_id ON dbo.fact_journal(to_team_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_to_group_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_to_group_id ON dbo.fact_journal(to_group_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_to_oracle_code_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_to_oracle_code_id ON dbo.fact_journal(to_oracle_code_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_reason_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_reason_id ON dbo.fact_journal(reason_id ASC) INCLUDE ([run_id], [as_of_date_id], [from_account_id], [from_team_id], [from_group_id], [from_oracle_code_id], [to_account_id], [to_team_id], [to_group_id], [to_oracle_code_id], [comment_id], [journal_amt]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'ie_fact_journal_comment_id' AND object_id = OBJECT_ID('dbo.fact_journal'))
BEGIN
	CREATE NONCLUSTERED INDEX ie_fact_journal_comment_id ON dbo.fact_journal(comment_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

/*
** Drop temporary table
*/

IF (OBJECT_ID('tempdb..#runs')) IS NOT NULL
	DROP TABLE #runs