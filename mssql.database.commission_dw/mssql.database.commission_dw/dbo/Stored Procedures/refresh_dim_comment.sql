﻿




CREATE PROCEDURE [dbo].[refresh_dim_comment]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the comment dimension
**	table (dbo.dim_comment).  It is primarily used as a comment definition.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create dim_comment if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.schema_id = s.schema_id WHERE t.name = 'dim_comment' AND t.type='U' AND s.name = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_comment
	(comment_id		INT IDENTITY(1,1) NOT NULL
	,comment_txt	VARCHAR(1000) NOT NULL
	CONSTRAINT pk_dim_comment PRIMARY KEY CLUSTERED 
	(
		comment_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Insert Default comment record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_comment WHERE comment_id = 0)
BEGIN

	SET IDENTITY_INSERT dbo.dim_comment ON

	INSERT	dbo.dim_comment
	(comment_id
	,comment_txt
	)
	SELECT	comment_id = 0
	,		comment_txt = ''

	SET IDENTITY_INSERT dbo.dim_comment OFF

END

/*
** Insert comment templates record
*/

IF NOT EXISTS (SELECT 1 FROM dim_comment WHERE comment_id = 1)
BEGIN

	SET IDENTITY_INSERT dim_comment ON

	INSERT	dim_comment
	(comment_id
	,comment_txt
	)
	SELECT	comment_id = 1
	,		comment_txt = 'Salary for <to_account_nm> from <from_account_nm> for <yyyymm>'

	SET IDENTITY_INSERT dim_comment OFF

END

IF NOT EXISTS (SELECT 1 FROM dim_comment WHERE comment_id = 2)
BEGIN

	SET IDENTITY_INSERT dim_comment ON

	INSERT	dim_comment
	(comment_id
	,comment_txt
	)
	SELECT	comment_id = 2
	,		comment_txt = 'Accrued bonus for <to_account_nm> related to <from_account_nm> for <yyyymm>.'

	SET IDENTITY_INSERT dim_comment OFF

END

/*
** Populate comment table
*/

INSERT	dim_comment
(comment_txt
)
SELECT DISTINCT	comment_txt = cap.adhoc_bonus_cmt
FROM			[$(commission)].out.calc_analyst_payment cap LEFT JOIN dim_comment c ON (cap.adhoc_bonus_cmt = c.comment_txt)
WHERE			c.comment_txt IS NULL

INSERT	dim_comment
(comment_txt
)
SELECT DISTINCT	comment_txt = cap.recurring_bonus_cmt
FROM			[$(commission)].out.calc_analyst_payment cap LEFT JOIN dim_comment c ON (cap.recurring_bonus_cmt = c.comment_txt)
WHERE			c.comment_txt IS NULL

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_comment' AND object_id = OBJECT_ID('dbo.dim_comment'))
BEGIN
	ALTER TABLE dbo.dim_comment ADD  CONSTRAINT pk_dim_comment PRIMARY KEY CLUSTERED (comment_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'uie_dim_comment_comment_txt' AND object_id = OBJECT_ID('dbo.dim_comment'))
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX uie_dim_comment_comment_txt ON dbo.dim_comment(comment_txt ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END


