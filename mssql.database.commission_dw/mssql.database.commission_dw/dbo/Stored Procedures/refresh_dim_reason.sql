﻿




CREATE PROCEDURE [dbo].[refresh_dim_reason]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the Reason dimension
**	table (dbo.dim_reason).  It is primarily used as a reason definition.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/07/2020		Added new_account_bonus_kicker_amt.
**	FHersh		01/17/2020		Added commissionable_revenue*_amt and adjust size of fields
**								reason_nm and reason_descr.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create dim_reason if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.SCHEMA_ID = s.schema_id WHERE t.name = 'dim_reason' AND t.type='U' AND s.NAME = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_reason
	(reason_id				INT IDENTITY(1,1) NOT NULL
	,reason_nm				VARCHAR(100) NOT NULL
	,reason_descr			VARCHAR(200) NOT NULL
	,delta_adj_reason_id	INT
	CONSTRAINT pk_dim_reason PRIMARY KEY CLUSTERED 
	(
		reason_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Insert Default reason record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_reason WHERE reason_id = 0)
BEGIN

	SET IDENTITY_INSERT dbo.dim_reason ON

	INSERT	dbo.dim_reason
	(reason_id
	,reason_nm
	,reason_descr
	)
	SELECT	reason_id = 0
	,		reason_nm = 'unknown'
	,		reason_descr = 'unknown'

	SET IDENTITY_INSERT dbo.dim_reason OFF

END

/*
** Populate reason table
*/

----------------------------------------------------------------------------------------------------------------------------------------------------------------
--calc_analyst_cost
----------------------------------------------------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'accrued_adhoc_bonus_flg')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('accrued_adhoc_bonus_flg', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'accrued_bonus_flg')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('accrued_bonus_flg', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'accrued_bonus_payment_flg')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('accrued_bonus_payment_flg', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_amount_pct')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_amount_pct', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_amount_val')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_amount_val', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_split_rate_pct')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_split_rate_pct', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_val')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_val', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'post_commission_rate_flg')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('post_commission_rate_flg', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'reserve_bonus_flg')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('reserve_bonus_flg', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'shared_bonus_flg')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('shared_bonus_flg', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'shared_bonus_rate_flg')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('shared_bonus_rate_flg', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'total_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('total_bonus_amt', '')

----------------------------------------------------------------------------------------------------------------------------------------------------------------
--calc_analyst_payment
----------------------------------------------------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'accrued_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('accrued_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'accrued_bonus_part_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('accrued_bonus_part_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'adhoc_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('adhoc_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'adhoc_bonus_part_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('adhoc_bonus_part_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_part_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_part_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'recurring_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('recurring_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'recurring_bonus_rate_yield_part_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('recurring_bonus_rate_yield_part_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'salary_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('salary_amt', '')

----------------------------------------------------------------------------------------------------------------------------------------------------------------
--calc_salessalesperson_commission
----------------------------------------------------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'accrued_bonus_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('accrued_bonus_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'accrued_bonus_rate_cost_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('accrued_bonus_rate_cost_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'accrued_total_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('accrued_total_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'adhoc_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('adhoc_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'adhoc_reserve_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('adhoc_reserve_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'advance_balance_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('advance_balance_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'advance_cost_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('advance_cost_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'advance_to_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('advance_to_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'analyst_salary_cost_total_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('analyst_salary_cost_total_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_amount_total_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_amount_total_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_rate_cost_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_rate_cost_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_reserve_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_reserve_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'bonus_reserve_rate_cost_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('bonus_reserve_rate_cost_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'capital_mkt_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('capital_mkt_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'capital_mkt_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('capital_mkt_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'collective_bonus_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('collective_bonus_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'collective_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('collective_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commission_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commission_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commission_per_producer_ytd_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commission_per_producer_ytd_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_cap_mkt_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_cap_mkt_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_bullets_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_slice_bullets_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_cap_mkts_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_slice_cap_mkts_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_credit_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_slice_credit_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_muni_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_slice_muni_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_niche_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_slice_niche_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_other_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_slice_other_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_slice_resi_mbs_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_slice_resi_mbs_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'commissionable_revenue_trading_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('commissionable_revenue_trading_amt', '')IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'deferral_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('deferral_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'deferral_balance_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('deferral_balance_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'deferral_input_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('deferral_input_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'draw_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('draw_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'fig_rate_deferral_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('fig_rate_deferral_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'fig_rate_pct')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('fig_rate_pct', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'fig_table_per_producer_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('fig_table_per_producer_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'fines_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('fines_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'gross_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('gross_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'gross_commission_bond_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('gross_commission_bond_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'gross_commission_loan_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('gross_commission_loan_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'gross_sales_credit_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('gross_sales_credit_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'ib_referral_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('ib_referral_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'ib_referral_expense_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('ib_referral_expense_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'journal_accrued_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('journal_accrued_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'journal_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('journal_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'journal_collective_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('journal_collective_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'journal_deferral_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('journal_deferral_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'journal_deferral_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('journal_deferral_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'journal_gross_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('journal_gross_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'monthly_guarantee_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('monthly_guarantee_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'new_account_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('new_account_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'new_account_bonus_kicker_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('new_account_bonus_kicker_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'non_oms_sales_credit_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('non_oms_sales_credit_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'paid_commission_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('paid_commission_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'past_month_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('past_month_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'post_commission_rate_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('post_commission_rate_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'pre_commission_rate_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('pre_commission_rate_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'prepaid_commission_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('prepaid_commission_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'pt_score_referral_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('pt_score_referral_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'reserve_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('reserve_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'reserve_bonus_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('reserve_bonus_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'reserve_total_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('reserve_total_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'sales_credit_bond_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('sales_credit_bond_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'sales_credit_loan_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('sales_credit_loan_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'shared_loan_sales_credit_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('shared_loan_sales_credit_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'shared_non_oms_sales_credit_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('shared_non_oms_sales_credit_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'shared_sales_credit_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('shared_sales_credit_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'total_costs_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('total_costs_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'total_gross_revenue_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('total_gross_revenue_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'trade_desk_adj_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('trade_desk_adj_amt', '')
IF NOT EXISTS (SELECT 'x' FROM dim_reason WHERE reason_nm = 'trade_desk_amt')
	INSERT	dim_reason (reason_nm, reason_descr)
	VALUES	('trade_desk_amt', '')

----------------------------------------------------------------------------------------------------------------------------------------------------------------
--delta_adjustment reasons
----------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT	dim_reason (reason_nm, reason_descr)
SELECT	reason_nm = r.reason_nm + '_delta_adj'
,		reason_descr = 'Delta adjustment for ' + r.reason_nm + ' when comparing to prior run'
FROM	dim_reason r LEFT JOIN dim_reason rda ON (r.reason_nm  + '_delta_adj' = rda.reason_nm)
WHERE	r.reason_id <> 0
AND		rda.reason_id IS NULL
AND		CHARINDEX('_delta_adj', r.reason_nm) = 0

/*
** Associate reasons to their delta adjusments
*/

UPDATE	r
SET		delta_adj_reason_id = rda.reason_id
FROM	dim_reason r INNER JOIN dim_reason rda ON (r.reason_nm  + '_delta_adj' = rda.reason_nm)
WHERE	ISNULL(r.delta_adj_reason_id, 0) <> rda.reason_id

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_reason' AND object_id = OBJECT_ID('dbo.dim_reason'))
BEGIN
	ALTER TABLE dbo.dim_reason ADD  CONSTRAINT pk_dim_reason PRIMARY KEY CLUSTERED (reason_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'uie_dim_reason_reason_nm' AND object_id = OBJECT_ID('dbo.dim_reason'))
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX uie_dim_reason_reason_nm ON dbo.dim_reason(reason_nm ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END


