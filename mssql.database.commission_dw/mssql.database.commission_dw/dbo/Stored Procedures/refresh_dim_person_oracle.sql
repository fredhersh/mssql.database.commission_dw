﻿






CREATE PROCEDURE [dbo].[refresh_dim_person_oracle]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the commission DW version of the Oracle Code
**	dimension table (dbo.dim_person_oracle).  It is primarily used as a run definition.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create dim_person_oracle if it does not already exist, else truncate table
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.schema_id = s.schema_id WHERE t.name = 'dim_person_oracle' AND t.type='U' AND s.name = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_person_oracle
	(person_cd			VARCHAR(50) NOT NULL
	,start_id			INT NOT NULL
	,end_id				INT NULL
	,department_cd		VARCHAR(3) NOT NULL
	,lob_cd				VARCHAR(2) NOT NULL
	,location_cd		VARCHAR(3) NOT NULL
	CONSTRAINT pk_dim_person_oracle PRIMARY KEY CLUSTERED 
	(
		person_cd ASC,
		start_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END
ELSE
BEGIN

	TRUNCATE TABLE dim_person_oracle

END

/*
** Get temporary table of unique combinations of department, lob and location codes
*/

INSERT	dim_person_oracle
(person_cd
,start_id
,end_id
,department_cd
,lob_cd
,location_cd
)
SELECT	person_cd = ps.cd
,		start_id = CAST(CONVERT(CHAR(8), ps.start_dt, 112) AS INT)
,		end_id = CASE WHEN ps.end_dt = '9999-12-31' THEN NULL ELSE CAST(CONVERT(CHAR(8), ps.end_dt, 112) AS INT) END
,		department_cd = d.cd
,		lob_cd = lb.cd
,		location_cd = lc.cd
FROM	[$(commission)].jour.person_setup ps	INNER JOIN [$(commission)].jour.department d ON (ps.department_id = d.department_id)
												INNER JOIN [$(commission)].jour.lob lb ON (ps.lob_id = lb.lob_id)
												INNER JOIN [$(commission)].jour.location lc ON (ps.location_id = lc.location_id)

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_person_oracle' AND object_id = OBJECT_ID('dbo.dim_person_oracle'))
BEGIN
	ALTER TABLE dbo.dim_person_oracle ADD  CONSTRAINT pk_dim_person_oracle PRIMARY KEY CLUSTERED (person_cd ASC, start_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END


