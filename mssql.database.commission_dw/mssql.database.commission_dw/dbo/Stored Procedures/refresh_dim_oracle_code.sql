﻿





CREATE PROCEDURE [dbo].[refresh_dim_oracle_code]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the Oracle Code
**	dimension table (dbo.dim_oracle_code).  It is primarily used as a run definition.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create dim_oracle_code if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.schema_id = s.schema_id WHERE t.name = 'dim_oracle_code' AND t.type='U' AND s.name = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_oracle_code
	(oracle_code_id			INT IDENTITY(1,1) NOT NULL
	,department_cd		VARCHAR(3) NULL
	,department_descr	VARCHAR(100) NULL
	,lob_cd				VARCHAR(2) NULL
	,lob_descr			VARCHAR(100) NULL
	,location_cd		VARCHAR(3) NULL
	,location_descr		VARCHAR(100) NULL
	CONSTRAINT pk_dim_oracle_code PRIMARY KEY CLUSTERED 
	(
		oracle_code_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Insert Default run record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_oracle_code WHERE oracle_code_id = 0)
BEGIN

	SET IDENTITY_INSERT dbo.dim_oracle_code ON

	INSERT	dbo.dim_oracle_code
	(oracle_code_id
	,department_cd
	,department_descr
	,lob_cd
	,lob_descr
	,location_cd
	,location_descr
	)
	SELECT	oracle_code_id = 0
	,		department_cd = NULL
	,		department_descr = NULL
	,		lob_cd = NULL
	,		lob_descr = NULL
	,		location_cd = NULL
	,		location_descr = NULL

	SET IDENTITY_INSERT dbo.dim_oracle_code OFF

END

/*
** Get temporary table of unique combinations of department, lob and location codes
*/

SELECT DISTINCT	department_cd = d.cd
,				lob_cd = lb.cd
,				location_cd = lc.cd
INTO			#oracle_code
FROM			[$(commission)].jour.person_setup ps	INNER JOIN [$(commission)].jour.department d on (ps.department_id = d.department_id)
														INNER JOIN [$(commission)].jour.lob lb on (ps.lob_id = lb.lob_id)
														INNER JOIN [$(commission)].jour.location lc on (ps.location_id = lc.location_id)

/*
** Populate run table
*/
INSERT	dim_oracle_code
(department_cd
,department_descr
,lob_cd
,lob_descr
,location_cd
,location_descr
)
SELECT	oc.department_cd
,		department_descr = jd.description
,		oc.lob_cd
,		lob_descr = jlb.description
,		oc.location_cd
,		location_descr = jlc.description
FROM	#oracle_code oc	LEFT JOIN dim_oracle_code do ON (oc.department_cd = do.department_cd AND oc.lob_cd = do.lob_cd AND oc.location_cd = do.location_cd)
						LEFT JOIN [$(commission)].jour.department jd ON (oc.department_cd = jd.cd)
						LEFT JOIN [$(commission)].jour.lob jlb ON (oc.lob_cd = jlb.cd)
						LEFT JOIN [$(commission)].jour.location jlc ON (oc.location_cd = jlc.cd)
WHERE	do.oracle_code_id IS NULL

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_oracle_code' AND object_id = OBJECT_ID('dbo.dim_oracle_code'))
BEGIN
	ALTER TABLE dbo.dim_oracle_code ADD  CONSTRAINT pk_dim_oracle_code PRIMARY KEY CLUSTERED (oracle_code_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'uie_dim_oracle_code' AND object_id = OBJECT_ID('dbo.dim_oracle_code'))
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX uie_dim_oracle_code ON dbo.dim_oracle_code(department_cd ASC, lob_cd ASC, location_cd ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

/*
** Drop Temporary Tables
*/

IF (OBJECT_ID('tempdb..#oracle_code')) IS NOT NULL
	DROP TABLE #oracle_code
