﻿



CREATE PROCEDURE [dbo].[refresh_dim_department]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the Department dimension
**	table (dbo.dim_department).  It is primarily used as a department definition.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create dim_department if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.schema_id = s.schema_id WHERE t.name = 'dim_department' AND t.type='U' AND s.name = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_department
	(department_id		INT IDENTITY(1,1) NOT NULL
	,department_nm		VARCHAR(50) NOT NULL
	CONSTRAINT pk_dim_department PRIMARY KEY CLUSTERED 
	(
		department_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Insert Default department record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_department WHERE department_id = 0)
BEGIN

	SET IDENTITY_INSERT dbo.dim_department ON

	INSERT	dbo.dim_department
	(department_id
	,department_nm
	)
	SELECT	department_id = 0
	,		department_nm = 'Unknown'

	SET IDENTITY_INSERT dbo.dim_department OFF

END

/*
** Insert Non-Applicable department record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_department WHERE department_id = 1)
BEGIN

	SET IDENTITY_INSERT dbo.dim_department ON

	INSERT	dbo.dim_department
	(department_id
	,department_nm
	)
	SELECT	department_id = 1
	,		department_nm = 'non applicable'

	SET IDENTITY_INSERT dbo.dim_department OFF

END

/*
** Create temp table for departments
*/

CREATE TABLE #departments
(department_nm	VARCHAR(50)
)

/*
** Populate temporary table of departments
*/

INSERT	#departments
(department_nm
)
SELECT DISTINCT	department_nm = cts.dept
FROM			[$(commission)].out.calc_team_setup cts

/*
** Populate department table
*/

INSERT	dbo.dim_department
(		department_nm
)
SELECT	t.department_nm
FROM	#departments t LEFT JOIN dim_department da ON (t.department_nm = da.department_nm)
WHERE	da.department_nm IS NULL

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_department' AND object_id = OBJECT_ID('dbo.dim_department'))
BEGIN
	ALTER TABLE dbo.dim_department ADD  CONSTRAINT pk_dim_department PRIMARY KEY CLUSTERED (department_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'uie_dim_department_department_nm' AND object_id = OBJECT_ID('dbo.dim_department'))
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX uie_dim_department_department_nm ON dbo.dim_department(department_nm ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

/*
** Drop temporary table
*/

IF (OBJECT_ID('tempdb..#departments')) IS NOT NULL
	DROP TABLE #departments




