﻿

CREATE PROCEDURE [dbo].[refresh_dim_date]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the Date dimension
**	table (dbo.dim_date).  It is primarily used as a date definition.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SET NOCOUNT ON

DECLARE @start_dt	DATETIME
,		@end_dt		DATETIME


/*
** Create dim_date if it does not alreadt exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.schema_id = s.schema_id WHERE t.name = 'dim_date' AND t.type='U' AND s.name = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_date
	(date_id	INT IDENTITY(1,1) NOT NULL
	,date_dt	DATETIME NOT NULL
	,year_no	INT NOT NULL
	,month_no	INT NOT NULL
	,yyyymm_no	INT NOT NULL
	,day_no		INT NOT NULL
	CONSTRAINT pk_dim_date PRIMARY KEY CLUSTERED 
	(
		date_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Insert Default Time record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_date WHERE date_id = 0)
BEGIN

	SET IDENTITY_INSERT dbo.dim_date ON

	INSERT	dbo.dim_date
	(date_id
	,date_dt
	,yyyymm_no
	,year_no
	,month_no
	,day_no
	)
	SELECT	date_id = 0
	,		date_dt = '1900-01-01'
	,		yyyymm_no = 0
	,		year_no = 0
	,		yyyymm_no = 0
	,		day_no = 0

	SET IDENTITY_INSERT dbo.dim_date OFF

END

/*
** Create temp table for dates
*/

CREATE TABLE #dates(date_dt DATETIME)

/*
** Get date range for dimension
*/

SELECT	@start_dt = '1/1/1990', 
		@end_dt = '12/31/' + CONVERT(CHAR(4), DATEADD(YY, 10, GETDATE()), 112)

WHILE @start_dt <= @end_dt
BEGIN

	INSERT #dates (date_dt) VALUES (@start_dt)
	
	SET @start_dt = DATEADD(DAY, 1, @start_dt)

END


/*
** Populate Time table
*/

SET IDENTITY_INSERT dbo.dim_date ON

INSERT	dbo.[dim_date]
(		date_id
,	    date_dt
,		yyyymm_no
,		year_no
,		month_no
,		day_no
)
SELECT	date_id = CAST(CONVERT(CHAR(8), t.date_dt, 112) AS INT)
,	    t.date_dt
,		yyyymm_no = CAST(CONVERT(CHAR(6), t.date_dt, 112) AS INT)
,		year_no = DATEPART(yyyy, t.date_dt)
,		month_no = DATEPART(mm, t.date_dt)
,		day_no = DATEPART(day, t.date_dt)
FROM	#dates t LEFT JOIN dim_date dd ON (t.date_dt = dd.date_dt)
WHERE	dd.date_dt IS NULL

SET IDENTITY_INSERT dbo.dim_date ON

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_date' AND object_id = OBJECT_ID('dbo.dim_date'))
BEGIN
	ALTER TABLE dbo.dim_date ADD  CONSTRAINT pk_dim_date PRIMARY KEY CLUSTERED (date_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'uie_dim_date_date_dt' AND object_id = OBJECT_ID('dbo.dim_date'))
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX uie_dim_date_date_dt ON dbo.dim_date(date_dt ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

/*
** Drop temporary table
*/

IF (OBJECT_ID('tempdb..#dates')) IS NOT NULL
	DROP TABLE #dates


