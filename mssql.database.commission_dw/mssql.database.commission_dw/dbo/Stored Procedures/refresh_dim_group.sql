﻿




CREATE PROCEDURE [dbo].[refresh_dim_group]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the Group dimension
**	table (dbo.dim_group).  It is primarily used as a group definition.
**
**	FHersh		12/01/2019		Created.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create dim_group if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.schema_id = s.schema_id WHERE t.name = 'dim_group' AND t.type='U' AND s.name = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_group
	(group_id		INT IDENTITY(1,1) NOT NULL
	,group_nm		VARCHAR(50) NOT NULL
	CONSTRAINT pk_dim_group PRIMARY KEY CLUSTERED 
	(
		group_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Insert Default group record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_group WHERE group_id = 0)
BEGIN

	SET IDENTITY_INSERT dbo.dim_group ON

	INSERT	dbo.dim_group
	(group_id
	,group_nm
	)
	SELECT	group_id = 0
	,		group_nm = 'Unknown'

	SET IDENTITY_INSERT dbo.dim_group OFF

END

/*
** Insert Non-Applicable group record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_group WHERE group_id = 1)
BEGIN

	SET IDENTITY_INSERT dbo.dim_group ON

	INSERT	dbo.dim_group
	(group_id
	,group_nm
	)
	SELECT	group_id = 1
	,		group_nm = 'non applicable'

	SET IDENTITY_INSERT dbo.dim_group OFF

END

/*
** Create temp table for groups
*/

CREATE TABLE #groups
(group_nm	VARCHAR(50)
)

/*
** Populate temporary table of groups
*/

INSERT	#groups
(group_nm
)
SELECT DISTINCT	group_nm = cts.dept
FROM			[$(commission)].out.calc_team_setup cts

/*
** Populate group table
*/

INSERT	dbo.dim_group
(		group_nm
)
SELECT	t.group_nm
FROM	#groups t LEFT JOIN dim_group da ON (t.group_nm = da.group_nm)
WHERE	da.group_nm IS NULL

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_group' AND object_id = OBJECT_ID('dbo.dim_group'))
BEGIN
	ALTER TABLE dbo.dim_group ADD  CONSTRAINT pk_dim_group PRIMARY KEY CLUSTERED (group_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'uie_dim_group_group_nm' AND object_id = OBJECT_ID('dbo.dim_group'))
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX uie_dim_group_group_nm ON dbo.dim_group(group_nm ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END

/*
** Drop temporary table
*/

IF (OBJECT_ID('tempdb..#groups')) IS NOT NULL
	DROP TABLE #groups





