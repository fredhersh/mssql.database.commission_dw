﻿




CREATE PROCEDURE [dbo].[refresh_dim_run]
AS

/************************************************************************************************
**
**	This procedure does a complete refresh of the Commission DW version of the Run dimension
**	table (dbo.dim_run).  It is primarily used as a run definition.
**
**	FHersh		12/01/2019		Created.
**	FHersh		01/03/2020		Do not add record into dimension if the calc_run_id is
**								already represented.
**	FHersh		03/05/2020		Update existing dimension record if it is marked as 'not locked,
**								but final_flg in out.calc_run is set.
**
************************************************************************************************/

SET NOCOUNT ON

/*
** Create dim_run if it does not already exist
*/

IF NOT EXISTS(SELECT * FROM sys.tables t JOIN sys.schemas s ON t.schema_id = s.schema_id WHERE t.name = 'dim_run' AND t.type='U' AND s.name = 'dbo')
BEGIN

	CREATE TABLE dbo.dim_run
	(run_id				INT IDENTITY(1,1) NOT NULL
	,run_dt				DATETIME NOT NULL
	,run_eff_year_no	INT NOT NULL
	,run_eff_month_no	INT NOT NULL
	,run_eff_yyyymm_no	INT NOT NULL
	,is_run_locked		VARCHAR(3) NOT NULL
	,calc_run_id		INT NOT NULL
	CONSTRAINT pk_dim_run PRIMARY KEY CLUSTERED 
	(
		run_id ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
	) ON [commission_dw_fg01]

END

/*
** Insert Default run record
*/

IF NOT EXISTS (SELECT 1 FROM dbo.dim_run WHERE run_id = 0)
BEGIN

	SET IDENTITY_INSERT dbo.dim_run ON

	INSERT	dbo.dim_run
	(run_id
	,run_dt
	,run_eff_year_no
	,run_eff_month_no
	,run_eff_yyyymm_no
	,is_run_locked
	,calc_run_id
	)
	SELECT	run_id = 0
	,		run_dt = '1900-01-01'
	,		run_eff_year_no = 1900
	,		run_eff_month_no = 1
	,		run_eff_yyyymm_no = 190001
	,		is_run_locked = 'No'
	,		calc_run_id = 0

	SET IDENTITY_INSERT dbo.dim_run OFF

END

/*
** Populate run table
*/

INSERT	dim_run
(run_dt
,run_eff_year_no
,run_eff_month_no
,run_eff_yyyymm_no
,is_run_locked
,calc_run_id
)
SELECT	run_dt = CAST(cr.run_dt AS DATETIME)
,		run_eff_year_no = cr.effective_year
,		run_eff_month_no = cr.effective_month
,		run_eff_yyyymm_no = CAST(CAST(cr.effective_year AS VARCHAR) + RIGHT('00' + CAST(cr.effective_month AS VARCHAR), 2) AS INT)
,		is_run_locked =	CASE
						WHEN cr.final_flg = 1 THEN 'Yes'
						ELSE 'No'
						END
,		cr.calc_run_id
FROM	[$(commission)].out.calc_run cr LEFT JOIN dim_run dr ON (cr.calc_run_id = dr.calc_run_id)
WHERE	dr.calc_run_id IS NULL
ORDER BY calc_run_id

/*
** Update is_run_locked
*/

UPDATE	dr
SET		is_run_locked = 'Yes'
FROM	dim_run dr INNER JOIN [$(commission)].out.calc_run cr ON (dr.calc_run_id = cr.calc_run_id)
WHERE	dr.is_run_locked = 'No'
AND		cr.final_flg = 1

/*
** Add any missing indexes
*/

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'pk_dim_run' AND object_id = OBJECT_ID('dbo.dim_run'))
BEGIN
	ALTER TABLE dbo.dim_run ADD  CONSTRAINT pk_dim_run PRIMARY KEY CLUSTERED (run_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg01]
END

IF NOT EXISTS(SELECT * FROM sys.indexes WHERE name = 'uie_dim_run_calc_run_id' AND object_id = OBJECT_ID('dbo.dim_run'))
BEGIN
	CREATE UNIQUE NONCLUSTERED INDEX uie_dim_run_calc_run_id ON dbo.dim_run(calc_run_id ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [commission_dw_fg02]
END
