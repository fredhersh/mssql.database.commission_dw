﻿


CREATE PROCEDURE [calc].[run_commission_dw_etl] AS

/************************************************************************************************
**
**	This procedure runs the complete ETL process for the Commission DW.  It
**	is called by the Calculator API.
**
**	FHersh		01/08/2020		Created.
**	FHersh		01/15/2020		Added refresh_dim_department.
**	FHersh		04/07/2020		Added refresh_rpt_journal_delta.
**
************************************************************************************************/

SET NOCOUNT ON

DECLARE	@return_value int

/*
** Refresh all dimensions
*/
EXEC	@return_value = [dbo].[refresh_dim_date]

EXEC	@return_value = [dbo].[refresh_dim_run]

EXEC	@return_value = [dbo].[refresh_dim_account]

EXEC	@return_value = [dbo].[refresh_dim_department]

EXEC	@return_value = [dbo].[refresh_dim_team]

EXEC	@return_value = [dbo].[refresh_dim_group]

EXEC	@return_value = [dbo].[refresh_dim_comment]

EXEC	@return_value = [dbo].[refresh_dim_reason]

EXEC	@return_value = [dbo].[refresh_dim_oracle_code]

EXEC	@return_value = [dbo].[refresh_dim_person_oracle]

/*
** Refresh all fact tables
*/
EXEC	@return_value = [dbo].[refresh_fact_journal]

/*
** Refresh all reporting tables
*/
EXEC	@return_value = [dbo].[refresh_rpt_journal]

EXEC	@return_value = [dbo].[refresh_rpt_journal_delta]