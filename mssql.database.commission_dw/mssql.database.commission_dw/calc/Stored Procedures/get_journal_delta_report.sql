﻿


CREATE PROCEDURE [calc].[get_journal_delta_report] AS

/************************************************************************************************
**
**	This procedure returns the jourtnal delta facts.
**
**	FHersh		01/02/2020		Created.
**
************************************************************************************************/

SELECT	*
FROM	[dbo].[complete_journal_delta_facts]