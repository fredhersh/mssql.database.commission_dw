﻿


CREATE PROCEDURE [calc].[get_journal_report]

@AsOfPeriodId AS INT = NULL

/************************************************************************************************
**
**	This procedure returna all data required for the Journal Report.
**
**	FHersh		01/13/2020		Created.
**	FHersh		02/26/2020		Added "Override" capabilities.
**	FHersh		04/07/2020		Return rpt_journal_delta and added is delta indicator.
**
************************************************************************************************/

AS

IF @AsOfPeriodId IS NULL
	SET @AsOfPeriodId = CAST(CONVERT(CHAR(6), DATEADD(MONTH, -1, GETDATE()), 112) AS INT)

SELECT	AsOfPeriodId = r.AsOfPeriodId
,		[Group] = r.[Group]
,		Team = r.Team
,		Person = r.Person
,		Category = r.Category
,		OracleAccount = r.OracleAccount
,		OracleAccountDesc = r.OracleAccountDesc
,		Side = r.Side
,		LOB = r.LOB
,		Department = r.Department
,		Location = r.Location
,		Debit =	CASE
				WHEN o.AsOfPeriodId IS NOT NULL THEN o.Debit
				ELSE r.Debit
				END
,		Credit =	CASE
					WHEN o.AsOfPeriodId IS NOT NULL THEN o.Credit
					ELSE r.Credit
					END
,		Formula =	CASE
					WHEN o.AsOfPeriodId IS NOT NULL THEN o.Formula
					ELSE r.Formula
					END
,		is_delta = 'No'
FROM	rpt_journal r LEFT JOIN rpt_journal_override o ON (r.AsOfPeriodId = o.AsOfPeriodId AND r.[Group] = o.[Group] AND r.Team = o.Team AND r.Person = o.Person AND r.Category = o.Category AND r.OracleAccount = o.OracleAccount AND r.OracleAccountDesc = o.OracleAccountDesc AND r.LOB = o.LOB AND r.Department = o.Department AND r.Location = o.Location)
WHERE	r.AsOfPeriodId = @AsOfPeriodId

UNION ALL

SELECT	AsOfPeriodId = o.AsOfPeriodId
,		[Group] = o.[Group]
,		Team = o.Team
,		Person = o.Person
,		Category = o.Category
,		OracleAccount = o.OracleAccount
,		OracleAccountDesc = o.OracleAccountDesc
,		Side = o.Side
,		LOB = o.LOB
,		Department = o.Department
,		Location = o.Location
,		Debit =	o.Debit
,		Credit = o.Credit
,		Formula = o.Formula
,		is_delta = 'No'
FROM	rpt_journal r RIGHT JOIN rpt_journal_override o ON (r.AsOfPeriodId = o.AsOfPeriodId AND r.[Group] = o.[Group] AND r.Team = o.Team AND r.Person = o.Person AND r.Category = o.Category AND r.OracleAccount = o.OracleAccount AND r.OracleAccountDesc = o.OracleAccountDesc AND r.LOB = o.LOB AND r.Department = o.Department AND r.Location = o.Location)
WHERE	o.AsOfPeriodId = @AsOfPeriodId
AND		r.AsOfPeriodId IS NULL

UNION ALL

SELECT	AsOfPeriodId = r.AsOfPeriodId
,		[Group] = r.[Group]
,		Team = r.Team
,		Person = r.Person
,		Category = r.Category
,		OracleAccount = r.OracleAccount
,		OracleAccountDesc = r.OracleAccountDesc
,		Side = r.Side
,		LOB = r.LOB
,		Department = r.Department
,		Location = r.Location
,		Debit =	r.Debit
,		Credit = r.Credit
,		Formula = r.Formula
,		is_delta = 'Yes'
FROM	rpt_journal_delta r

ORDER BY	AsOfPeriodId
,			[Group]
,			Team
,			Person
,			Category
,			OracleAccount
,			LOB
,			Side
